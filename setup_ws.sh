#!/bin/bash
# Call with . setup_ws.sh to setup all environment variables
source ws/install/local_setup.bash
source /usr/share/gazebo/setup.bash
export GAZEBO_PLUGIN_PATH="`pwd`/ws/build/reacsa_control:${GAZEBO_PLUGIN_PATH}"
export GAZEBO_MODEL_PATH="`pwd`/ws/src/reacsa_gazebo/models:${GAZEBO_MODEL_PATH}"