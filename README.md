# SatSim-TAR

The main repository for the Satellite Simulator employing thrusters as well as a reaction wheel. TThis repository consists of five main folders:
- ForceTorqueSensor: All files and data from the F/T-Sensor and the Characterization of the thrusters 
- scripting: Simple python scripts that do one job. Mostly related to plotting or creating the heighmap image
- trajectory_finder: the Matlab (deprecated) and PyDrake implementation used to find optimal trajectories. In particular the special trajectories (such as the circular and the s-shaped) are here. There is a copy of this as a ros2-node within the workspace that provides the computation as an action as well. However this only finds state-2-state trajectories.
- ws: The ROS2 workspace. More info below.

# How to cite

If you use this work for an academic publication please cite the following publications:

```
@inproceedings{bredenbeck2022astra,
  doi = {10.48550/ARXIV.2206.03993},
  url = {https://arxiv.org/abs/2206.03993},
  author = {Bredenbeck, Anton and Vyas, Shubham and Suter,Willem and Zwick, Martin and Borrmann, Dorit and Olivares-Mendez, Miguel and Nüchter, Andreas},
  title = {Finding and Following Optimal Trajectories for an Overactuated Floating Robotic Platform},
  booktitle = {16th Symposium on Advanced Space Technologies in Robotics and Automation},
  year = {2022}
}
```

```
@misc{https://doi.org/10.48550/arxiv.2207.10693,
  doi = {10.48550/ARXIV.2207.10693},
  url = {https://arxiv.org/abs/2207.10693},
  author = {Bredenbeck, Anton and Vyas, Shubham and Zwick, Martin and Borrmann, Dorit and Olivares-Mendez, Miguel and Nüchter, Andreas},
  keywords = {Robotics (cs.RO), FOS: Computer and information sciences, FOS: Computer and information sciences},
  title = {Trajectory Optimization and Following for a Three Degrees of Freedom Overactuated Floating Platform},
  publisher = {arXiv},
  year = {2022},
  copyright = {Creative Commons Attribution 4.0 International}
}

```


## TL;DR or How to make this run on your floating platform

To make this controller run on your free-floating platform you should follow these steps:
- Install all the prerequisites as described below
- Create ROS2 nodes that function as your drivers. In particular:
    - your pose sensor must publish the system pose on the topic `/vicon/reacsa/reacsa` which is of type `geometry_msgs/msg/PoseWithCovarianceStamped`
    - your thruster driver and motor driver must subscriber to the topics `/set_force` and `/set_torque` respectively and translate those actions to the physical system. These messages are custom types defined in `reacsa_interfaces`
- Create a trajectory you want to follow either via the ros node or by running the python scripts in `trajectory_finder/pydrake_impl/`.
- Precompute the feedback gain matrices K by calling the service `/prepare_trajectory` offered by the trajectory follower with the relative paths to the text files containing the trajectory as arguments.
- To follow the trajectory call the action `/follow_trajectory`. This action will only proceed if you've called `/prepare_trajectory` beforehand. Otherwise the execution will stop with a warning. 

## Installation

### Prerequisites
- ROS2 - Foxy ([install](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html))
- Gazebo11 ([install](http://gazebosim.org/tutorials?tut=install_ubuntu))
- ROS2-Gazebo interaction packages (`sudo apt-get install ros-foxy-gazebo-ros-pkgs`)
- PyDrake ([install](https://drake.mit.edu/apt.html#stable-releases))
- Vicon API (install as described in [this](https://github.com/OPT4SMART/ros2-vicon-receiver) git repo.)

### Setup
Clone this repository to your machine and navigate to the repository:
`git clone https://gitlab.com/anton.bredenbeck/ff-trajectories && cd ff-trajectories`
Source ros2:
`source /opt/ros/foxy/setup.bash`
Compile the interfaces:
`cd ws && colcon build --packages-select reacsa_interfaces`
Setup the local workspace
`cd .. && . setup_ws.sh`
Compile the actions:
`cd ws && colcon build --packages-select reacsa_actions`
Compile the rest:
`colcon build`

After you compile the first time and the ws is sourced you can simply compile the whole workspace with `colcon build`.

## ROS-2 Nodes

There are six ROS2 packages that contain a number of nodes. They are the following:
- reacsa_interfaces: Contains no nodes. Simply contains all message and service definitions that are required for the Simulation and the controller
- reacsa_actions: Same as above, except it contains all the action definitions
- reacsa_gazebo: Contains all things related to the gazebo simulation. I.e. all models, all plugins that simulate different sensors. The simulation can be started via the launchfile `ros2 launch reacsa_gazebo reacsa.launch.py`. Make sure you call this from within the `ws`-Folder as the location to the logging files is incorrect otherwise. 
The parameters of the simulation can be adjusted in the [models](ws/src/reacsa_gazebo/models).
- reacsa_control: 
    - joystick_control: A node that translates the joystick commands to the thruster and reaction wheel messages. When pressing start it finds and follows an optimal trajectory to the origin. Started via `ros2 launch reacsa_control joystick_control.launch.py`
    - trajectory_follower: A node that advertises the action _/trajectory_follower_ that follows the trajectory specified by the text files which names are passed as an argument. This node uses Time Varying LQR to follow said trajectories. Launch via `ros2 launch reacsa_control trajectory_follower.launch.py`
    - trajectory_planner: A node that computes the optimal trajectory between two states. It advertises the action /compute_trajectory which finds and stores the optimal trajectory in text files at a specified location. Run via `ros2 run reacsa_control trajectory_planner.py --ros-args --params-file src/reacsa_control/config/params.yaml`
- vicon_receiver: Contains the ros2 node vicon_client that reads the vicon stream and publishes the pose on the correct ros2 topic. Launch via `ros2 launch vicon_receiver client.launch.py ` 
- thruster_drivers: Contains the node that needs to run on the controlling RaspberryPi. See instructions below.

See below for a structure of all nodes, topics, services and actions and how they interact: 

![ROS2 Node/Message/Service Structure](doc/ros_structure.png)

## Raspberry Pi Setup

- Linux 20.04 Server OS
- Connect to WiFi
- Install ROS2 foxy base via [this](https://docs.ros.org/en/foxy/Installation/Ubuntu-Install-Debians.html)
- Install WiringPi and libwiringpi-dev via apt
- Add the custom realtime kernel via [this](http://www.frank-durr.de/?p=203), BUT do not delete the firmware (don't do `pi@raspberry ~$ sudo rm -r /lib/firmware/` as it will delete the wifi firmware ase well)
- install compilatin tools via `sudo apt install build-essential python3-colcon-common-extensions`
- create a workspace vie `mkdir -p ~/ws/src && cd ~/ws && colcon build`
- Copy the folders [`reacsa_interfaces`](ws/src/reacsa_interfaces) and [`thruster_drivers`](ws/src/thruster_drivers) into the `src` folder of your colcon workspace
- Source ros2 `source /opt/ros/foxy/setup.bash` 
- First compile the interfaces (`colcon build --packages-select reacsa-interfaces`), source the local workspace (`source ws/install/local_setup.bash`) and afterwards the drivers (`colcon build --packages-select thruster_drivers`)
- Run via `ros2 run thruster_drivers thruster_drivers` 


## Control Architecture

# System Model 

The system model used to design the controller is shown below. **Beware:** the thruster numbering and coordinate system is different as indicated on the Hardware. This offset is handled in the thruster setter node. Please make sure you test if the correct thrusters are activated if you design a new controller. 

![REACSA system model](doc/reacsa.png)
