#include <stdio.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <chrono>
#include <LabJackM.h>
#include "LJM_StreamUtilities.h"
#include <ctime>
#include <chrono>

using namespace std;

void Stream(int handle, int numChannels, const char ** channelNames,
	double scanRate, int scansPerRead, int numReads);

void HardcodedConfigureStream(int handle);

int Numbers[] = {0, 2, 4, 6, 8, 10}; // positive readible chanel numbers
const char * CHANNEL_NAMES[] = {"AIN0", "AIN2", "AIN4", "AIN6", "AIN8", "AIN10"}; // positive readible chanel names
double INIT_SCAN_RATE = 100; // 100 Scan rate Hz not more than 1000Hz
const int NUM_READS = 60;   // 120 ~Scan time = input value /2 (seconds)
int NUM_CHANNELS = 6; // Number of channels
int SCANS_PER_READ = INIT_SCAN_RATE / 2; // default value is /2, but the maximum is could be equal to INIT_SCAN_RATE
int handle;

int main()
//int Streamdiff(void*)
{
	// Open first found LabJack
	handle = OpenOrDie(LJM_dtANY, LJM_ctANY, "LJM_idANY");
	// handle = OpenSOrDie("LJM_dtANY", "LJM_ctANY", "LJM_idANY");
	//PrintDeviceInfoFromHandle(handle);
	//printf("\n");
	Stream(handle, NUM_CHANNELS, CHANNEL_NAMES, INIT_SCAN_RATE, SCANS_PER_READ,
		NUM_READS);
	CloseOrDie(handle);
	WaitForUserIfWindows();
	return LJME_NOERROR;
}

void HardcodedConfigureStream(int handle)
{
	// Configure the analog inputs' negative channel, range, settling time and
	// resolution.
	// Note: when streaming, negative channels and ranges can be configured for
	// individual analog inputs, but the stream has only one settling time and
	// resolution.
	const int STREAM_TRIGGER_INDEX = 0; //Setting triger. Default value 0
	const int STREAM_CLOCK_SOURCE = 0; // Default for not synchronized scan
	const int STREAM_RESOLUTION_INDEX = 0; // The resolution index for stream readings.
	// A larger resolution index generally results in lower noise and longer sample times
	// Device-specific details: Valid values: 0-8. 0=Default=>1.
	const double STREAM_SETTLING_US = 0; // Default value: 0
	// Time in microseconds to allow signals to settle after switching the mux. Default=0.
	//If set to a value < 1 the T7 will automatically determine the necessary settling time.
	//When the sample rate is above 60kHz settling time will be set to ~5 us. Max is 4400.
	const int AIN_ALL_RANGE = 0; //A write to this global parameter affects all AIN.
	// A read will return the correct setting if all channels are set the same, but otherwise will return -9999.
	//------------------------------------------------------------------------------------------------------------//
	//const int AIN_ALL_NEGATIVE_CH = LJM_GND; // Use this to check differential chan setting only, in others cases should be commented.
	//WriteNameOrDie(handle, "AIN_ALL_NEGATIVE_CH", AIN_ALL_NEGATIVE_CH); // Use this to check differential chan setting only, in others cases it should be commented.


	WriteNameOrDie(handle, "STREAM_TRIGGER_INDEX", STREAM_TRIGGER_INDEX);
	WriteNameOrDie(handle, "STREAM_CLOCK_SOURCE", STREAM_CLOCK_SOURCE);
	WriteNameOrDie(handle, "STREAM_RESOLUTION_INDEX", STREAM_RESOLUTION_INDEX);
	WriteNameOrDie(handle, "STREAM_SETTLING_US", STREAM_SETTLING_US);
	WriteNameOrDie(handle, "AIN_ALL_RANGE", AIN_ALL_RANGE);


	for( int i = 0; i < NUM_CHANNELS; i++) // Writing diferential settings to Labjack
	{
		char buffer[32];
		sprintf(buffer , "AIN%d_NEGATIVE_CH" , Numbers[i]);
		WriteNameOrDie(handle, buffer , Numbers[i] + 1);
	}
}


void Stream(int handle, int numChannels, const char ** channelNames, // Fuction to stream from Labjack to txt files
	double scanRate, int scansPerRead, int numReads)
{
	// integers needed for Labjack functions
	int err, iteration;
	int numSkippedScans = 0;
	int totalSkippedScans = 0;
	int deviceScanBacklog = 0;
	int LJMScanBacklog = 0;

	int * aScanList = (int*) malloc(sizeof(int) * numChannels); //Space allocation for names string

	unsigned int aDataSize = numChannels * scansPerRead; // //Space allocation for main data string
	double * aData = (double*) malloc(sizeof(double) * aDataSize); // aData - All streamed data from labjack is here,
																   //if you need to check how ir looks print it to screen

	// Clear aData. This is not strictly necessary, but can help debugging.
	memset(aData, 0, sizeof(double) * aDataSize);

	err = LJM_NamesToAddresses(numChannels, channelNames, aScanList, NULL); // Seting chanel to stream from buffer
	ErrorCheck(err, "Getting positive channel addresses");

	HardcodedConfigureStream(handle); // function described above for setting up Labjack

	printf("Starting stream...\n");
	err = LJM_eStreamStart(handle, scansPerRead, numChannels, aScanList,
		&scanRate); // initiating stream start
	ErrorCheck(err, "LJM_eStreamStart");
	printf("\n");
	printf("Stream started. Actual scan rate: %.04f Hz (%.02f sample rate)\n",
		  scanRate, scanRate * numChannels);

	//Opening files for data loging
	ofstream myfile ("FT_05243.txt", ios::out);

    int dataLength = 0;

    // measuring time of stream loop
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    //Main stream loop
	for (iteration = 0; iteration < numReads; iteration++) {
		err = LJM_eStreamRead(handle, aData, &deviceScanBacklog,
			&LJMScanBacklog); // geting Data from Labjack
		ErrorCheck(err, "LJM_eStreamRead");
		dataLength += aDataSize/NUM_CHANNELS;// Counting how many Values of each channel was streamed

		//Timestampng in log file. Same clock should be used as in total time taken loop
		struct timeval tmnow;
		struct tm *tm;
		char buf[30], usec_buf[6];
		gettimeofday(&tmnow, NULL);
		tm = localtime(&tmnow.tv_sec);
		strftime(buf,30,"%H:%M:%S", tm);
		strcat(buf,".");
		sprintf(usec_buf,"%03d",(int)tmnow.tv_usec/1000);
		strcat(buf,usec_buf);

		// Performing data separation to proper view in a Log from aData string and writing to file
		for ( int z = 0; z < scansPerRead; z++) {
			myfile << buf;

			for ( int o = 0; o < 6; o++) {
				myfile << " " << aData[z*numChannels+o];
			}
			myfile << endl;
		}
		//Loop end time and total time writing to a file
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end-start;
		ofstream myfile2("time.txt", ios::out);
		myfile2 << (elapsed_seconds.count())<< endl;
		myfile2.close();

		// Functions to check if there was no error in a stream.
		numSkippedScans = CountAndOutputNumSkippedScans(numChannels,
		scansPerRead, aData);

		if (numSkippedScans) {
			printf("  %d skipped scans in this LJM_eStreamRead\n",
			numSkippedScans);
			totalSkippedScans += numSkippedScans;
		}
		//printf("\n");
	}
	myfile.close();

	if (totalSkippedScans) {
		printf("\n****** Total number of skipped scans: %d ******\n\n",
		totalSkippedScans);
	}

	printf("Stopping stream\n");
	err = LJM_eStreamStop(handle);
	ErrorCheck(err, "Stopping stream");

	ofstream myfile3 ("scans.txt", ios::out);
	myfile3 << dataLength << endl;
	std::cout << dataLength << std::endl;
	myfile3.close();

	free(aData);
	free(aScanList);
}
