#!/bin/python
import sys
import numpy as np
import scipy.constants as cons
import matplotlib.pyplot as plt

T = 293  # K
M = 0.2896  # kg/mol
gamma = 1.4  # 1
pe = 100e3   # Pa
p = 700e3  # Pa
Ae = 1.644e-5  # m
rho = 1.2  # kg/m^3

# Increase font size
plt.rcParams.update({'font.size': 30})

# Folder names of datasets
folder_names = ["2bar/", "4bar/", "6bar/", "7bar/100msec/", "7,5bar/"]
# This factor was determined from measuring the force
# enacted by random stuff (of which the weight was determined
# using the scale in the prl) enacted on the F/T-sensing setup
# see calib_fac.py
corr_factor = 62.1


def main(path):
    # Read all datatsets...
    force = np.zeros([len(folder_names)])
    for i in range(len(folder_names)):
        # ... and compute the bias corrected and scaled norm ...
        data = np.loadtxt(
            path + folder_names[i] + 'FT_05243.txt', usecols=(1, 2, 3))
        norms = np.linalg.norm(data[20:], axis=1)
        bias = np.mean(norms[20: 180])
        # ... and find the maximally occuring force
        force[i] = max(corr_factor * (norms - bias))

    x = np.array([300e3, 500e3, 700e3, 800e3, 850e3])
    # Find ideal force profile
    ve = np.sqrt(T * cons.R / M * 2 * gamma / (gamma - 1)
                 * (1 - (pe / x)**((gamma - 1) / gamma)))
    f_ideal = Ae * (rho * ve**2 + (x - pe))

    x = x / 100e3
    # Do a linear fit through the data
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, force, rcond=None)[0]

    # Plot data and linear fit
    fig, ax = plt.subplots()
    ax.errorbar(x, force, yerr=0.63, xerr=0.2, fmt=" ", capsize=3)
    ax.plot(x, m * x + c, 'r', label='Fitted line')
    ax.plot(x, f_ideal, 'g', label='Ideal force profile')
    ax.grid()
    textstr = f"Best Fit: y = {m:.2f}x + {c:.2f}"
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(3, 10, textstr, fontsize=20, bbox=props)
    ax.set_xlabel("Absolute Pressure [Bar]")
    ax.set_ylabel("Maximal Force [N]")
    ax.legend(["Best Fit", "Ideal Force", "Measurements"])
    fig.set_size_inches(16, 12)
    ax.set_title("Nominal Force at different Pressures")
    plt.tight_layout()
    plt.savefig("pressure_vs_force.eps", dpi=100)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Not enough arguments. Please specify path.")
    else:
        main(sys.argv[1])
