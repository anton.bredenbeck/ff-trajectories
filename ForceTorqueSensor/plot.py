#!/bin/python
import sys
import numpy as np
import matplotlib.pyplot as plt

# This factor was determined from measuring the force
# enacted by random stuff (of which the weight was determined
# using the scale in the prl) enacted on the F/T-sensing setup
# see calib_fac.py
corr_factor = 62.1


def main(path, f):
    # Load the respective dfile
    data = np.loadtxt(path + 'FT_05243.txt', usecols=(1, 2, 3))[20:]

    # Move timestampts to start at 0
    stamps = np.arange(0, 1 / f * len(data), 1 / f)
    # Overall force is norm of the x-y-z-force vector
    norms = np.linalg.norm(data, axis=1)

    # Compute bias by averaging the initial phase
    bias = np.mean(norms[0: 180])

    # Increase font size
    plt.rcParams.update({'font.size': 30})

    # Plot scaled and bias corrected data
    fig, ax = plt.subplots()
    ax.plot(stamps, corr_factor * (norms - bias))
    ax.set_title("Thrusts at 6 Bar with duration 100 ms of opposing banks")
    ax.grid()
    ax.set_xlabel("Time [s]")
    ax.set_ylabel("Force [N]")

    fig.set_size_inches(16, 12)
    plt.tight_layout()
    plt.savefig("force.eps", dpi=100)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("Not enough arguments. Please specify path.")
    else:
        main(sys.argv[1], int(sys.argv[2]))
