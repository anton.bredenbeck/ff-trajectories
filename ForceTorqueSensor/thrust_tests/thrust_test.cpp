#include "common.hpp"

int main(int argc, char const *argv[])
{
	if(argc < 3)
	{
		std::cout << "Not enough arguments. Usage:\n"
		          << "\t thrust_test <Thruster Number1> [<Thruster Number2>] <Thrust Duration [ms]>"
		          << std::endl;
        return 1;
	}

	init_gpio();

	int idx1, idx2, ms;

	if(argc == 3)
	{ 
		idx1 = std::stoi(argv[1]);
		ms = std::stoi(argv[2]);

		std::cout << "Firing thruster " << idx1 << " for " << ms << "ms" << std::endl;
		delay(100);

		set_thruster(idx1, HIGH);
		delay(ms); 
		set_thruster(idx1, LOW);
	}
	else if(argc == 4)
	{
		idx1 = std::stoi(argv[1]);
		idx2 = std::stoi(argv[2]);
		ms = std::stoi(argv[3]);

		std::cout << "Firing thruster " << idx1 << " and " << idx2 << " for " << ms << "ms" << std::endl;
		delay(100);

		set_thruster(idx1, HIGH);
		set_thruster(idx2, HIGH);
		delay(ms); 
		set_thruster(idx1, LOW);
		set_thruster(idx2, LOW);
	}
	else
	{
		std::cout << "Wrong number of arguments. Usage:\n" 
		          << "\t thrust_test <Thruster Number1> [<Thruster Number2>] <Thrust Duration [ms]>"
		          << std::endl;
	}

	return 0;
}