import numpy as np
import matplotlib.pyplot as plt

# Actual weights measured on a scale of the objects on a scale in the prl
weight = np.array([0.2828, 0.3376, 0.6204, 1.4514, 2.0718, 2.4860])

data = np.array(
    [9.81 * weight,  # actual force
     # measured difference in signal, not scaled a including bias
     [0.016692, 0.03586, 0.0851644, 0.22229, 0.347283, 0.342439]]
)

# Extract data
x = data[0, :]
y = data[1, :]

# Do a linear fit
A = np.vstack([x, np.ones(len(x))]).T
m, c = np.linalg.lstsq(A, y, rcond=None)[0]

# Plot it
plt.figure(figsize=(16, 12), dpi=100)
plt.scatter(x, y)
plt.plot(x, m * x + c, 'r', label='Fitted line')
plt.xlabel("Force [N]")
plt.ylabel("Signal [1]")
plt.grid()
textstr = f"Conversion factor is: {1/m:.2f} N/1"
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
plt.text(10, 0.3, textstr, fontsize=14, bbox=props)
plt.tight_layout()
plt.savefig("f-t-calib.png")
