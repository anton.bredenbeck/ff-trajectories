#include <cassert>
#include <iostream>
#include <wiringPi.h>

/* The GPIO pins used by the thrusters. The index
 * inside the array corresponds to the numbering
 * on the system previously this was done in RPi.GPIO
 * which does the numbers according to BCM as
 * 		{18, 4, 17, 27, 22, 10, 9, 11}
 * In wiring pi this corresponds to:*/
const int GPIO_PINS[8] = {1, 7, 0, 2, 3, 12, 13, 14};

/* Checks if the thruster index and value
 * are valid and sets the value if that is the
 * case. Terminates the program otherwise */
void set_thruster(int idx, int value);

/* Init the wiring pi library and set all
 * required GPIO pins (all in GPIO_PINS)
 * to mode OUTPUT */
void init_gpio();
