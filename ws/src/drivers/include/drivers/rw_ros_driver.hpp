#ifndef RW_DRIVER_HPP
#define RW_DRIVER_HPP 

#include <rclcpp/rclcpp.hpp>

#include "reacsa_interfaces/srv/spin_up_rw.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/msg/motor_state.hpp"

#include "ReactionWheelDriver.hpp"

class RWDriver: public rclcpp::Node
{
public:
    RWDriver(float max_torque=1.44, /* Nm */
             float max_velocity=500.0*2.0*M_PI / 60.0 /* rad/s == 500 RPM */,
             float epsilon=0.1, /* rad / s */
             float rw_inertia=0.047 /* kgm^2 */,
             float motor_inertia=0.000102 /* kgm^2 */,
             float dt=0.01 /* s */,
             float idling_vel=250.0*2.0*M_PI /* rad/s */
                           ):
                            rclcpp::Node("rw_driver"),
                            controller("192.168.1.155"),
                            spun_up(false),
                            watchdog_counter(0),
                            MAX_TORQUE(max_torque),
                            MAX_VELOCITY(max_velocity),
                            EPSILON(epsilon),
                            DT(dt),
                            RW_INERTIA(rw_inertia),
                            MOTOR_INERTIA(motor_inertia),
                            /* Store the inverse of the inertia, such that we don't 
                             * need to recompute it all the time */
                            M_1_I(1.0 / (MOTOR_INERTIA + RW_INERTIA))
	{
        /* Make sure the singleton isn't already instantiated */
        if(instance != NULL)
        {
            RCLCPP_ERROR(this->get_logger(), "Controller already exists! Shutting down.");
            rclcpp::shutdown();
        }
        else
        {
            /* Init the sigelton instance */
            instance = this; 
            /* Init Motor */
            controller.enableReactionWheel();

            /*Init pub and sub */
            this->torque_sub = this->create_subscription<reacsa_interfaces::msg::Torque>("/set_torque",
                                      /* QoS */ rclcpp::SensorDataQoS(),
                                      std::bind(&RWDriver::torque_callback,
                                                this,
                                                std::placeholders::_1));
            this->rw_pub = this->create_publisher<reacsa_interfaces::msg::MotorState>("/motor_state", 
                                                                                     /* QoS */ rclcpp::SensorDataQoS());

            /* Init the timer that regularly calls the function that reads out current velocity
             * and also enacts the current torque command */
            act_timer = this->create_wall_timer(
                    std::chrono::milliseconds((int) (DT * 1e3)),
                    std::bind(&RWDriver::act, this));

            /* Init the action server for spinning up the reaction wheel */
            server = this->create_service<reacsa_interfaces::srv::SpinUpRW>("spin_up_rw",
                std::bind(&RWDriver::spin_up, this, std::placeholders::_1, std::placeholders::_2));

            this->WATCHDOG_LIMIT = int(1.0 / this->DT);
            this->idling_vel = idling_vel;
        }
    }
    ~RWDriver()
    {
        /* Deleting instance, hence no sigleton exists anymore */
        instance = NULL;
    }

    static void static_sigint_handler(int sig);

private:
    void torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr t_msg);
    void act();
    void sigint_handler(int sig);
    float torque_to_vel(float torque, float dt);

    /* Service related binding function */
    void spin_up(const std::shared_ptr<reacsa_interfaces::srv::SpinUpRW::Request> request,
                      std::shared_ptr<reacsa_interfaces::srv::SpinUpRW::Response> response);

    /********** Class Variables ******************************************/
    ReactionWheelController controller;

    /* Bool that is true if the RW is sping up to some
     * idling velocity, false if it is not */
    bool spun_up;
    int watchdog_counter;

    /* Max allowable torque and angular velocity */
    const float MAX_TORQUE, MAX_VELOCITY,
                 EPSILON, DT; // Error to stop ar when spinning up the RW

    /* System parameters */
    const float RW_INERTIA;
    const float MOTOR_INERTIA;
    const float M_1_I;
    float idling_vel;
    int WATCHDOG_LIMIT;

    /* Pub/Sub and timers */
    rclcpp::TimerBase::SharedPtr act_timer;
    rclcpp::Publisher<reacsa_interfaces::msg::MotorState>::SharedPtr rw_pub;
    rclcpp::Subscription<reacsa_interfaces::msg::Torque>::SharedPtr torque_sub;
    rclcpp::Service<reacsa_interfaces::srv::SpinUpRW>::SharedPtr server;
    /* Current Torque command and current velocity read */
    reacsa_interfaces::msg::Torque tau;
    reacsa_interfaces::msg::MotorState rw;

    /********** Static members ************************/
    inline static RWDriver* instance;
};

#endif // RW_DRIVER_HPP
