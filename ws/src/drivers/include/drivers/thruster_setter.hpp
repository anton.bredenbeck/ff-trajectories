/* A ROS2 node that offers to set the force on all thrusters on the
 * REACSA system */
#include <vector>
#include <limits>
#include <rclcpp/rclcpp.hpp>

#include "reacsa_interfaces/msg/force.hpp"
#include "common.hpp"

using msg = reacsa_interfaces::msg::Force;

/* Since the coordinate systems are flipped we have to have a 
 * mapping of the simulated to the actual thruster indeces */
const int SIM_2_REAL[8] = {7, 6, 5, 4, 3, 2, 1, 0};
const rclcpp::Duration MAX_ON(2, 0); //  seconds 
const rclcpp::Duration COOL_DOWN(0, 5e8); // 0.5 seconds 

class ThrusterSetter: public rclcpp::Node
{
    public:
    
    ThrusterSetter():rclcpp::Node("thruster_setter")
	{
            this->thrust_sub = this->create_subscription<msg>("/set_force",
                                      /* QoS */ rclcpp::SensorDataQoS(),
                                      std::bind(&ThrusterSetter::thrust_callback,
                                                this,
                                                std::placeholders::_1));
            /* Initialize the GPIO pins */
            init_gpio();

            for(int i = 0; i < 8; i++){
                time_stamps[i] = this->now() + rclcpp::Duration(std::numeric_limits<int>::max() / 2);
             	currently_on[i] = false;
	        }

            /* Init the timer that regularly publishes the force messages */
            timer = this->create_wall_timer(
                    std::chrono::milliseconds(int(100)),
                    std::bind(&ThrusterSetter::timer_callback, this));
    }
    private:
        void thrust_callback(const msg::SharedPtr f_msg);
        void timer_callback();

        /********** Class Variables ******************************************/
        rclcpp::Subscription<msg>::SharedPtr thrust_sub;
        rclcpp::TimerBase::SharedPtr timer;
        rclcpp::Time time_stamps[8];

        bool received_initial_msg = false;
	    bool currently_on[8];
        msg curr_force_cmd;
};
