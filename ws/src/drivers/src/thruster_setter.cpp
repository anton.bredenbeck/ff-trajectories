/* A ROS2 node that offers to set the force on all thrusters on the
 * REACSA system */
#include "thruster_setter.hpp"

void mySigintHandler(int sig)
{
      // Do some custom action.
      for(int j = 0; j < 8; j++) set_thruster(j, LOW);
      // All the default sigint handler does is call shutdown()
      rclcpp::shutdown();
      exit(sig);
}

void ThrusterSetter::timer_callback()
{
    /* Only do the safety checks if you received at least one message */
    if(received_initial_msg)
    {
        rclcpp::Time t = this->now();

        /* Make sure we don't deal with old messages */
        if(t - this->curr_force_cmd.header.stamp > MAX_ON)
        {
            RCLCPP_ERROR(this->get_logger(), "Didn't receive a message for > 2s. Shutting down ...");
            rclcpp::shutdown();
            exit(EXIT_FAILURE);
        }
        /* Make sure no thruster is on longer than the maximal on duration */
        for(int i = 0; i < 8; i++)
        {
            if(t - this->time_stamps[i] > MAX_ON)
            {
               for(int j = 0; j < 8; j++) set_thruster(j, LOW);
               RCLCPP_ERROR(this->get_logger(), "The controller fucked up and commanded pulse > 2s. Shutting down ...");
               rclcpp::shutdown();
               exit(EXIT_FAILURE);
            }
        }

        /* Putting the command on the actual hardware */
        for(int i = 0; i < 8; i++)
        {
            int set_to = this->curr_force_cmd.force[i] > 0.5 ? HIGH : LOW;

            if(!this->currently_on[i] && set_to == HIGH)
            {
                this->currently_on[i] = true;
                this->time_stamps[i] = t;
            }
            else if(set_to == LOW)
            {
                this->currently_on[i] = false;
                this->time_stamps[i] = t;
            }
            /* Actually send the command */
            set_thruster(SIM_2_REAL[i], set_to);
        }
    }   
}

void ThrusterSetter::thrust_callback(const msg::SharedPtr f_msg)
{
    /* We received at least one message */
    if(!received_initial_msg) received_initial_msg = true;

    /* Logging the f_msg */
    RCLCPP_INFO(this->get_logger(), "Received f_msg [%f, %f, %f, %f, %f, %f, %f, %f]",
                                      f_msg->force[0], f_msg->force[1],
                                      f_msg->force[2], f_msg->force[3],
                                      f_msg->force[4], f_msg->force[5],
                                      f_msg->force[6], f_msg->force[7]);

    /* Store the message in the class variable */
    this->curr_force_cmd = *f_msg;
}

int main(int argc, char* argv[])
{
    /* Init ROS2 if its not already running */
    if(!rclcpp::ok())
    {
         rclcpp::init(argc, argv);
    }

    signal(SIGINT, mySigintHandler);
    rclcpp::spin(std::make_shared<ThrusterSetter>());
    return 0;
}
