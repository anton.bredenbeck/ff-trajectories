#include "common.hpp"

int main(int argc, char * argv[])
{
	std::cout << "Not using main function arguments argc: " << argc << " and argv " << argv << std::endl;
	init_gpio();

	for(int idx = 0; idx < 8; idx++)
	{
		/* Test some thrusters */
		std::cout << "Turning on thruster " << idx << " i.e. GPIO " << GPIO_PINS[idx] << "..." << std::endl;
		delay(100);
		set_thruster(idx, HIGH);
		delay(500); // 500 ms
		set_thruster(idx, LOW);
		delay(500); // 500 ms
		std::cout << "... and done" << std::endl;
	}
}

