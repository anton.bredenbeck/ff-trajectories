#include <rclcpp/rclcpp.hpp>
#include "reacsa_interfaces/msg/force.hpp"

int main(int argc, char const *argv[])
{
	if(!rclcpp::ok()) rclcpp::init(argc,argv);

	auto n = std::make_shared<rclcpp::Node>("BenchmarkPublisher");
	auto pub = n->create_publisher<reacsa_interfaces::msg::Force>("/benchmark_topic", 10);

	for(unsigned int i = 0; i <= 1e7; i++)
	{
		auto msg = reacsa_interfaces::msg::Force();
		msg.force = {0, 0, 0, 0, 0, 0, 0, 0};

		pub->publish(msg);
	} 

	return 0;
}