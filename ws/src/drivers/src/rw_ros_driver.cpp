/* A ROS2 node that offers to set the torque on the reaction wheel */
#include "rw_ros_driver.hpp"

void RWDriver::spin_up(const std::shared_ptr<reacsa_interfaces::srv::SpinUpRW::Request> request,
                      std::shared_ptr<reacsa_interfaces::srv::SpinUpRW::Response> response)
{
    if(std::fabs(request->des_vel) > this->MAX_VELOCITY *  60.0 / (2 * M_PI))
    {
        RCLCPP_INFO(this->get_logger(), "Request of %f RPM exceeds velocity limts. Not enforcing.",
                                        request->des_vel);
        response->success = false;
    }
    else
    {
        RCLCPP_INFO(this->get_logger(), "Spinning up to %f RPM.",
                                        request->des_vel);
        this->spun_up = false;
        this->idling_vel = request->des_vel * 2.0 * M_PI / 60.0;
        response->success = this->controller.sendVelocityCommandRPM((int)std::round(request->des_vel));
    }
}

void RWDriver::static_sigint_handler(int sig)
{
    instance->sigint_handler(sig);
}

void RWDriver::sigint_handler(int sig)
{
      RCLCPP_INFO(this->get_logger(), "Sending zero Velocity command and shutting down.");
      // Send command to shutdown the reaction wheel, i.e. no velocity
      this->controller.sendVelocityCommand(0.0);
      this->controller.disableReactionWheel();
      // All the default sigint handler does is call shutdown()
      rclcpp::shutdown();
      exit(sig);
}


float RWDriver::torque_to_vel(float torque, float dt)
{
    /* Numerically integrate to find the desired velocity if the given torque 
     * was applied for the specified time interval */
    return this->rw.rw_velocity + this->M_1_I * torque * dt;
}

void RWDriver::torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr t_msg)
{
    /* Reset the watchdog counter*/
    this->watchdog_counter = 0;
    /* Storing the current torque command */
    this->tau = *t_msg;
}

void RWDriver::act()
{
    if(this->watchdog_counter > WATCHDOG_LIMIT)
    {
        /* We haven't received torque messages in a while
         * reset the torque to zero */
        RCLCPP_WARN(this->get_logger(), "Haven't received a message in 100ms, setting torque to zero");
        this->tau.torque = 0.0;
    }

    /* Increase the counter */
    this->watchdog_counter++;
    /* Read out and store the current velocity */
    float curr_vel = 0.0;
    this->controller.readCurrentVelocity(curr_vel);
    this->rw.header.stamp = this->now();
    this->rw.header.frame_id = "vicon";
    /* If the bit-error (which occasionally happens) occures
     * we leave rw_vel untouched */
    this->rw.rw_velocity = std::fabs(curr_vel) > this->MAX_VELOCITY + 500 ?
                              rw.rw_velocity : curr_vel;

    /* If we're not yet spun up we don't interfere with torque
     * commands */
    if(!this->spun_up)
    {
       this->spun_up = std::fabs(curr_vel - this->idling_vel) < this->EPSILON;
       if(this->spun_up) RCLCPP_INFO(this->get_logger(), "Spun Up successfully");
    }
    else if(std::fabs(this->tau.torque) < 0.00001)
    {
        RCLCPP_INFO(this->get_logger(), "Torque below motor sensitivity. Not forwarding.");
    }
    else
    {
    	RCLCPP_DEBUG(this->get_logger(), "Setting Torque %f Nm", this->tau.torque);

    	float vel_des;
    	/* Compute velocity required to enact the desired torque. 
     	 * If we exceed the max possible torque we simply cap it to the max*/
    	if(std::fabs(this->tau.torque) > this->MAX_TORQUE)
    	{
        	RCLCPP_INFO(this->get_logger(), "Maximal torque exceeded by controller, exerting only max torque.");
        	vel_des = this->torque_to_vel(((this->tau.torque > 0) - (this->tau.torque < 0)) * this->MAX_TORQUE, this->DT);
    	}
    	/* Otherwise simply forward to the controller */
    	else
    	{
        	vel_des = this->torque_to_vel(this->tau.torque, this->DT);
    	}


        if(vel_des < -this->MAX_VELOCITY) vel_des = -this->MAX_VELOCITY;
        else if(vel_des > 0.0) vel_des = 0.0;
        
    	RCLCPP_DEBUG(this->get_logger(), "Corresponds to Velocity %f rad/s", vel_des);

    	/* Actually send the velocity command to the controller */
    	bool success = this->controller.sendVelocityCommand(vel_des);
    	if(!success) RCLCPP_WARN(this->get_logger(), "Couldn't Set Velocity!");
     }
     /* Actually publish the current velocity */
     this->rw_pub->publish(rw);
}

int main(int argc, char* argv[])
{
    /* Init ROS2 if its not already running */
    if(!rclcpp::ok())
    {
        rclcpp::init(argc, argv);
    }

    /* Link the sigint handler */
    signal(SIGINT, RWDriver::static_sigint_handler);
    /* Spin the ROS2 node */
    rclcpp::spin(std::make_shared<RWDriver>());

    return 0;
}
