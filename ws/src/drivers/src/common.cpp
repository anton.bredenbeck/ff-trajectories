#include "common.hpp"

void set_thruster(int idx, int value)
{
	/* Make sure we're allowed to do this, i.e
	 * thruster index and value are in the desired
	 * range. */
	assert(idx >= 0 && idx <= 7);
	assert(value == HIGH || value == LOW);

	digitalWrite(GPIO_PINS[idx], value);
}

void init_gpio()
{
	/* Setting up the GPIO library and pins*/
	wiringPiSetup();
	for(int i: GPIO_PINS) 
	{
		pinMode(i, OUTPUT);
		digitalWrite(i, LOW);
	}
}


