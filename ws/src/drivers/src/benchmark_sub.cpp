#include <chrono>
#include <rclcpp/rclcpp.hpp>

#include "common.hpp"
#include "reacsa_interfaces/msg/force.hpp"

unsigned int msg_count = 0;
static std::chrono::time_point<std::chrono::high_resolution_clock> t1;
static std::chrono::time_point<std::chrono::high_resolution_clock> t2;

void topic_callback(const reacsa_interfaces::msg::Force::SharedPtr msg)
{	
  if(msg_count == 0) t1 = std::chrono::high_resolution_clock::now();
  else if(msg_count >= 1e6)
  {
  	t2 = std::chrono::high_resolution_clock::now();

  	RCLCPP_INFO(rclcpp::get_logger("Timer"), "Duration for 1e6 messages: %d ms",
  		std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1));
  	rclcpp::shutdown();
  }

  /* Putting the command on the actual hardware */
  for(int i = 0; i < 8; i++) set_thruster(i, (msg->force[i] > 0.5 ? HIGH : LOW));

  msg_count++;
}

int main(int argc, char const *argv[])
{ 

	if(!rclcpp::ok()) rclcpp::init(argc,argv);

	auto n = std::make_shared<rclcpp::Node>("BenchmarkPublisher");
	auto sub = n->create_subscription<reacsa_interfaces::msg::Force>("/benchmark_topic", 10,
		std::bind(&topic_callback, std::placeholders::_1));
	rclcpp::spin(n);

	return 0;
}