from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()

    node_observer = Node(
        package='drivers',
        name='rw_driver',
        executable='rw_driver',
        parameters=[]
    )
    ld.add_action(node_observer)

    node_follow = Node(
        package='drivers',
        name='thruster_setter',
        executable='thruster_setter',
        parameters=[]
    )
    ld.add_action(node_follow)
    return ld
