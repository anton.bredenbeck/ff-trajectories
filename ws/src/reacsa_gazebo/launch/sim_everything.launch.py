"""Launch Gazebo with the orl world that has Reacsa in it,
   also launch the trajectory follower the ekf and send the action
   to follow the optimal trajectory."""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    config = os.path.join(
        get_package_share_directory('reacsa_control'),
        'config',
        'params.yaml'
    )

    ld = LaunchDescription()

    node_observer = Node(
        package='reacsa_control',
        name='ekf',
        executable='ekf',
        parameters=[config]
    )
    ld.add_action(node_observer)

    node_follow = Node(
        package='reacsa_control',
        name='trajectory_follower',
        executable='trajectory_follower',
        parameters=[config]
    )
    ld.add_action(node_follow)

    node_b2txt = Node(
        package='bag2txt',
        name='bag2txt',
        executable='bag2txt'
    )
    ld.add_action(node_b2txt)

    node_traj_planner = Node(
        package='reacsa_control',
        name='trajectory_planner',
        executable='trajectory_planner.py'
    )
    ld.add_action(node_traj_planner)

    return ld
