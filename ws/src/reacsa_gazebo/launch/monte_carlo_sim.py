#!/usr/bin/python3

import os
import time
import subprocess
import signal
import random
import numpy as np
import rclpy
from rclpy.action import ActionClient
from action_msgs.msg import GoalStatus

from reacsa_actions.action import ComputeTrajectory
from reacsa_actions.action import FollowTrajectory
from reacsa_interfaces.srv import PrepareTrajectory
from gazebo_msgs.srv import SetEntityState
from std_srvs.srv import Empty


class MonteCarloHandler(rclpy.node.Node):

    def __init__(self):
        super().__init__("monte_carlo_handler")

        self._comp_action_client = ActionClient(
            self, ComputeTrajectory, "/compute_trajectory")
        self.comp_status = GoalStatus.STATUS_EXECUTING

        self._flw_action_client = ActionClient(
            self, FollowTrajectory, "/follow_trajectory")
        self.flw_status = GoalStatus.STATUS_EXECUTING

        self._prepare_traj_client = self.create_client(PrepareTrajectory,
                                                       "/prepare_trajectory")
        while not self._prepare_traj_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/prepare_trajectory service not available,"
                                   + " waiting again...")

        self.mv_cli = self.create_client(SetEntityState,
                                         "/mc/set_entity_state")
        while not self.mv_cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/mc/set_entity_state service"
                                   + " not available, waiting again...")

        self.pause_gz = self.create_client(Empty, "/pause_physics")
        while not self.pause_gz.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/pause_physics not available,"
                                   + "waiting again...")

        self.unpause_gz = self.create_client(Empty, "/unpause_physics")
        while not self.unpause_gz.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/unpause_physics service not available,"
                                   + " waiting again...")

        self.reset_gz_sim = self.create_client(Empty, "/reset_simulation")
        while not self.reset_gz_sim.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/reset_simulation service not available,"
                                   + " waiting again...")

        self.reset_gz_world = self.create_client(Empty, "/reset_world")
        while not self.reset_gz_world.wait_for_service(timeout_sec=1.0):
            self.get_logger().info("/reset_world service not available,"
                                   + " waiting again...")

        self.reset_obs = self.create_client(Empty, "reset_observer")

    def reset_gz(self):
        self.get_logger().info("Reset Simulation and World")
        reset_world_future = self.reset_gz_world.call_async(Empty.Request())
        while not reset_world_future.done():
            rclpy.spin_once(self)

        reset_sim_future = self.reset_gz_sim.call_async(Empty.Request())
        while not reset_sim_future.done():
            rclpy.spin_once(self)

    def comp_send_goal(self, goal):
        self.comp_status = GoalStatus.STATUS_EXECUTING

        self._comp_action_client.wait_for_server()

        self._comp_send_goal_future =\
            self._comp_action_client.send_goal_async(
                goal,
                feedback_callback=self.comp_feedback_callback)

        self._comp_send_goal_future.add_done_callback(
            self.comp_goal_response_callback)

    def comp_goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info("Goal rejected :(")
            return

        self.get_logger().info("Computation Goal accepted")

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(
            self.comp_get_result_callback)

    def comp_get_result_callback(self, future):
        self.get_logger().info("Trajectory Found!")
        self.comp_status = GoalStatus.STATUS_SUCCEEDED

    def comp_feedback_callback(self, feedback_msg):
        self.get_logger().info("Received feedback")

    def flw_send_goal(self, goal):
        self.flw_status = GoalStatus.STATUS_EXECUTING

        self._flw_action_client.wait_for_server()

        self._flw_send_goal_future =\
            self._flw_action_client.send_goal_async(
                goal,
                feedback_callback=self.flw_feedback_callback)

        self._flw_send_goal_future.add_done_callback(
            self.flw_goal_response_callback)

    def flw_goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info("Goal rejected :(")
            return

        self.get_logger().info("Follow Goal accepted")

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.flw_get_result_callback)

    def flw_get_result_callback(self, future):
        self.get_logger().info("Trajectory successfully followed!")
        self.flw_status = GoalStatus.STATUS_SUCCEEDED

    def flw_feedback_callback(self, feedback_msg):
        self.get_logger().info("Received feedback")

    def pause_physics(self):
        self.get_logger().info("Pausing Physics")
        pause_future = self.pause_gz.call_async(Empty.Request())
        while not pause_future.done():
            rclpy.spin_once(self)

    def unpause_physics(self):
        self.get_logger().info("Unpausing Physics")
        unpause_future = self.unpause_gz.call_async(Empty.Request())
        while not unpause_future.done():
            rclpy.spin_once(self)

    def move_robot(self, request):
        self.get_logger().info("Moving Robot")
        mv_cli_future = self.mv_cli.call_async(request)
        while not mv_cli_future.done():
            rclpy.spin_once(self)

    def sleep(self, seconds):
        time.sleep(seconds)


def delete_files():
    if os.path.exists("./force.txt"):
        os.remove("./force.txt")
    if os.path.exists("./torque.txt"):
        os.remove("./torque.txt")
    if os.path.exists("./rw.txt"):
        os.remove("./rw.txt")
    if os.path.exists("./pose.txt"):
        os.remove("./pose.txt")
    if os.path.exists("./obsv_state.txt"):
        os.remove("./obsv_state.txt")
    if os.path.exists("././src/reacsa_gazebo/log/q.log"):
        os.remove("./src/reacsa_gazebo/log/q.log")
    if os.path.exists("./src/reacsa_gazebo/log/rw.log"):
        os.remove("./src/reacsa_gazebo/log/rw.log")
    if os.path.exists("./src/reacsa_gazebo/log/u.log"):
        os.remove("./src/reacsa_gazebo/log/u.log")


def main():
    num_sim = 100

    if not os.path.exists("monte_carlo"):
        os.makedirs("monte_carlo")

    if(not rclpy.ok()):
        rclpy.init()

    mc = MonteCarloHandler()

    # Launch all control nodes
    # bash_command = "ros2 launch reacsa_gazebo sim_everything.launch.py"
    # sim_everything = subprocess.Popen(bash_command.split())

    while not mc.reset_obs.wait_for_service(timeout_sec=1.0):
        mc.get_logger().info("service not available, waiting again...")

    i = 0
    while i < num_sim:
        mc.get_logger().info(f"Starting trial {i}")

        # Pause Physics
        mc.pause_physics()

        if not os.path.exists(f"monte_carlo/trial{i:02}"):
            os.makedirs(f"monte_carlo/trial{i:02}")

        x = random.uniform(-2.0, 2.0)
        x_dot = 0.0  # random.uniform(-0.01, 0.01)
        y = random.uniform(-4.0, 4.0)
        y_dot = 0.0  # random.uniform(-0.01, 0.01)
        theta = random.uniform(-np.pi, np.pi)
        theta_dot = 0.0  # random.uniform(- 0.01, 0.01)

        goal = ComputeTrajectory.Goal()

        goal.init.pose.pose.position.x = x
        goal.init.pose.pose.position.y = y
        if np.sin(theta / 2) < 0:
            goal.init.pose.pose.orientation.x = 0.0
            goal.init.pose.pose.orientation.y = 0.0
            goal.init.pose.pose.orientation.z = -np.sin(theta / 2)
            goal.init.pose.pose.orientation.w = -np.cos(theta / 2)
        else:
            goal.init.pose.pose.orientation.x = 0.0
            goal.init.pose.pose.orientation.y = 0.0
            goal.init.pose.pose.orientation.z = np.sin(theta / 2)
            goal.init.pose.pose.orientation.w = np.cos(theta / 2)
        goal.init.twist.twist.linear.x = x_dot
        goal.init.twist.twist.linear.y = y_dot
        goal.init.twist.twist.angular.z = theta_dot
        goal.init.rw_vel = 0.0

        goal.final.pose.pose.position.x = 0.0
        goal.final.pose.pose.position.y = 0.0
        goal.final.pose.pose.orientation.x = 0.0
        goal.final.pose.pose.orientation.y = 0.0
        goal.final.pose.pose.orientation.z = 0.0
        goal.final.pose.pose.orientation.w = 1.0
        goal.final.rw_vel = 0.0

        goal.num_points = 100

        mc.get_logger().info("Sending Action Request (Compute Trajectory)...")
        mc.comp_send_goal(goal)
        while mc.comp_status != GoalStatus.STATUS_SUCCEEDED:
            rclpy.spin_once(mc)
        mc.get_logger().info("... done.")

        if len(np.loadtxt("src/reacsa_control/trajectories/x_star.txt")) <= 7:
            mc.get_logger().info("The trajectory couldn't be found."
                                 + " Skipping...")
            mc.unpause_physics()
            delete_files()
            continue

        # Reset the simulation
        mc.reset_gz()

        # Reset the robot state
        mc.get_logger().info(f"Reset Robot State (x: {x:.4}, y: {y:.4},"
                             + f" theta: {theta * 180 / np.pi:.4})"
                             + " and Delete Log files.")
        mv_req = SetEntityState.Request()
        mv_req.state.name = "reacsa"
        mv_req.state.pose.position.x = x
        mv_req.state.pose.position.y = y
        mv_req.state.pose.position.z = 0.1
        if np.sin(theta / 2) < 0:
            mv_req.state.pose.orientation.x = 0.0
            mv_req.state.pose.orientation.y = 0.0
            mv_req.state.pose.orientation.z = -np.sin(theta / 2)
            mv_req.state.pose.orientation.w = -np.cos(theta / 2)
        else:
            mv_req.state.pose.orientation.x = 0.0
            mv_req.state.pose.orientation.y = 0.0
            mv_req.state.pose.orientation.z = np.sin(theta / 2)
            mv_req.state.pose.orientation.w = np.cos(theta / 2)
        mv_req.state.twist.linear.x = x_dot
        mv_req.state.twist.linear.y = y_dot
        mv_req.state.twist.angular.z = theta_dot

        mc.move_robot(mv_req)

        # Unpause physics so that the move service is executed
        mc.unpause_physics()

        # Give the Kalman filter time to settle in
        t = mc.get_clock().now()
        mc.get_logger().info("Waiting for Kalman Filter to settle in...")
        while (mc.get_clock().now() - t).nanoseconds < 0.2e9:
            pass
        mc.get_logger().info("... done.")

        # Pause Physics again
        mc.pause_physics()

        # Prepare trajectory
        mc.get_logger().info("Setting up action goal for traj follower.")
        prep_req = PrepareTrajectory.Request()
        prep_req.state_filename = "src/reacsa_control/trajectories/x_star.txt"
        prep_req.ctrl_filename = "src/reacsa_control/trajectories/u_star.txt"
        prep_req.time_filename = "src/reacsa_control/trajectories/t_star.txt"
        mc.get_logger().info("Sending Service Request...")
        prep_future = mc._prepare_traj_client.call_async(prep_req)
        while not prep_future.done():
            rclpy.spin_once(mc)

        # Delte log files before restarting physics
        mc.move_robot(mv_req)
        delete_files()

        # Unpause physics just before executing the trajectory
        mc.unpause_physics()

        # Starting the trajectory following
        mc.get_logger().info("Starting to follow the trajectory.")
        flw_goal = FollowTrajectory.Goal()
        mc.flw_send_goal(flw_goal)

        # Waiting for goal to finish
        mc.get_logger().info("Waiting for goal to finish...")
        while mc.flw_status != GoalStatus.STATUS_SUCCEEDED:
            rclpy.spin_once(mc)

        mc.get_logger().info("... done.")

        # Move all the logs to the respective folder
        os.rename("./force.txt", f"./monte_carlo/trial{i:02}/force.txt")
        os.rename("./obsv_state.txt",
                  f"./monte_carlo/trial{i:02}/obsv_state.txt")
        os.rename("./pose.txt", f"./monte_carlo/trial{i:02}/pose.txt")
        os.rename("./rw.txt", f"./monte_carlo/trial{i:02}/rw.txt")
        os.rename("./torque.txt", f"./monte_carlo/trial{i:02}/torque.txt")
        os.rename("./src/reacsa_gazebo/log/q.log",
                  f"./monte_carlo/trial{i:02}/q.log")
        os.rename("./src/reacsa_gazebo/log/rw.log",
                  f"./monte_carlo/trial{i:02}/rw.log")
        os.rename("./src/reacsa_gazebo/log/u.log",
                  f"./monte_carlo/trial{i:02}/u.log")

        # Increment counter
        i = i + 1

    # sim_everything.send_signal(signal.SIGINT)


if __name__ == "__main__":
    main()
