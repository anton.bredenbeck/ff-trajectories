<?xml version='1.0'?>
<sdf version='1.4'>
  <model name="reacsa">
    <!-- If true, physics engine will ignore -->
    <static>false</static>

    <!-- Main robot body -->
    <link name='chassis'>
      <!--Position and orientation of the center of the robot chassis-->
      <pose>0 0 0.5 0 0 0</pose>

      <!-- Specify the shape and surface properties for collisions -->
      <collision name='collision'>
        <geometry>
          <!-- Define the robot chassis as a uniform cylinder -->
          <cylinder>
            <length> 1</length>
            <radius> 0.325</radius>
          </cylinder>
        </geometry>

        <surface>
          <!-- Make the cylinder frictionless -->
          <friction>
            <ode>
              <mu>0.0</mu>
              <mu2>0.0</mu2>
              <slip1>1.0</slip1>
              <slip2>1.0</slip2>
            </ode>
          </friction>
        </surface>
      </collision>

      <!-- Specify the shape for visualization -->
      <visual name='visual'>
        <geometry>
          <cylinder>
            <length> 1</length>
            <radius> 0.325</radius>
          </cylinder>
        </geometry>
      </visual>

      <!-- Specify the inertial parameters of the robot chassis -->
      <inertial>
        <!-- CoM in the middle of the cylinder -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the overall system excluding the reaction wheel -->
        <mass> 217.66 </mass>
        <!-- Only relevant inertia: About z hence all other computed as if reacsa is an ideal cylinder  -->
        <inertia>
          <ixx>24.3314</ixx>
          <ixy>1e-7</ixy>
          <ixz>1e-7</ixz>
          <iyy>24.3314</iyy>
          <iyz>1e-7</iyz>
          <izz>11.506</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Reaction wheel -->
    <link name="reaction_wheel">
      <pose>0 0 1.2 0 0 0</pose>

      <collision name="collision">
        <geometry>
          <cylinder>
            <radius>0.148</radius>
            <length>0.05</length>
          </cylinder>
        </geometry>
      </collision>

      <visual name="visual">
        <geometry>
          <cylinder>
            <radius>0.148</radius>
            <length>0.05</length>
          </cylinder>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the cylinder -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the reaction wheel -->
        <mass> 4.29 </mass>
        <!-- Only relevant inertia: About z hence all other computed as if the reaction wheel is an
              ideal cylinder -->
        <inertia>
          <ixx>0.02438</ixx>
          <ixy>1e-7</ixy>
          <ixz>1e-7</ixz>
          <iyy>0.02438</iyy>
          <iyz>1e-7</iyz>
          <izz>0.047</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 0 -->
    <link name='thruster0'>
      <!--Thruster is positioned to +x pointing -y -->
      <pose> 0.3 -0.05 1.025 0 0 -1.570796 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 1 -->
    <link name='thruster1'>
      <!--Thruster is positioned to +x pointing +y -->
      <pose> 0.3 0.05 1.025 0 0 1.570796 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 2 -->
    <link name='thruster2'>
      <!--Thruster is positioned to +y pointing +x -->
      <pose> 0.05 0.3 1.025 0 0 0 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 3 -->
    <link name='thruster3'>
      <!--Thruster is positioned to +y pointing -x -->
      <pose> -0.05 0.3 1.025 0 0 3.141592654 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 4 -->
    <link name='thruster4'>
      <!--Thruster is positioned to -x pointing +y -->
      <pose> -0.3 0.05 1.025 0 0 1.570796 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 5 -->
    <link name='thruster5'>
      <!--Thruster is positioned to -x pointing -y -->
      <pose> -0.3 -0.05 1.025 0 0 -1.570796 </pose>
      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 6 -->
    <link name='thruster6'>
      <!--Thruster is positioned to -y pointing -x -->
      <pose> -0.05 -0.3 1.025 0 0 3.141592654 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Thruster 7 -->
    <link name='thruster7'>
      <!--Thruster is positioned to -y pointing +x -->
      <pose> 0.05 -0.3 1.025 0 0 0 </pose>

      <collision name='collision'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </collision>

      <visual name='visual'>
        <geometry>
          <box>
            <size> 0.1 0.05 0.05 </size>
          </box>
        </geometry>
      </visual>

      <inertial>
        <!-- CoM in the middle of the thruster -->
        <pose> 0 0 0 0 0 0 </pose>
        <!-- Mass of the thruster is negligible -->
        <mass> 1e-7 </mass>
        <inertia>
          <ixx>1e-7</ixx>
          <ixy>1e-10</ixy>
          <ixz>1e-10</ixz>
          <iyy>1e-7</iyy>
          <iyz>1e-10</iyz>
          <izz>1e-7</izz>
        </inertia>
      </inertial>
    </link>

    <!-- Linking Reaction Wheel to the robot chassis -->
    <joint type="revolute" name="reaction_wheel_hinge">
        <pose>0 0 1.0 0 0 0</pose>
        <child>reaction_wheel</child>
        <parent>chassis</parent>
        <axis>
          <xyz>0 0 1</xyz>
          <dynamics>
            <damping>1e-7</damping>
          </dynamics>
          <limit>
            <velocity> 52.3598 <!-- = 500 RPM--> </velocity>
          </limit>
        </axis>
    </joint>

    <!-- Linking Thruster 0 to the robot chassis -->
    <joint type="fixed" name="thruster0_joint">
      <pose> 0 0.30 1 0 0 0 </pose>
      <child> thruster0 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster1 to the chassis -->
    <joint type="fixed" name="thruster1_joint">
      <pose> 0 0.30 1 0 0 0 </pose>
      <child> thruster1 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster2 to the chassis -->
    <joint type="fixed" name="thruster2_joint">
      <pose> 0.30 0 1 0 0 0 </pose>
      <child> thruster2 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster3 to the chassis -->
    <joint type="fixed" name="thruster3_joint">
      <pose> 0.30 0 1 0 0 0 </pose>
      <child> thruster3 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster 4 to the chassis -->
    <joint type="fixed" name="thruster4_joint">
      <pose> 0 -0.30 1 0 0 0 </pose>
      <child> thruster4 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster5 to the chassis -->
    <joint type="fixed" name="thruster5_joint">
      <pose> 0 -0.30 1. 0 0 0 </pose>
      <child> thruster5 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster6 to the chassis -->
    <joint type="fixed" name="thruster6_joint">
      <pose> -0.30 0 1 0 0 0 </pose>
      <child> thruster6 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Linking Thruster7 to the chassis -->
    <joint type="fixed" name="thruster7_joint">
      <pose> -0.30 0 1 0 0 0 </pose>
      <child> thruster7 </child>
      <parent> chassis </parent>
    </joint>

    <!-- Adding the plugin to control force and torque -->
    <plugin name="ft_setter" filename="libft_setter.so">
      <initial_velocity> -250.0 </initial_velocity>
    </plugin>

    <!-- Adding the plugin to publish pose of robot -->
    <plugin name="vicon_publisher" filename="libvicon_publisher.so">
      <position_noise_mean>
        <x> 0.0 </x>
        <y> 0.0 </y>
        <z> 0.0 </z>
      </position_noise_mean>
      <!-- This matrix is symmetric, so only upper half specified -->
      <position_noise_covariance>
        <xx> 0.00001 </xx>
        <xy> 0.0 </xy>
        <xz> 0.0 </xz>
        <yy> 0.00001 </yy>
        <yz> 0.0 </yz>
        <zz> 0.0 </zz>
      </position_noise_covariance>
      <orientation_noise_mean>
        <x> 0 </x>
        <y> 0 </y>
        <z> 0 </z>
      </orientation_noise_mean>
      <!-- This matrix is symmetric, so only upper half specified -->
      <orientation_noise_covariance>
        <xx> 0.0 </xx>
        <xy> 0.0 </xy>
        <xz> 0.0 </xz>
        <yy> 0.0 </yy>
        <yz> 0.0 </yz>
        <zz> 0.00001 </zz>
      </orientation_noise_covariance>
    </plugin>

    <!-- Adding the plugin to simulate realistic motor behavior -->
    <plugin name="rw_motor" filename="librw_motor.so">
      <mean> 0.0 </mean>
      <stddev> 0.01 </stddev>
    </plugin>

  </model>
</sdf>
