
### Excel data ###
Max x = 4.500706263 m, min x = -0.000859829 m, difference = 4.501566092 m
Max y = 8.500873408 m, min y = -0.000897879 m, difference = 8.501771287 m
Max z = 0.000324156 m, min z = -0.000753734 m, difference = 0.00107789 m
### Image data (image coordinate system) ###
Image size = (10.0,10.0) m
Middle image coordinate: (5.0,5.0) m
Floor corner coordinates: (0.625795,0.625795) m, (5.375795,5.375795) m
Floor coordinate system: (0.750012,0.750012)
Middle floor coordinate: (3.000795,5.015795)
### Floor data (floor coordinate system) ###
Image corner coodinates: (-0.750012,-0.750012) m, (9.249988,9.249988) m
Middle image coordinate: [4.249988,4.249988] m
Floor corner coodinates: (-0.124217,-0.124217) m, 4.625783,4.625783 m, x length = 4.75 m, y length = 8.78 m
Middle floor coordinate: (2.250783,4.265783)

Heightmap_513
### Interpolated data ###
Max z = 0.000308 m, min z = -0.000767 m, difference = 0.001074 m
### Image data (image coordinate system) ###
Resolution: 19.493177 mm/px

Heightmap_257
### Interpolated data ###
Max z = 0.000296 m, min z = -0.000766 m, difference = 0.001062 m
### Image data (image coordinate system) ###
Resolution: 38.910506 mm/px

Heightmap_129
### Interpolated data ###
Max z = 0.000306 m, min z = -0.000764 m, difference = 0.001070 m
### Image data (image coordinate system) ###
Resolution: 77.519380 mm/px