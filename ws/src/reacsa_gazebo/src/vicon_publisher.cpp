/* This plugin is attached to a Link and publishes the 6d pose 
 * of said link on 
 * 	/vicon/pose/
 * Which emulates being tracked by the vicon system. 
 * The pose is then subjected to noise. In particular,
 *  - positon is subjected to random disturbances drawn from a 3d Gaussian distribution
 *  - orientation is subjected to TODO
 */

#include <memory>
#include <thread>

#include "gazebo/common/common.hh"
#include "gazebo/physics/physics.hh"

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/header.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"

#include "common.hpp"

/* A Ros Node that provides services to set the gazebo simulation values of torque and force 
 * to the desired torque and forces. In particular, it stores the values internally and during the 
 * the next simulation step applies them.*/
class ViconPublisherRos2Node : public rclcpp::Node
{
  public:
    /* Constructor: Calls the super constructor (Node) to init it as a ros node and sets default 
     * values of rw_torque and thruster_force. Lastly initializes the services. */
    ViconPublisherRos2Node():
    rclcpp::Node("vicon_publisher")
    {
      pose_pub = this->create_publisher<geometry_msgs::msg::PoseWithCovarianceStamped>("/vicon/reacsa/reacsa",
                                                             /* QoS */ rclcpp::SensorDataQoS());
      /* Init the timer that regularly calls the publish function */
      pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(10),
            std::bind(&ViconPublisherRos2Node::publish_pose, this));
    }

    void publish_pose()
    {
      this->curr_pose.header.stamp = this->now();
      this->curr_pose.header.frame_id = "vicon";

      /* Enforce positivity of rotational axis. Enforces the sense of 
       * rotation to be consitent  */
      if(this->curr_pose.pose.pose.orientation.z < 0)
      {
        this->curr_pose.pose.pose.orientation.x *= -1;
        this->curr_pose.pose.pose.orientation.y *= -1;
        this->curr_pose.pose.pose.orientation.z *= -1;
        this->curr_pose.pose.pose.orientation.w *= -1;
      }

      this->pose_pub->publish(this->curr_pose);
    }


    void set_pose(geometry_msgs::msg::PoseWithCovariance pose)
    {
      this->curr_pose.pose = pose;
    }
  private:

    /* Class member variables: the pose publisher publishers */
    geometry_msgs::msg::PoseWithCovarianceStamped curr_pose;
    rclcpp::Publisher<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr pose_pub;
    rclcpp::TimerBase::SharedPtr pub_timer;
};

namespace gazebo
{
  /* A Gazebo model plugin that emulates being tracked by a vicon system and publishes the pose
   * on a ros2 topic */
  class ViconPublisher : public ModelPlugin
  {
  
  public:
  
    /* Entry point for gazebo. Called when instantiating the model initially */
    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) override
    {
      /* Store the pointer to the model */
      this->model = _parent;

      /* Listen to the update event. This event is broadcast at every
       * end of a simulation iteration. */
      this->updateConnection = event::Events::ConnectWorldUpdateEnd(
          std::bind(&ViconPublisher::OnUpdate, this));


      /* Init the random distributions */
      if(_sdf->HasElement("position_noise_mean"))
      {
        position_noise_mean = Eigen::Vector3d(_sdf->GetElement("position_noise_mean")->Get<double>("x"), 
                                             _sdf->GetElement("position_noise_mean")->Get<double>("y"),
                                             _sdf->GetElement("position_noise_mean")->Get<double>("x"));
      }
      if(_sdf->HasElement("position_noise_covariance"))
      {
        /* Since the covariance matrix is symmetric we only specify the upper triangle matrix in the sdf mode 
         * but store a full matrix for computations */
        position_noise_covariance << _sdf->GetElement("position_noise_covariance")->Get<double>("xx"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("xy"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("xz"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("yx"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("yy"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("yz"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("xz"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("yz"),
                                     _sdf->GetElement("position_noise_covariance")->Get<double>("zz");
      }

      if(_sdf->HasElement("orientation_noise_mean"))
      {
        orientation_noise_mean = Eigen::Vector3d(_sdf->GetElement("orientation_noise_mean")->Get<double>("x"), 
                                                 _sdf->GetElement("orientation_noise_mean")->Get<double>("y"),
                                                 _sdf->GetElement("orientation_noise_mean")->Get<double>("x"));
      }
      if(_sdf->HasElement("orientation_noise_covariance"))
      {
        /* Since the covariance matrix is symmetric we only specify the upper triangle matrix in the sdf mode 
         * but store a full matrix for computations */
        orientation_noise_covariance << _sdf->GetElement("orientation_noise_covariance")->Get<double>("xx"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("xy"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("xz"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("yx"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("yy"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("yz"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("xz"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("yz"),
                                        _sdf->GetElement("orientation_noise_covariance")->Get<double>("zz");
      }
      
      
      /* Initialize ros, if it has not already been initialized. */
      if (!rclcpp::ok())
      {
        int argc = 0;
        char **argv = NULL;
        rclcpp::init(argc, argv);
      }

      /* Instantiate the ros node, and create the publishers. */
      this->ros2node = std::make_shared<ViconPublisherRos2Node>();

      /* Instantiate the executor */
      this->executor = std::make_shared<rclcpp::executors::SingleThreadedExecutor>();
      /* Add the node to the executor ... */
      this->executor->add_node(this->ros2node);

      /* ... executing this task/loop (aka while ros is running spin and check each time 
       *  if ros is actually still running)  .....*/
      auto spin = [this]()
      {
        while(rclcpp::ok())
        {
          this->executor->spin_once();
        }
      };

      /* ... in this thread. */
      this->thread_executor_spin_ = std::thread(spin);
    }

    /* This function is called at the end of every simulation iteration step, as we bound it to said event
     * We update the current publish the resulting pose to the respective ros topics */
    void OnUpdate()
    {
      /* Get the pose from the gazebo model */
      physics::LinkPtr chassis_model = this->model->GetLink("chassis");
      Eigen::Vector3d curr_position(chassis_model->WorldPose().Pos().X(),
                                    chassis_model->WorldPose().Pos().Y(),
                                    chassis_model->WorldPose().Pos().Z());

      Eigen::Quaternion curr_orientation(chassis_model->WorldPose().Rot().W(),
                                         chassis_model->WorldPose().Rot().X(),
                                         chassis_model->WorldPose().Rot().Y(),
                                         chassis_model->WorldPose().Rot().Z());
      curr_orientation.normalize();


      /* Init Message */
      geometry_msgs::msg::PoseWithCovariance ros2_pose;

      /* TODO find proper expression for covariance */
      ros2_pose.covariance = {0,0,0,0,0,0,
                              0,0,0,0,0,0,
                              0,0,0,0,0,0,
                              0,0,0,0,0,0,
                              0,0,0,0,0,0,
                              0,0,0,0,0,0};

      ros2_pose.pose.position.x = curr_position(0);
      ros2_pose.pose.position.y = curr_position(1);
      ros2_pose.pose.position.z = curr_position(2);

      ros2_pose.pose.orientation.x = curr_orientation.z() < 0 ? 
          - curr_orientation.x() : curr_orientation.x();
      ros2_pose.pose.orientation.y = curr_orientation.z() < 0 ? 
          - curr_orientation.y() : curr_orientation.y();
      ros2_pose.pose.orientation.z = curr_orientation.z() < 0 ? 
          - curr_orientation.z() : curr_orientation.z();
      ros2_pose.pose.orientation.w = curr_orientation.z() < 0 ? 
          - curr_orientation.w() : curr_orientation.w();
      /* ... done. */

      if(this->LOG)
      {
        std::ofstream f;
        std::string path = "src/reacsa_gazebo/log/q.log";
        f.open(path , std::ios_base::app); // append instead of overwrite
        if(!f)
        {
          throw std::runtime_error("Couldn't open log file " + path);
        }

        long int t = this->ros2node->now().nanoseconds();

        f << t << " " << ros2_pose.pose.position.x << " "
                      << ros2_pose.pose.position.y << " "
                      << 2 * std::acos(ros2_pose.pose.orientation.w) << std::endl;

        f.close();
      }

      /* Subject the messages to noise */
      this->subject_to_noise(ros2_pose);

      /* Store the message */
      this->ros2node->set_pose(ros2_pose);
    }

    void subject_to_noise(geometry_msgs::msg::PoseWithCovariance & pose)
    {
      /* Sample multivariate normal distribution and add the noise to the position */
      Eigen::Vector3d position_noise = sample_mv_normal(this->position_noise_mean,
                                                        this->position_noise_covariance);
      pose.pose.position.x += position_noise(0);
      pose.pose.position.y += position_noise(1);
      pose.pose.position.z += position_noise(2);

      /* Sample multivariat normal distribution (in rotation space, i.e. rot around x,y,z
       * then transform to a quaternion and multiply the true quaternion by it. The result 
       * is then the noisy quaternion */
      Eigen::Vector3d orientation_noise = sample_mv_normal(this->orientation_noise_mean,
                                                           this->orientation_noise_covariance);
      Eigen::Quaternion curr_orientation(pose.pose.orientation.w,
                                         pose.pose.orientation.x,
                                         pose.pose.orientation.y,
                                         pose.pose.orientation.z); 
      Eigen::Quaternion orientation_noise_quat = to_quaternion(orientation_noise(0),
                                                               orientation_noise(1),
                                                               orientation_noise(2));

      /* Apply noise to orientation. I.e. we rotate true pose by the noise, therefore quaternion rotation.*/
      Eigen::Quaternion noisy_orientation = curr_orientation * orientation_noise_quat;

      /* Store to message */
      pose.pose.orientation.x = noisy_orientation.x();
      pose.pose.orientation.y = noisy_orientation.y();
      pose.pose.orientation.z = noisy_orientation.z();
      pose.pose.orientation.w = noisy_orientation.w();
    }
    
  private:
      /* Pointer to the model */
      physics::ModelPtr model;
      /* Pointer to the update event connection */
      event::ConnectionPtr updateConnection;
      /* ros2 node pointer*/
      std::shared_ptr<ViconPublisherRos2Node> ros2node;
      /* Executor for the ros2 node */
      std::shared_ptr<rclcpp::executors::SingleThreadedExecutor> executor;
      /* Thread where the executor will spin */
      std::thread thread_executor_spin_;

      /* Define the distribution from which the noise is sampled */
      Eigen::Vector3d position_noise_mean;
      Eigen::Matrix3d position_noise_covariance;
      Eigen::Vector3d orientation_noise_mean;
      Eigen::Matrix3d orientation_noise_covariance;

      /* Bool whether pose is logged to file */
      const bool LOG=true;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(ViconPublisher)
}

