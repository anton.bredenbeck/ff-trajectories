/* This node provides the services /set_torque and /set_force
 * and sets them at the reaction wheel motor joint and thruster
 * propulsion joint immediately in the gazebo model. */

#include <array>
#include <memory>
#include <atomic>
#include <thread>
#include <fstream>
#include <ament_index_cpp/get_package_share_directory.hpp>

#include "gazebo/common/common.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/Node.hh"
#include "gazebo/transport/TransportTypes.hh"

#include "common.hpp"
#include "rclcpp/rclcpp.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/msg/force.hpp"


const double RPM2RADPS = (2 * 3.1415926) / 60.0;

/* A Ros Node that provides services to set the gazebo simulation values of torque and force 
 * to the desired torque and forces. In particular, it stores the values internally and during the 
 * the next simulation step applies them.*/
class FtSetterRos2Node : public rclcpp::Node
{
  public:
    /* Constructor: Calls the super constructor (Node) to init it as a ros node and sets default 
     * values of rw_torque and thruster_force. Lastly initializes the services. */
    FtSetterRos2Node():
    rclcpp::Node("ft_setter"),
    rw_torque(0),
    thruster_force{0,0,0,0,0,0,0,0}
    {
      torque = this->create_subscription<reacsa_interfaces::msg::Torque>(
        "/set_torque", /* QoS */ rclcpp::SensorDataQoS(),
         std::bind(&FtSetterRos2Node::torque_callback, this,
                                std::placeholders::_1));
      force = this->create_subscription<reacsa_interfaces::msg::Force>(
        "/set_force", /* QoS */ rclcpp::SensorDataQoS(),
         std::bind(&FtSetterRos2Node::force_callback, this,
                                std::placeholders::_1));
    }

    /* Accessors for private variables */
    double get_rw_torque() {return this->rw_torque.load();}
    std::array<double,8> get_thruster_force() {
      std::array<double, 8> vals;
      for(int i = 0; i < 8; i ++)
      {
        vals[i] = this->thruster_force[i].load();
      }
      return vals;
    }

    rclcpp::Time get_tau_ts(){return this->tau_ts;}
    rclcpp::Time get_f_ts(){return this->f_ts;}

  private:

    /* Callback function that stores the newest service call */ 
    void torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr msg)
    {
      this->rw_torque = msg->torque;
      this->tau_ts = this->now();
      RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "Received Torque: %f", this->rw_torque.load());
    }

    /* Callback function that stores the newest service call */
    void force_callback(const reacsa_interfaces::msg::Force::SharedPtr msg)
    {
      this->f_ts = this->now();
      for(int i = 0; i < 8; i++)
      {
        this->thruster_force[i] = msg->force[i];
      }
      RCLCPP_DEBUG(rclcpp::get_logger("rclcpp"), "Received Force: %f,%f,%f,%f,%f,%f,%f,%f",
                   this->thruster_force.at(0).load(),this->thruster_force.at(1).load(),this->thruster_force.at(2).load(),
                   this->thruster_force.at(3).load(),this->thruster_force.at(4).load(),this->thruster_force.at(5).load(),
                   this->thruster_force.at(6).load(),this->thruster_force.at(7).load());
    }

    /* Class member variables: the two service handles and the most recent values */
    rclcpp::Subscription<reacsa_interfaces::msg::Torque>::SharedPtr torque;
    rclcpp::Subscription<reacsa_interfaces::msg::Force>::SharedPtr force;
    rclcpp::Time tau_ts, f_ts;
    std::atomic<double> rw_torque;
    std::array<std::atomic<double>,8> thruster_force;
};

namespace gazebo
{
  /* A Gazebo model plugin that sets the torque and force of the REACSA model to the 
   * values stored by the FtSetterRos2Node instance (Service Server) 
   * that is a member to this class */
  class FtSetter : public ModelPlugin
  {
  
  public:
  
    /* Entry point for gazebo. Called when instantiating the model initially */
    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) override
    {
      /* Store the pointer to the model */
      this->model = _parent;

      /* Listen to the update event. This event is broadcast every
       * simulation iteration. */
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&FtSetter::OnUpdate, this));

      /* Initialize ros, if it has not already been initialized. */
      if (!rclcpp::ok())
      {
        int argc = 0;
        char **argv = NULL;
        rclcpp::init(argc, argv);
      }

      /* Instantiate the ros node and executor */
      this->rosNode = std::make_shared<FtSetterRos2Node>();
      this->executor = std::make_shared<rclcpp::executors::SingleThreadedExecutor>();
      /* Add the node to the executer ... */
      this->executor->add_node(this->rosNode);

      /* Set Initial Velocity on the RW joint */
      if(_sdf->HasElement("initial_velocity"))
      {
        initial_velocity =_sdf->Get<double>("initial_velocity");
      }
      this->model->GetJoint("reaction_wheel_hinge")->SetVelocity(0, initial_velocity *  RPM2RADPS);
      
      /* ... executing this task/loop (aka while ros is running spin and check each time 
       *  if ros is actually still running)  .....*/
      auto spin = [this]()
      {
        while(rclcpp::ok())
        {
          this->executor->spin_once();
        }
      };

      /* ... in this thread. */
      this->thread_executor_spin_ = std::thread(spin);
    }

    /* This function is called on every simulation iteration step, as we bound it to said even
     * We update the current forces and torques on the respective joints and links as specified
     * by the values in out ros node */
    void OnUpdate()
    {
      /* Store all the force values, such that all thrusters are updated at the same time */
      std::array<double,8> forces = this->rosNode->get_thruster_force();
      double tau = this->rosNode->get_rw_torque();

      if((rclcpp::Clock().now() - rclcpp::Time(this->rosNode->get_tau_ts().nanoseconds())).seconds() > 0.5)
      {
        RCLCPP_WARN(this->rosNode->get_logger(), "No torque message for more than half a second. Setting to zero...");
        tau = 0.0;
      }


      if((rclcpp::Clock().now() - rclcpp::Time(this->rosNode->get_f_ts().nanoseconds())).seconds() > 0.55)
      {
        RCLCPP_WARN(this->rosNode->get_logger(), "No force message for more than half a second. Setting to zero...");
        forces = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      }

      
      if(this->LOG)
      {
        std::ofstream f;
        std::string path = "src/reacsa_gazebo/log/u.log";
        f.open(path , std::ios_base::app); // append instead of overwrite

        if(!f)
        {
          throw std::runtime_error("Couldn't open log file " + path);
        }

        long int t = this->rosNode->now().nanoseconds();

        f << t << " " << tau << " "
                      << forces.at(0) << " "
                      << forces.at(1) << " "
                      << forces.at(2) << " "
                      << forces.at(3) << " "
                      << forces.at(4) << " "
                      << forces.at(5) << " "
                      << forces.at(6) << " "
                      << forces.at(7) << std::endl;

        f.close();
      }
      /* If we haven't saturated at 0 RPM and are commanding 
       * a torque in the same direction set RW Torque.
       * Upper limit is handeled by sdf definition */
      if(this->model->GetJoint("reaction_wheel_hinge")->GetVelocity(0) >= 0.0 &&
         tau >= 0)
      {
        this->model->GetJoint("reaction_wheel_hinge")->SetForce(0, 0.0);
      }
      else
      {
        this->model->GetJoint("reaction_wheel_hinge")->SetForce(0, tau);
      }
      /* Add it to the model. The direction needs to be inversed because the thrust vector is opposite 
       * thrust pointing direction */
      this->model->GetLink("thruster0")->AddRelativeForce(ignition::math::Vector3(-forces.at(0),0.0, 0.0));
      this->model->GetLink("thruster1")->AddRelativeForce(ignition::math::Vector3(-forces.at(1),0.0, 0.0));
      this->model->GetLink("thruster2")->AddRelativeForce(ignition::math::Vector3(-forces.at(2),0.0, 0.0));
      this->model->GetLink("thruster3")->AddRelativeForce(ignition::math::Vector3(-forces.at(3),0.0, 0.0));
      this->model->GetLink("thruster4")->AddRelativeForce(ignition::math::Vector3(-forces.at(4),0.0, 0.0));
      this->model->GetLink("thruster5")->AddRelativeForce(ignition::math::Vector3(-forces.at(5),0.0, 0.0));
      this->model->GetLink("thruster6")->AddRelativeForce(ignition::math::Vector3(-forces.at(6),0.0, 0.0));
      this->model->GetLink("thruster7")->AddRelativeForce(ignition::math::Vector3(-forces.at(7),0.0, 0.0));
    }
    
    
  private:
      /* Pointer to the model */
      physics::ModelPtr model;
      /* Pointer to the update event connection */
      event::ConnectionPtr updateConnection;
      /* Ros node pointer*/
      std::shared_ptr<FtSetterRos2Node> rosNode;
      /* Executer for the ros node */
      std::shared_ptr<rclcpp::executors::SingleThreadedExecutor> executor;
      /* Thread where the executor will sping */
      std::thread thread_executor_spin_;
      /* Bool whether or not to log the forces and torques */
      const bool LOG=true;
      double initial_velocity;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(FtSetter)
}

