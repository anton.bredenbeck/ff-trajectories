/* A node that constantly publishes motor messages with zero rotational velocity */
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/header.hpp"

#include "reacsa_interfaces/msg/motor_state.hpp"

/* A node that regularly publishes the current motor rotational velocity
 */
class DummyMotor : public rclcpp::Node
{
  public:

    DummyMotor():
      rclcpp::Node("rw_motor"),
      theta_dot(0)
    {
      /* Init the publisher */
      motor_pub = this->create_publisher<reacsa_interfaces::msg::MotorState>("/motor_state", 
                                                                              /* QoS */ rclcpp::SensorDataQoS());

      /* Init the timer that regularly calls the publish function */
      pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(10),
            std::bind(&DummyMotor::publish_motor_state, this));
    }

  private:

    void publish_motor_state()
    {
      /* Publish the current value */
      reacsa_interfaces::msg::MotorState motor_msg;

      /* Filling the ros messages with the values. Header time stamp is the current simulation time */
      /* First we find the information for the header */
      motor_msg.header.stamp = this->now();
      motor_msg.header.frame_id = "map";
      /* Store the other values */
      motor_msg.rw_velocity = this->theta_dot;

      /* Actually publish the message*/
      this->motor_pub->publish(motor_msg);
    }

    /*****************************************************************************************
     ***************************** Class Variables *******************************************
     *****************************************************************************************/
    rclcpp::Publisher<reacsa_interfaces::msg::MotorState>::SharedPtr motor_pub;

    rclcpp::TimerBase::SharedPtr pub_timer;

    /* The variables in which the current RW state is stored */
    double theta_dot;

    /******************************************************************************************/
};

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<DummyMotor>());

  return 0;
}
