#include "gazebo/common/common.hh"
#include "gazebo/physics/physics.hh"

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/header.hpp"
#include "std_msgs/msg/float64.hpp"

#include "common.hpp"
#include "reacsa_interfaces/msg/motor_state.hpp"


/* A node that regularly publishes the current motor rotational velocity
 */
class RWmotorRos2Node : public rclcpp::Node
{
  public:

    RWmotorRos2Node(double mean, double stddev):
      rclcpp::Node("rw_motor"),
      theta_dot(0),
      mean(mean), stddev(stddev)
    {
      /* Init the publisher */
      motor_pub = this->create_publisher<reacsa_interfaces::msg::MotorState>("/motor_state",
                                                             /* QoS */ rclcpp::SensorDataQoS());

      /* Init the timer that regularly calls the publish function */
      pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(10),
            std::bind(&RWmotorRos2Node::publish_motor_state, this));


      /* Init the distributions */    
      distribution = std::normal_distribution<double>(mean, stddev);
    }

    void set_theta_dot(double theta_dot)
    {
      this->theta_dot = theta_dot;
    }

  private:

    /* Helper function that subjects the provided double to gausion noise
     * sample from a distribution defined by mean and stddev in the config 
     * file */
    void subject_to_noise(double & theta_dot)
    {
      theta_dot+= this->distribution(this->generator);
    }

    void publish_motor_state()
    {
      /* Publish the current value */
      reacsa_interfaces::msg::MotorState motor_msg;
      motor_msg.header.stamp = this->now();
      motor_msg.header.frame_id = "vicon";
      motor_msg.rw_velocity = this->theta_dot;

      /* Subject the motor measurement to noise */
      this->subject_to_noise(motor_msg.rw_velocity);

      /* Actually publish the message*/
      this->motor_pub->publish(motor_msg);
    }

    /*****************************************************************************************
     ***************************** Class Variables *******************************************
     *****************************************************************************************/
    rclcpp::Publisher<reacsa_interfaces::msg::MotorState>::SharedPtr motor_pub;

    rclcpp::TimerBase::SharedPtr pub_timer;

    /* The variables in which the current RW state is stored */
    double theta_dot,
           mean, stddev;

    /* Distribution Variable */
    std::default_random_engine generator;
    std::normal_distribution<double> distribution;
    /******************************************************************************************/
};

namespace gazebo
{
  /* A Gazebo model plugin that gets the RW state at the end of every simulation step update*/
  class RWmotor : public ModelPlugin
  {
  
  public:
  
    /* Entry point for gazebo. Called when instantiating the model initially */
    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) override
    {
      /* Store the pointer to the model */
      this->model = _parent;

      /* Listen to the update event. This event is broadcast at every
       * end of a simulation iteration. */
      this->updateConnection = event::Events::ConnectWorldUpdateEnd(
          std::bind(&RWmotor::OnUpdate, this));

      /* Init the random distributions */
      if(_sdf->HasElement("mean"))
      {
        this->mean = _sdf->Get<double>("mean");
      }
      if(_sdf->HasElement("stddev"))
      {
        
        this->stddev = _sdf->Get<double>("stddev");
      }

      /* Initialize ros, if it has not already been initialized. */
      if (!rclcpp::ok())
      {
        int argc = 0;
        char **argv = NULL;
        rclcpp::init(argc, argv);
      }

      /* Instantiate the ros node, and create the publishers. */
      this->ros2node = std::make_shared<RWmotorRos2Node>(this->mean, this->stddev);

      /* Init the last time stamp */
      this->last_time_stamp = this->ros2node->now();

      /* Instantiate the executor */
      this->executor = std::make_shared<rclcpp::executors::SingleThreadedExecutor>();
      /* Add the node to the executor ... */
      this->executor->add_node(this->ros2node);

      /* ... executing this task/loop (aka while ros is running spin and check each time 
       *  if ros is actually still running)  .....*/
      auto spin = [this]()
      {
        while(rclcpp::ok())
        {
          this->executor->spin_once();
        }
      };

      /* ... in this thread. */
      this->thread_executor_spin_ = std::thread(spin);
    }
    
    /* This function is called at the end of every simulation iteration step, as we bound it to said event
     * We update the current publish the resulting pose to the respective ros topics */
    void OnUpdate()
    {
      /* Get the pose from the gazebo model */
      physics::JointPtr rw_joint = this->model->GetJoint("reaction_wheel_hinge");
      double theta_dot = rw_joint->GetVelocity(2);
      this->ros2node->set_theta_dot(theta_dot);

      if(this->LOG)
      {
        std::ofstream f;
        std::string path = "src/reacsa_gazebo/log/rw.log";
        f.open(path , std::ios_base::app); // append instead of overwrite
        if(!f)
        {
          throw std::runtime_error("Couldn't open log file " + path);
        }

        long int t = this->ros2node->now().nanoseconds();

        f << t << " " << theta_dot << std::endl;

        f.close();
      }
    }

    
  private:
      /* Pointer to the model */
      physics::ModelPtr model;
      /* Pointer to the update event connection */
      event::ConnectionPtr updateConnection;
      /* ros2 node pointer*/
      std::shared_ptr<RWmotorRos2Node> ros2node;
      /* Executor for the ros2 node */
      std::shared_ptr<rclcpp::executors::SingleThreadedExecutor> executor;
      /* Thread where the executor will spin */
      std::thread thread_executor_spin_;
      /* The last time stamp in order to compute time difference */
      rclcpp::Time last_time_stamp;
      /* Bool whether the motor state is logged to a file */
      const bool LOG = true;

      /* Noise parameters */
      double mean, stddev;
  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(RWmotor)
}