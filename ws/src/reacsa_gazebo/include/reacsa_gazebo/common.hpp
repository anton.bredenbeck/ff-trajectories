#ifndef GAZEBO_COMMON_H
#define GAZEBO_COMMON_H 

#include <Eigen/Dense>
#include <Eigen/Geometry>

const unsigned int S2NS = 1000000000;

Eigen::Quaterniond to_quaternion(double x_rot, double y_rot, double z_rot ) 
{
    // Abbreviations for the various angular functions
    double cy = cos(z_rot * 0.5);
    double sy = sin(z_rot * 0.5);
    double cp = cos(y_rot * 0.5);
    double sp = sin(y_rot * 0.5);
    double cr = cos(x_rot * 0.5);
    double sr = sin(x_rot * 0.5);

    Eigen::Quaternion q(cr * cp * cy + sr * sp * sy,  // w
                        sr * cp * cy - cr * sp * sy,  // x
                        cr * sp * cy + sr * cp * sy,  // y
                        cr * cp * sy - sr * sp * cy); // z

    return q;
}

Eigen::Vector3d sample_mv_normal(const Eigen::Vector3d & mu,
                                 const Eigen::Matrix3d & s,
                                 unsigned int nr_iterations = 10)
{

  Eigen::Matrix3d sigma;
  sigma << s(0,0), s(0,1), s(0,2),
           s(1,0), s(1,1), s(1,2),
           s(2,0), s(2,1), s(2,2);
  // Generate x from the N(0, I) distribution
  Eigen::VectorXd x(3);
  Eigen::VectorXd sum(3);
  sum.setZero();
  for (unsigned int i = 0; i < nr_iterations; i++)
  {
    x.setRandom();
    x = 0.5 * (x + Eigen::VectorXd::Ones(3));
    sum = sum + x;
  }
  sum = sum - (static_cast<double>(nr_iterations) / 2) * Eigen::VectorXd::Ones(3);
  x = sum / (std::sqrt(static_cast<double>(nr_iterations) / 12));

  // Find the eigen vectors of the covariance matrix
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigen_solver(sigma);
  Eigen::MatrixXd eigenvectors = eigen_solver.eigenvectors().real();

  // Find the eigenvalues of the covariance matrix
  Eigen::MatrixXd eigenvalues = eigen_solver.eigenvalues().real().asDiagonal();
  
  // Find the transformation matrix
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> es(eigenvalues);
  Eigen::MatrixXd sqrt_eigenvalues = es.operatorSqrt();
  Eigen::MatrixXd Q = eigenvectors * sqrt_eigenvalues;

  Eigen::Matrix3d q_ign;
  q_ign << Q(0,0), Q(0,1),Q(0,2),
           Q(1,0), Q(1,1),Q(1,2),
           Q(2,0), Q(2,1),Q(2,2);
  Eigen::Vector3d x_ign(x(0), x(1), x(2));

  return q_ign * x_ign + mu;
}

double to_seconds(unsigned int sec, unsigned int nanosec)
{
    return double(sec) + double(nanosec)/double(S2NS);
}


#endif //GAZEBO_COMMON_H