#include <ignition/math.hh>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"

#include "reacsa_interfaces/msg/state.hpp"
#include "reacsa_interfaces/msg/float_stamped.hpp"


class ObservedStateRepeater: public rclcpp::Node 
{
public:
	ObservedStateRepeater():rclcpp::Node("observed_state_repeater")
	{
   	obsv_sub = this->create_subscription<reacsa_interfaces::msg::State>(
       	"/observed_state", /* QoS */ rclcpp::SensorDataQoS(), std::bind(&ObservedStateRepeater::obsv_callback, this,
                                std::placeholders::_1));
   	obsv_pub = this->create_publisher<geometry_msgs::msg::PoseWithCovarianceStamped>("/observed_pose", 
                                                                                     /* QoS */ rclcpp::SensorDataQoS());
    theta_pub = this->create_publisher<reacsa_interfaces::msg::FloatStamped>("/theta", 
                                                                            /* QoS */ rclcpp::SensorDataQoS());

	}
private: 

    double normalize_angle(double angle)
    {
      ignition::math::Angle ang(angle);
      ang.Normalize();
      return ang.Radian();
    }

    void obsv_callback(const reacsa_interfaces::msg::State::SharedPtr msg)
    {
     	auto pub_msg = geometry_msgs::msg::PoseWithCovarianceStamped();
     	pub_msg.header = msg->header;
     	pub_msg.pose = msg->pose;
     	this->obsv_pub->publish(pub_msg);

      auto theta_msg = reacsa_interfaces::msg::FloatStamped();
      theta_msg.header = msg->header;
      theta_msg.data = normalize_angle(2 * std::acos(msg->pose.pose.orientation.w)) * 180.0 * M_1_PI;
      this->theta_pub->publish(theta_msg);
    }

    /***************************************************************************************
     *************** Class Variables *******************************************************
     ***************************************************************************************/
		rclcpp::Subscription<reacsa_interfaces::msg::State>::SharedPtr obsv_sub;
		rclcpp::Publisher<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr obsv_pub;
    rclcpp::Publisher<reacsa_interfaces::msg::FloatStamped>::SharedPtr theta_pub;
};

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<ObservedStateRepeater>());

  return 0;
}