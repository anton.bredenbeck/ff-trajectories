#include <fstream>
#include <iomanip>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"

#include "reacsa_interfaces/msg/force.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/msg/motor_state.hpp"
#include "reacsa_interfaces/msg/state.hpp"


class Bag2Txt: public rclcpp::Node 
{
public:
	Bag2Txt():rclcpp::Node("bag2txt")
	{
	 	torque_sub = this->create_subscription<reacsa_interfaces::msg::Torque>(
        	"/set_torque", /* QoS */ rclcpp::SensorDataQoS(),
	 		     std::bind(&Bag2Txt::torque_callback, this,
                                std::placeholders::_1));
  	force_sub = this->create_subscription<reacsa_interfaces::msg::Force>(
		    	"/set_force", /* QoS */ rclcpp::SensorDataQoS(),
				     std::bind(&Bag2Txt::force_callback, this,
                            std::placeholders::_1));
  	motor_sub = this->create_subscription<reacsa_interfaces::msg::MotorState>(
		    	"/motor_state", /* QoS */ rclcpp::SensorDataQoS(), std::bind(&Bag2Txt::motor_state_callback, this,
		                            std::placeholders::_1));
  	pose_sub = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>(
		    	"/vicon/reacsa/reacsa", /* QoS */ rclcpp::SensorDataQoS(), std::bind(&Bag2Txt::pose_callback, this,
		                            std::placeholders::_1));
  	obsv_sub = this->create_subscription<reacsa_interfaces::msg::State>(
		    	"/observed_state", /* QoS */ rclcpp::SensorDataQoS(), std::bind(&Bag2Txt::obsv_callback, this,
		                            std::placeholders::_1));
	}
	
private: 

    void torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr msg)
    {
	    std::ofstream f;
	    f.open("torque.txt", std::ios_base::app);
	    rclcpp::Time t = msg->header.stamp;
	    f << t.nanoseconds() << " " << msg->torque << std::endl;
	    f.close();
    }

    void force_callback(const reacsa_interfaces::msg::Force::SharedPtr msg)
    {
	    std::ofstream f;
	    f.open("force.txt", std::ios_base::app);
	    rclcpp::Time t = msg->header.stamp;
	    f << t.nanoseconds() << " " << msg->force[0] << " "
			                            << msg->force[1] << " "
				                          << msg->force[2] << " "
				                          << msg->force[3] << " "
				                          << msg->force[4] << " "
				                          << msg->force[5] << " "
				                          << msg->force[6] << " "
				                          << msg->force[7] 
				                          << std::endl;
	    f.close();
    }

    void motor_state_callback(const reacsa_interfaces::msg::MotorState::SharedPtr msg)
    {
	    std::ofstream f;
	    f.open("rw.txt", std::ios_base::app);
	    rclcpp::Time t = msg->header.stamp;
	    f <<  t.nanoseconds()  << " " << msg->rw_velocity << std::endl;
	    f.close(); 
    }

    void pose_callback(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg)
    {
      std::ofstream f;
	    f.open("pose.txt", std::ios_base::app);
	    rclcpp::Time t = msg->header.stamp;
	    double theta = 2 * acos(msg->pose.pose.orientation.w);
	    f  << t.nanoseconds() << " " << msg->pose.pose.position.x << " "
	                                 << msg->pose.pose.position.y << " "
	                                 << theta << std::endl;
	    f.close();  
    }

    void obsv_callback(const reacsa_interfaces::msg::State::SharedPtr msg)
    {
      std::ofstream f;
	    f.open("obsv_state.txt", std::ios_base::app);
	    rclcpp::Time t = msg->header.stamp;
	    f << t.nanoseconds() << " " 
	      << msg->pose.pose.position.x << " "
	      << msg->pose.pose.position.y << " "
        << 2 * acos(msg->pose.pose.orientation.w) << " "
        << msg->twist.twist.linear.x << " "
        << msg->twist.twist.linear.y << " "
        << msg->twist.twist.angular.z << " "
        << msg->rw_vel << std::endl;
	    f.close();  
    }

    /***************************************************************************************
     *************** Class Variables *******************************************************
     ***************************************************************************************/
		rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr pose_sub;
		rclcpp::Subscription<reacsa_interfaces::msg::State>::SharedPtr obsv_sub;
    rclcpp::Subscription<reacsa_interfaces::msg::MotorState>::SharedPtr motor_sub;
		rclcpp::Subscription<reacsa_interfaces::msg::Force>::SharedPtr force_sub;
		rclcpp::Subscription<reacsa_interfaces::msg::Torque>::SharedPtr torque_sub; 
};

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<Bag2Txt>());

  return 0;
}