import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()
    config = os.path.join(
        get_package_share_directory('reacsa_control'),
        'config',
        'params.yaml'
    )

    node_observer = Node(
        package='reacsa_control',
        name='ekf',
        executable='ekf',
        parameters=[config]
    )
    ld.add_action(node_observer)

    return ld
