import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    ld = LaunchDescription()
    config = os.path.join(
        get_package_share_directory('reacsa_control'),
        'config',
        'params.yaml'
    )

    node_joy = Node(
        package='reacsa_control',
        name='joystick_control',
        executable='joystick_control',
        parameters=[config]
    )
    ld.add_action(node_joy)

    node_follow = Node(
        package='reacsa_control',
        name='trajectory_follower',
        executable='trajectory_follower',
        parameters=[config]
    )
    ld.add_action(node_follow)

    node_cmpt = Node(
        package='reacsa_control',
        name='trajectory_planner',
        executable='trajectory_planner.py',
        parameters=[config]
    )
    ld.add_action(node_cmpt)

    node_joy = Node(
        package='joy_linux',
        name='joy_linux_node',
        executable='joy_linux_node',
        parameters=[]
    )
    ld.add_action(node_joy)
    return ld
