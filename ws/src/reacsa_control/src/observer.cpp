#include "observer.hpp"

double Observer::get_dt(){return this->dt;}

void Observer::pause_timer(){this->pub_timer->reset();}

void Observer::restart_timer()
{
    this->pub_timer = this->create_wall_timer(
        std::chrono::milliseconds(int(this->dt * 1e3)),
        std::bind(&Observer::publish_observed_state, this));
}

void Observer::pose_callback(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg)
{
  this->curr_meas_state.header = msg->header;
  this->curr_meas_state.pose = msg->pose;
}

void Observer::motor_callback(const reacsa_interfaces::msg::MotorState::SharedPtr msg)
{
  this->curr_meas_state.header = msg->header;
  this->curr_meas_state.rw_vel = msg->rw_velocity;
}

void Observer::force_callback(const reacsa_interfaces::msg::Force::SharedPtr msg)
{
	this->curr_force_cmd = *msg;
}

void Observer::torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr msg)
{
  this->curr_torque_cmd.header = msg->header;
  /* If we reached the saturation limits and commanded torque in the same direction
   * is meaningless for observation. Hence the torque is assumed to be zero */
  if((this->curr_meas_state.rw_vel <= this->rw_limits.at(0) + this->RPM_BUFFER &&
     msg->torque < 0.0) ||
     (this->curr_meas_state.rw_vel >= this->rw_limits.at(1) - this->RPM_BUFFER &&
      msg->torque > 0.0))
  {
    RCLCPP_INFO(this->get_logger(), "Reached RW saturation. Observer not considering current torque");
    this->curr_torque_cmd.torque = 0.0;
  }
  else
  {
    this->curr_torque_cmd.torque = msg->torque;
  }
}


