#include "trajectory_follower.hpp"

/* Service related binding function */
void TrajectoryFollower::prepare_trajectory_callback(
      const std::shared_ptr<reacsa_interfaces::srv::PrepareTrajectory::Request> request,
            std::shared_ptr<reacsa_interfaces::srv::PrepareTrajectory::Response> response)
{
  // Load the trajectory from the files
  this->load_trajectory(request->state_filename, request->ctrl_filename, request->time_filename);

  // Pre-Compute Optimal State Feedback gains within the framework of a Time Varying Finite Horizon LQR
  this->precompute_ks();

  // We prepared the trajectory so it can be set to true
  this->trajectory_prepared = response->success = true;
}

void TrajectoryFollower::precompute_ks()
{
  RCLCPP_INFO(this->get_logger(), "Pre-Compute feedback Gain Matrices... ");
  /* Build the system.*/ 
  auto sys = drake::systems::TrajectoryAffineSystem<double>(this->A, this->B, this->f0, this->C, this->D, this->y0);
  auto context = sys.CreateDefaultContext();
  sys.get_input_port().FixValue(context.get(), Eigen::Matrix<double, CONTROL_DIM, 1>::Zero());
  drake::systems::controllers::FiniteHorizonLinearQuadraticRegulatorOptions options;

  auto N = Eigen::Matrix<double, STATE_DIM, CONTROL_DIM>::Zero();
  double t0 = this->state_trajectory.start_time();
  double tf = this->state_trajectory.end_time();

  auto lqr_result = drake::systems::controllers::LinearQuadraticRegulator(
          this->A.value(tf), this->B.value(tf), this->Q_final, this->R_final, N);

  options.Qf = lqr_result.S;
  
  /* Compute the Feedback Matrices */
  drake::systems::controllers::FiniteHorizonLinearQuadraticRegulatorResult result =
      FiniteHorizonLinearQuadraticRegulator(sys, *context, t0, tf, this->Q, this->R,
                                            options);

  this->K = result.K;
  this->k0 = result.k0;

  RCLCPP_INFO(this->get_logger(), "... done!");
}

void TrajectoryFollower::compute_control()
{
  /* Depending on which control state we're in we do different actions*/
  switch(this->get_state())
  {
  	/* There is no task for us we're just idling, i.e. publish zero actuation*/
  	case(IDLE):
    {
      /* Set thrust to zero*/
      auto f_msg = reacsa_interfaces::msg::Force();
      f_msg.header.stamp = this->now();
      f_msg.header.frame_id = "vicon";
      f_msg.force = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
      this->set_current_force_command(f_msg);

      /* Set the torque to zero */
      auto t_msg = reacsa_interfaces::msg::Torque();
      t_msg.header.stamp = this->now();
      t_msg.header.frame_id = "vicon";
      t_msg.torque = 0;
      this->set_current_torque_command(t_msg);
    }
  	break;

  	/* In case we only want to control heading. Purposefully falling
     * through to the next case since we want to do the same thing 
     * in both cases. */
  	case(HEADING_CTRL):
    /* We're in pose control mode, i.e. we're  moving the robot
     * towards the next pose in the trajectory */
  	case(POSE_CTRL):
    {
      /* Compute the current state */
      RobotState curr_state = this->get_most_recent_robot_state();
      Eigen::Matrix<double, STATE_DIM,1> goal = this->state_trajectory.value(this->state_trajectory.end_time());

      /* If the distance to the desired position is smaller than some defined 
       * EPSILON we've reached the goal and can go back to IDLE. Also checking
       * that we reached the final index to avoid finishing circular trajectories
       * at the initial(==final) pose */
      if(eucl_dist(curr_state.x, curr_state.y, goal(0), goal(1)) < this->epsilon_position &&
         eucl_dist(curr_state.x_dot, curr_state.y_dot, goal(3), goal(4)) < this->dot_epsilon_position &&
         angle_diff(curr_state.theta, goal(2)) < this->epsilon_orientation  &&
         std::fabs(curr_state.theta_dot - goal(5)) < this->dot_epsilon_orientation &&
         to_seconds(curr_state.sec, curr_state.nanosec) >= this->state_trajectory.end_time())
      {
        this->set_state(IDLE);

        this->turn_off_actuators();

        RCLCPP_INFO(this->get_logger(), "Trajectory goal achieved, going back to IDLE");
      }
      else
      {
        /* Find the currently desired state
         * Find the state along the desired trajectory that we are currently heading for. First we find the time difference
         * of the current state to the start of the trajectory. This delta t is what we are looking for in the trajectory 
         * vector and hence we find the index of the closest state. We store the index in order to know where we can start 
         * searching on the next iteration. */
        double dt = to_seconds(curr_state.sec, curr_state.nanosec) 
                  - to_seconds(this->traj_start_sec, this->traj_start_nanosec);
        Eigen::Matrix<double, STATE_DIM, 1> x_des = this->state_trajectory.value(dt);

        RCLCPP_INFO(this->get_logger(), "dt = %f", dt);

        /* Find the current control error */
        Eigen::Matrix<double, STATE_DIM, 1> error;
        error << curr_state.x - x_des(0),
                 curr_state.y - x_des(1),
                 angle_diff(curr_state.theta, x_des(2)),
                 curr_state.x_dot - x_des(3),
                 curr_state.y_dot - x_des(4),
                 curr_state.theta_dot - x_des(5),
                 curr_state.rw_velocity - x_des(6);

        /* Given the error, find the feedback control */ 
        auto u0 = dt <= this->ctrl_trajectory.end_time() ? this->ctrl_trajectory.value(dt) :
                                                           Eigen::Matrix<double, CONTROL_DIM,1>::Zero();
        Eigen::Matrix<double, CONTROL_DIM, 1> control = u0 +  
                                                      - this->K.get()->value(dt) * error
                                                      - this->k0.get()->value(dt);

        /*************************** LOGGING ***********************************************/
        RCLCPP_DEBUG(this->get_logger(), "State [%f,%f,%f,%f,%f,%f,%f]", curr_state.x, curr_state.y, curr_state.theta,
                                                                        curr_state.x_dot, curr_state.y_dot, curr_state.theta_dot,
                                                                        curr_state.rw_velocity);
        RCLCPP_DEBUG(this->get_logger(), "Des [%f,%f,%f,%f,%f,%f,%f]", x_des(0), x_des(1), x_des(2), x_des(3),
                                                                         x_des(4), x_des(5), x_des(6));
        RCLCPP_DEBUG(this->get_logger(), "Error [%f,%f,%f,%f,%f,%f,%f]", error(0), error(1), error(2), error(3),
                                                                         error(4), error(5), error(6));
        RCLCPP_DEBUG(this->get_logger(), "Control [%f,%f,%f,%f,%f,%f,%f,%f,%f]", control(0),control(1),control(2),
                                                                                 control(3),control(4),control(5),
                                                                                 control(6),control(7),control(8));
        /***********************************************************************************/

        /* Pack the control values in ROS2 messages. 
         * First the torque ... */ 
        auto t_msg = reacsa_interfaces::msg::Torque();
        t_msg.header.stamp =  this->now();
        t_msg.header.frame_id = "vicon";
        t_msg.torque = std::fabs(control(0)) < this->tau ?  control(0) : 
                   ((control(0) > 0) - (control(0) < 0)) * this->tau;

        /* Then the force. We only accumulate the sigma delta modulator here. The output reading happens
         * when we actually publish it (at some smaller frequency). 
         * We also divide by the nominal force to get to the [0,1] space on which the modulator operates 
         */
        for(unsigned int i = 0; i < CONTROL_DIM - 1; i++) 
          /* The modulator operates on [0,1] hence we have to first divide then multiply by the nominal force */
          this->modulators.at(i).give_input((control(i+1) > 0) * control(i + 1) 
                              / this->f , to_seconds(curr_state.sec, curr_state.nanosec));        
        /* Execute control */
        this->set_current_torque_command(t_msg);     
        
      }
      
  	}
    break;

  	/* Fallback case, should not happen */
  	default:
    {
  		assert(false);
    }
  	break;
  }
}

void TrajectoryFollower::turn_off_actuators()
{
   /* Set thrust to zero*/
    auto f_msg = reacsa_interfaces::msg::Force();
    f_msg.header.stamp = this->now();
    f_msg.header.frame_id = "vicon";
    f_msg.force = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    this->set_current_force_command(f_msg);

    /* Set the torque to zero */
    auto t_msg = reacsa_interfaces::msg::Torque();
    t_msg.header.stamp = this->now();
    t_msg.header.frame_id = "vicon";
    t_msg.torque = 0;
    this->set_current_torque_command(t_msg);
}

/* This overrides the super function */
void TrajectoryFollower::publish_force()
{
  /* Setup the force message */
  auto f_msg = reacsa_interfaces::msg::Force();
  f_msg.header.stamp = this->now();
  f_msg.header.frame_id = "vicon";

  /* Get all the output of the previously accumulated modulators
   * and assign it to the thruster indeces */
  for(unsigned int i = 0; i < CONTROL_DIM - 1; i++) 
  {
    /* Also we must multiply by the nominal force to optain the actual 
     * force since the modulator operates on the [0, 1] space. */
    f_msg.force[i] = this->f * this->modulators.at(i).read_output();
  }
  /* Set the current force command */
  this->set_current_force_command(f_msg);

  /* Call the super function to publish it as it was intended */
  this->Controller::publish_force();
}

void TrajectoryFollower::trigger_state()
{
	this->set_state(POSE_CTRL);
  RobotState curr_state = this->get_most_recent_robot_state();
  this->curr_idx = 0;
  this->traj_start_sec = curr_state.sec; 
  this->traj_start_nanosec = curr_state.nanosec;
}

Eigen::Matrix<double, STATE_DIM, 1> TrajectoryFollower::closest_pose(unsigned int sec, unsigned int nanosec)
{
  assert(!this->state_trajectory.empty());
  return this->state_trajectory.value(to_seconds(sec, nanosec));
}


void TrajectoryFollower::read_matrix(std::string file, Eigen::Ref<Eigen::MatrixXd> mat)
{
  /* Init helper variables */
  std::ifstream f(file);
  std::vector<double> vec;
  double value = 0;

  /* Read all file entries to data */
  while(f >> value)
  {
    vec.push_back(value);
  }

  /* Close the file */
  f.close();

  /* Init the eigen matrix with it */
  for(int i = 0; i < mat.rows(); i++){
    for(int j = 0; j < mat.cols(); j++)
    {
      mat(i, j) = vec.at(i * mat.rows() + j);
    }
  }
}

void TrajectoryFollower::read_weights(std::string path)
{
  this->read_matrix(path + "q.lqr", this->Q);
  this->read_matrix(path + "q_final.lqr", this->Q_final);
  this->read_matrix(path + "r.lqr", this->R);
  this->read_matrix(path + "r_final.lqr", this->R_final);
}

void TrajectoryFollower::load_trajectory(std::string state_filename,
                                         std::string ctrl_filename,
                                         std::string time_filename)
{
	RCLCPP_INFO(this->get_logger(), "Reading trajectory ...");
	/* Init file streams */
	std::ifstream state_f(state_filename);
  std::ifstream ctrl_f(ctrl_filename);
	std::ifstream time_f(time_filename);

	/* Make sure the files could be opened */
	assert(state_f.is_open());
  assert(ctrl_f.is_open());
	assert(time_f.is_open());

  std::vector<double> times;
  std::vector<Eigen::MatrixXd> states;
  std::vector<Eigen::MatrixXd> ctrls;
  std::vector<Eigen::MatrixXd> B_matrices;
	/* Temporary variables to read the eight different 
	 * robot states into */
	double x1,x2,x3,x4,x5,x6,x7;
  double u1,u2,u3,u4,u5,u6,u7,u8,u9;
	double t;

	while(state_f >> x1 >> x2 >> x3 >> x4 >> x5 >> x6 >> x7 && 
        ctrl_f >> u1 >> u2 >> u3 >> u4 >> u5 >> u6 >> u7 >> u8 >> u9 &&
		    time_f >> t)
	{
    Eigen::Matrix<double, STATE_DIM, 1> state;
    Eigen::Matrix<double, CONTROL_DIM, 1> ctrl;

    state << x1, x2, normalize_angle(x3), x4, x5, x6, x7;
    ctrl << u1, this->f * u2, this->f * u3, this->f * u4, this->f * u5,
                this->f * u6, this->f * u7, this->f * u8, this->f * u9;

    times.push_back(t);
    states.push_back(state);
    ctrls.push_back(ctrl);   

    /* Store trig functions in order not to evaluate them every time */
    double s_theta = sin(x3);
    double c_theta = cos(x3);

    /* This matrix is time dependent as the effect of the thruster 
     * on the position variables depends on the current state, in 
     * particular the current orientation. All other matrices are constant,
     * therefore they're defined in the constructor. */
    Eigen::Matrix<double, STATE_DIM, CONTROL_DIM> B_i;
    B_i << 0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0, 0, 0,
           0, - s_theta/m, s_theta/m, - c_theta/m, c_theta/m, s_theta/m, -s_theta/m, c_theta/m, -c_theta/m,
           0,  c_theta/m, - c_theta/m, - s_theta/m,  s_theta/m, - c_theta/m,  c_theta/m,  s_theta/m, - s_theta/m,
           -1.0/Ib,  r/Ib, - r/Ib,  r/Ib, - r/Ib,  r/Ib, - r/Ib,  r/Ib, - r/Ib,
           1.0/Iw, 0, 0, 0, 0, 0, 0, 0, 0;
    B_matrices.push_back(B_i);
  }

  /* Write the matrices to a drake trajectory */
  this->B = drake::trajectories::PiecewisePolynomial<double>::ZeroOrderHold(times, B_matrices);
  this->state_trajectory = drake::trajectories::PiecewisePolynomial<double>::CubicShapePreserving(times, states, false);
  this->ctrl_trajectory = drake::trajectories::PiecewisePolynomial<double>::ZeroOrderHold(times, ctrls);

	RCLCPP_INFO(this->get_logger(), "... done! Read %d poses + control.", times.size());
}


rclcpp_action::GoalResponse TrajectoryFollower::handle_goal(const rclcpp_action::GoalUUID & uuid,
                                        std::shared_ptr<const ControlAction::Goal> goal)
{
  RCLCPP_INFO(this->get_logger(), "Received goal request to follow trajectory...");
  (void)uuid;
  (void)goal;
  rclcpp_action::GoalResponse gr;
  /* Only accept goal if the trajectory was previously prepared using the /prepare_trajectory service */
  if(this->trajectory_prepared)
  { 
    RCLCPP_INFO(this->get_logger(), "... accepted! Following Trajectory.");
    gr = rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
  }
  else
  {
    RCLCPP_INFO(this->get_logger(),
      "... rejected! Please prepare the trajectory first by calling the service /prepare_trajectory giving the trajectory text files.");
    gr = rclcpp_action::GoalResponse::REJECT;
  }
  return gr;
}

rclcpp_action::CancelResponse TrajectoryFollower::handle_cancel(const std::shared_ptr<GoalHandle> goal_handle)
{
  (void)goal_handle;
  /* Accept all cancellations. Publish zero on all command topics */
  RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
  this->turn_off_actuators();
  return rclcpp_action::CancelResponse::ACCEPT;
}

void TrajectoryFollower::handle_accepted(const std::shared_ptr<GoalHandle> goal_handle)
{
  // Spin up the thread than publishes feedback and checks for cancellation and finalization
  std::thread{std::bind(&TrajectoryFollower::execute, this, std::placeholders::_1), goal_handle}.detach();
}

void TrajectoryFollower::execute(const std::shared_ptr<GoalHandle> goal_handle)
{
  /* Reset current commands */
  this->turn_off_actuators();
  /* Reset all integrators to zero */
  for(int i = 0; i < 8; i++)
  {
    this->modulators.at(i).reset();
  }
  // Get the initial time stamp
  rclcpp::Time start = this->now();
  (void)goal_handle;
 
  // Publish rate for feedback
  rclcpp::Rate loop_rate(1);
  auto result = std::make_shared<ControlAction::Result>();

  // switch state
  this->trigger_state();

  // Loop while ros2 runs
  while(rclcpp::ok())
  {
    // Check if there is a cancel request
    if(goal_handle->is_canceling())
    {
      result->duration = (this->now() - start).seconds();
      goal_handle->canceled(result);
      this->set_state(IDLE);
      this->trajectory_prepared = false;
      RCLCPP_INFO(this->get_logger(), "Goal canceled, state reset to IDLE");
      return;
    }

    // Find the current state and publish it as feedback
    RobotState curr_state = this->get_most_recent_robot_state();
    auto feedback = std::make_shared<ControlAction::Feedback>();
    feedback->curr_x = curr_state.x;
    feedback->curr_y = curr_state.y;
    goal_handle->publish_feedback(feedback);

    // Check if the action is completed, i.e. we're back to idle mode
    if(this->get_state() == IDLE)
    {
      // The action succeeded.
      result->duration = (this->now() - start).seconds();
      goal_handle->succeed(result);
      this->trajectory_prepared = false;
      RCLCPP_INFO(this->get_logger(), "Goal succeeded");
      return;
    }
    loop_rate.sleep();
  }
}

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<TrajectoryFollower>());

  return 0;
}
