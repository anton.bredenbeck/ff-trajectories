#include "joystick_control.hpp"

void JoystickControl::ct_goal_response_callback(std::shared_future<GoalHandleCT::SharedPtr> future)
{
	auto goal_handle = future.get();
	if (!goal_handle) {
	  RCLCPP_ERROR(this->get_logger(), "Goal was rejected by server");
	} else {
	  RCLCPP_INFO(this->get_logger(), "Goal accepted by server, waiting for result");
	}
}

void JoystickControl::ct_result_callback(const GoalHandleCT::WrappedResult & result)
{
	using namespace std::chrono_literals;
	using namespace std::placeholders;
	switch (result.code) {
	  case rclcpp_action::ResultCode::SUCCEEDED:
	  {
	  	RCLCPP_INFO(this->get_logger(), "Duration %f s", result.result->duration);
	    
	    /* If we succeeded at finding a trajectory we start following it */
	    /* Wait for the action server to become available */
		if (!this->follow_traj->wait_for_action_server(100ms)) {
	      RCLCPP_ERROR(this->get_logger(), "/trajectory_follower Action server not available after waiting");
	      return;
	    }

	    auto request = std::make_shared<reacsa_interfaces::srv::PrepareTrajectory::Request>();
	    request->state_filename = "src/reacsa_control/trajectories/x_star.txt";
		request->ctrl_filename = "src/reacsa_control/trajectories/u_star.txt";
		request->time_filename = "src/reacsa_control/trajectories/t_star.txt";
		auto result = this->prep_traj->async_send_request(request);

		/* TODO: Maybe we have to wait for the service respond here */

		auto send_goal = FollowTrajectory::Goal();	
		auto send_goal_options = rclcpp_action::Client<FollowTrajectory>::SendGoalOptions();
	    send_goal_options.goal_response_callback =
	      std::bind(&JoystickControl::ft_goal_response_callback, this, _1);
	    send_goal_options.feedback_callback =
	      std::bind(&JoystickControl::ft_feedback_callback, this, _1, _2);
	    send_goal_options.result_callback =
	      std::bind(&JoystickControl::ft_result_callback, this, _1);

		RCLCPP_INFO(this->get_logger(), "Following Trajectory");
		this->follow_traj->async_send_goal(send_goal, send_goal_options);

	    break;
      }
	  case rclcpp_action::ResultCode::ABORTED:
	    RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
	    return;
	  case rclcpp_action::ResultCode::CANCELED:
	    RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
	    return;
	  default:
	    RCLCPP_ERROR(this->get_logger(), "Unknown result code");
	    return;
	}

}


void JoystickControl::ft_goal_response_callback(std::shared_future<GoalHandleFT::SharedPtr> future)
{
	auto goal_handle = future.get();
	if (!goal_handle) {
	  RCLCPP_ERROR(this->get_logger(), "Goal was rejected by server");
	} else {
	  RCLCPP_INFO(this->get_logger(), "Goal accepted by server, waiting for result");
	}
}

void JoystickControl::ft_feedback_callback(
	GoalHandleFT::SharedPtr,
	const std::shared_ptr<const FollowTrajectory::Feedback> feedback)
{
	RCLCPP_INFO(this->get_logger(), "Curr x: %f, Curr y: %f", feedback->curr_x, feedback->curr_y);
}

void JoystickControl::ft_result_callback(const GoalHandleFT::WrappedResult & result)
{
	switch (result.code) {
	  case rclcpp_action::ResultCode::SUCCEEDED:
	    break;
	  case rclcpp_action::ResultCode::ABORTED:
	    RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
	    return;
	  case rclcpp_action::ResultCode::CANCELED:
	    RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
	    return;
	  default:
	    RCLCPP_ERROR(this->get_logger(), "Unknown result code");
	    return;
	}

	RCLCPP_INFO(this->get_logger(), "Duration %f s", result.result->duration);
}

void JoystickControl::pose_callback(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg)
{
	this->curr_x = msg->pose.pose.position.x;
	this->curr_y = msg->pose.pose.position.y;

	ignition::math::Quaternion<double> quat(msg->pose.pose.orientation.w,
                                     msg->pose.pose.orientation.x,
                                     msg->pose.pose.orientation.y,
                                     msg->pose.pose.orientation.z);
    this->curr_theta = (quat.Euler().Z());
}

void JoystickControl::joy_callback(const sensor_msgs::msg::Joy::SharedPtr msg)
{
	/* First check if the start button is pressed. If yes make the system 
	 * navigate to the Origin autonomously using the trajectory following 
	 * service. If it is not pressed, continue with the regular control */
	if(msg->buttons[9] == 1)
	{
		using namespace std::chrono_literals;
		using namespace std::placeholders;

		/* Wait for the action server to become available */
		if (!this->cmp_traj->wait_for_action_server(100ms)) {
	      RCLCPP_ERROR(this->get_logger(), "/compute_trajectory Action server not available after waiting");
	      return;
	    }

		/* Compute the trajectories */
		/* Find current and final state */
		reacsa_interfaces::msg::State init, final;
		init.pose.pose.position.x = this->curr_x;
		init.pose.pose.position.y = this->curr_y;
		init.pose.pose.position.z = 0;
		ignition::math::Quaterniond quat_init(ignition::math::Vector3d(0, 0, 1), this->curr_theta); 
		init.pose.pose.orientation.x = quat_init.X();
		init.pose.pose.orientation.y = quat_init.Y();
		init.pose.pose.orientation.z = quat_init.Z();
		init.pose.pose.orientation.w = quat_init.W();
		init.twist.twist.linear.x = 0;
		init.twist.twist.linear.y = 0; 
		init.twist.twist.angular.z = 0;
		init.rw_vel = 0;

		final.pose.pose.position.x = 0;
		final.pose.pose.position.y = 0;
		final.pose.pose.position.z = 0;
		ignition::math::Quaterniond quat_final(ignition::math::Vector3d(0, 0, 1), 0); 
		final.pose.pose.orientation.x = quat_final.X();
		final.pose.pose.orientation.y = quat_final.Y();
		final.pose.pose.orientation.z = quat_final.Z();
		final.pose.pose.orientation.w = quat_final.W();
		final.twist.twist.linear.x = 0;
		final.twist.twist.linear.y = 0; 
		final.twist.twist.angular.z = 0;
		final.rw_vel = 0;

		/* Compute trajectory to origin */
		auto send_goal = ComputeTrajectory::Goal();
		send_goal.init = init;
		send_goal.final = final;
		send_goal.num_points = 20;

		auto send_goal_options = rclcpp_action::Client<ComputeTrajectory>::SendGoalOptions();
	    send_goal_options.goal_response_callback =
	      std::bind(&JoystickControl::ct_goal_response_callback, this, _1);
	    send_goal_options.result_callback =
	      std::bind(&JoystickControl::ct_result_callback, this, _1);

	    /* Actually send the goal */
	    this->cmp_traj->async_send_goal(send_goal, send_goal_options);
	}
	else
	{
		Direction f_dir, t_dir;

		/* Find the force direction according to button presses */
		if(msg->axes[0] == 1) f_dir = Direction::LEFT;
		else if(msg->axes[0] == -1) f_dir = Direction::RIGHT;
		else if(msg->axes[1] == 1) f_dir = Direction::FRONT;
		else if(msg->axes[1] == -1) f_dir = Direction::BACK;
		else if(msg->buttons[4] == 1) f_dir = Direction::CCW;
		else if(msg->buttons[5] == 1) f_dir = Direction::CW;
		else f_dir = Direction::OFF;

		/* Find the torque direction according to button presses */
		if(msg->axes[5] < 1) t_dir = Direction::CCW;
		else if(msg->axes[2] < 1) t_dir = Direction::CW;
		else t_dir = Direction::OFF;

		/* Init the message to be published */
	    auto f_msg = reacsa_interfaces::msg::Force();
	    auto t_msg = reacsa_interfaces::msg::Torque();
	        
	    /* Fill the force message according to most recent joystick message */
		switch(f_dir)
		{
			case FRONT:
				f_msg.force = {this->f, 0.0, 0.0, 0.0, 0.0, this->f, 0.0, 0.0};
			break;
			case BACK:
				f_msg.force = {0.0, this->f, 0.0, 0.0, this->f, 0.0, 0.0, 0.0};
			break;
			case RIGHT:
				f_msg.force = {0.0, 0.0, 0.0, this->f, 0.0, 0.0, this->f, 0.0};
			break;
			case LEFT:
				f_msg.force = {0.0, 0.0, this->f, 0.0, 0.0, 0.0, 0.0, this->f};
			break;
			case CW:
				f_msg.force = {0.0, this->f, 0.0, 0.0, 0.0, this->f, 0.0, 0.0};
			break;
			case CCW:
				f_msg.force = {this->f, 0.0, 0.0, 0.0, this->f, 0.0, 0.0, 0.0};
			break;
			case OFF: 
				f_msg.force = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			break;
			default:
				f_msg.force = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			break;
		}

	    /* Fill the torque message according to most recent joystick message */
		switch(t_dir)
		{
			case CW: 
				t_msg.torque = - this->tau * 0.5 * (1 - msg->axes[2]);
			break;
			case CCW: 
				t_msg.torque = this->tau * 0.5 * (1 - msg->axes[5]);
			break;
			case OFF:
				t_msg.torque = 0;
			break;
			default: 
				t_msg.torque = 0;
			break;
		}

		RCLCPP_DEBUG(this->get_logger(), "Publish Force: [%f,%f,%f,%f,%f,%f,%f,%f,%f]", f_msg.force[0], f_msg.force[1],
			                                                                           f_msg.force[2], f_msg.force[3],
			                                                                           f_msg.force[4], f_msg.force[5],
			                                                                           f_msg.force[6], f_msg.force[7]);
		RCLCPP_DEBUG(this->get_logger(), "Publish Torque: [%f]", t_msg.torque);

		/* Actually publish the commands */
	    this->set_torque_pub->publish(t_msg);
	    this->set_force_pub->publish(f_msg);
	}
}


int main(int argc, char const *argv[])
{
	if(!rclcpp::ok())
	{
		rclcpp::init(argc, argv);
	}

	rclcpp::spin(std::make_shared<JoystickControl>());
	rclcpp::shutdown();
	return 0;
}