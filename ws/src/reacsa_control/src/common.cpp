#include "common.hpp"

double norm(double x, double y)
{
    return std::sqrt(x * x + y * y);
}

double eucl_dist(double x1, double y1,
                 double x2, double y2)
{
    return norm((x1-x2), (y1-y2));
}

double rad2deg(double rad)
{
  return  rad * 180 * M_1_PI;
}

double round5( double val )
{
    if( val < 0 ) return ceil(val*10000.0 - 0.5)/10000.0;
    return floor(val*10000.0 + 0.5)/10000.0;
}

double angle_diff(double ang1, double ang2)
{
    ignition::math::Angle angle1(ang1);
    angle1.Normalize();
    ignition::math::Angle angle2(ang2);
    angle2.Normalize();
    ignition::math::Angle angle = angle1 - angle2;
    angle.Normalize();
    return angle();
}

double normalize_angle(double angle)
{
  ignition::math::Angle ang(angle);
  ang.Normalize();
  return ang.Radian();
}

double to_seconds(unsigned int sec, unsigned int nanosec)
{
    return double(sec) + double(nanosec)/double(S2NS);
}

void to_sec_nsec(double t, unsigned int & sec, unsigned int & nanosec)
{
    sec = (unsigned int) t;
    nanosec = (unsigned int)((t - sec)*1e9); 
}
