#include "ekf.hpp"

void EKF::reset(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                      std::shared_ptr<std_srvs::srv::Empty::Response> response)
{
  /* Pause Timer to make sure no update is overwriting this reset */
  this->pause_timer();
  /* Function Parameters are unused since they're empty anyways */
  (void)request;
  (void)response;

  RCLCPP_INFO(this->get_logger(), "Resetting Kalman Filter.");
  /* Clear last states */
  this->last_states.vector.clear();
  this->last_thetas.vector.clear();
  
  /* Pack the current measurements to a vector */
  Eigen::Matrix<double, OUTPUT_DIM, 1> curr_meas;
  curr_meas << this->curr_meas_state.pose.pose.position.x, 
               this->curr_meas_state.pose.pose.position.y, 
               this->last_states.vector.size() < 2 ? 0.0 : 
                          (this->last_states.vector.at(1)(0) - this->last_states.vector.at(0)(0))/this->get_dt(),
               this->last_states.vector.size() < 2 ? 0.0 :
                          (this->last_states.vector.at(1)(1) - this->last_states.vector.at(0)(1))/this->get_dt(),
               this->curr_meas_state.rw_vel;
  Eigen::Matrix<double, SO2_OUTPUT, 1> so2_meas;
  so2_meas << normalize_angle(2 * std::acos(this->curr_meas_state.pose.pose.orientation.w)),
              this->last_states.vector.size() < 2 ? 0.0 : 
                          angle_diff(this->last_thetas.vector.at(1), 
                                     this->last_thetas.vector.at(0))/this->get_dt();
  
  /* Set the current estimates to the most recent measurement*/
  this->curr_est = curr_meas;

  /* SO2 extract measurement */
  Eigen::Matrix4d curr_meas_r;
  curr_meas_r << std::cos(so2_meas(0)), -std::sin(so2_meas(0)), 0, 0,
                 std::sin(so2_meas(0)),  std::cos(so2_meas(0)), 0, 0,
                                      0,                       0, 1, so2_meas(1),
                                      0,                       0, 0, 1;
  this->SO2_est = curr_meas_r;  

  /* After the reset is done restart the timer */
  this->restart_timer();
}

void EKF::predict(Eigen::Matrix<double, CONTROL_DIM, 1> curr_ctrl)
{
  /* Compute the current dynamics derivative with respect to the state x */
  double dt = this->get_dt();
  double dt2 = dt * dt;
  double cTm = std::cos(this->get_current_theta()) / m;
  double sTm = std::sin(this->get_current_theta()) / m;

  dFdx << 1, 0, dt, 0, 0,
          0, 1, 0, dt, 0,
          0, 0, 1, 0,  0,
          0, 0, 0, 1,  0,
          0, 0, 0, 0,  1;

  /* Compute the current input matrix*/
  this->B_ekf << 0, - dt2*0.5*sTm, dt2*0.5*sTm, -dt2*0.5*cTm, dt2*0.5*cTm, dt2*0.5*sTm, -dt2*0.5*sTm, dt2*0.5*cTm, -dt2*0.5*cTm,
                 0, dt2*0.5*cTm, - dt2*0.5*cTm, -dt2*0.5*sTm, dt2*0.5*sTm, -dt2*0.5*cTm, dt2*0.5*cTm, dt2*0.5*sTm, -dt2*0.5*sTm,
                 0, -dt*sTm, dt*sTm, -dt*cTm, dt*cTm, dt*sTm, -dt*sTm, dt*cTm, -dt*cTm,
                 0,  dt*cTm, -dt*cTm, -dt*sTm, dt*sTm, -dt*cTm, dt*cTm, dt*sTm, -dt*sTm,
                 dt/Iw, 0, 0, 0, 0, 0, 0, 0, 0;


  /* SO2 Prediction */                 
  // Find the change in angle due to controls and already existing velcity 
  double delta_theta_total = dt * this->get_current_omega() + (this->B_so2.row(0) * curr_ctrl)(0);
  double delta_dtheta = (this->B_so2.row(1) * curr_ctrl)(0);
  // Store in SO2 matrix
  Eigen::Matrix4d m;
  m <<                 0, -delta_theta_total, 0, 0,
       delta_theta_total,                  0, 0, 0,
                       0,                  0, 0, delta_dtheta,
                       0,                  0, 0, 0;
  // Predict Estimate by multiplying exponential
  this->SO2_est *= m.exp();

  // Predict Covariance 
  this->SO2_covariance = this->F_so2 * this->SO2_covariance * this->F_so2.transpose() + Q_so2;

  /* General Kalman Predict covariance matrix */
  this->covariance = ((dFdx * this->covariance * dFdx.transpose()) 
                    + dFdw * (dt * this->Q_ekf) * dFdw.transpose());

  /* Predict state */
  this->curr_est = (this->A_ekf * this->curr_est + this->B_ekf * curr_ctrl);
}

void EKF::update(Eigen::Matrix<double, OUTPUT_DIM, 1> curr_meas, 
                 Eigen::Matrix<double, SO2_OUTPUT, 1> so2_meas)
{
  /* Compute Kalman Gain */
  const Eigen::Matrix<double, STATE_DIM, OUTPUT_DIM> K =
        this->covariance * this->dHdx.transpose() * (this->dHdx * this->covariance * this->dHdx.transpose() 
          + this->dHdw * (this->R_ekf) * this->dHdw.transpose()).inverse();
  
  /* State estimation correction step */
  this->curr_est += (K * (curr_meas - this->dHdx * this->curr_est));

  /* Update covariance matrix */
  this->covariance -= (K * this->dHdx * this->covariance).eval();

  /* SO2 Correction step */
  Eigen::Matrix4d curr_meas_r;
  curr_meas_r << std::cos(so2_meas(0)), -std::sin(so2_meas(0)), 0, 0,
                 std::sin(so2_meas(0)),  std::cos(so2_meas(0)), 0, 0,
                                      0,                       0, 1, so2_meas(1),
                                      0,                       0, 0, 1;
  Eigen::Matrix2d H; 
  H << 1, 0,
       0, 1;
  Eigen::Matrix2d K_so2 = this->SO2_covariance * H.transpose() *
        (H * this->SO2_covariance * H.transpose() + R_so2).inverse(); // This is a double, hence no inverse but division by
  Eigen::Vector2d innovation;
  innovation << (this->SO2_est.inverse() * curr_meas_r).log()(1, 0),
                (this->SO2_est.inverse() * curr_meas_r)(2, 3); 
  Eigen::Vector2d weighted_innovation = K_so2 * (innovation);

  Eigen::Matrix4d m;
  m << 0, -weighted_innovation(0), 0, 0,
       weighted_innovation(0), 0, 0, 0,
       0, 0, 0, weighted_innovation(1),
       0, 0, 0, 0;

  this->SO2_est *= m.exp();
  this->SO2_covariance -= (K_so2 * H * this->SO2_covariance);
}

void EKF::cmpt_observed_state()
{
    /* Pack the current control command to a vector */
    Eigen::Matrix<double, CONTROL_DIM, 1> curr_ctrl;
    curr_ctrl << this->curr_torque_cmd.torque, 
                 this->curr_force_cmd.force[0],
                 this->curr_force_cmd.force[1],
                 this->curr_force_cmd.force[2],
                 this->curr_force_cmd.force[3],
                 this->curr_force_cmd.force[4],
                 this->curr_force_cmd.force[5],
                 this->curr_force_cmd.force[6],
                 this->curr_force_cmd.force[7];

    /* Run the prediction step */
    this->predict(curr_ctrl);

    /* Pack the current measurements to a vector */
    Eigen::Matrix<double, OUTPUT_DIM, 1> curr_meas;
    double dt_inv = 1.0/this->get_dt();
    curr_meas << this->curr_meas_state.pose.pose.position.x, 
                 this->curr_meas_state.pose.pose.position.y, 
                 this->last_states.vector.size() < 2 ? 0.0 : 
                            (this->last_states.vector.at(1)(0) - this->last_states.vector.at(0)(0)) * dt_inv,
                 this->last_states.vector.size() < 2 ? 0.0 :
                            (this->last_states.vector.at(1)(1) - this->last_states.vector.at(0)(1)) * dt_inv,
                 this->curr_meas_state.rw_vel;
    Eigen::Matrix<double, SO2_OUTPUT, 1> so2_meas;
    so2_meas << normalize_angle(2 * std::acos(this->curr_meas_state.pose.pose.orientation.w)),
                this->last_states.vector.size() < 2 ? 0 : 
                            normalize_angle(this->last_thetas.vector.at(1) 
                                          - this->last_thetas.vector.at(0)) * dt_inv;

    /* Run the update Step*/
    this->update(curr_meas, so2_meas);

    /* Remember the estimate */
    this->last_states.push(this->curr_est);
    this->last_thetas.push(this->get_current_theta());
} 

void EKF::publish_observed_state()
{
  /* Pack the current estimate to a message and publish it */
  reacsa_interfaces::msg::State x_est;
  x_est.header = this->curr_meas_state.header;
  x_est.pose.pose.position.x = this->curr_est(0);
  x_est.pose.pose.position.y = this->curr_est(1);
  ignition::math::Quaterniond quat(ignition::math::Vector3d(0.0, 0.0, 1.0),
                     this->get_current_theta());
  x_est.pose.pose.orientation.x = quat.Z() < 0 ? -quat.X() : quat.X();
  x_est.pose.pose.orientation.y = quat.Z() < 0 ? -quat.Y() : quat.Y();
  x_est.pose.pose.orientation.z = quat.Z() < 0 ? -quat.Z() : quat.Z();
  x_est.pose.pose.orientation.w = quat.Z() < 0 ? -quat.W() : quat.W();
  x_est.twist.twist.linear.x = this->curr_est(2);
  x_est.twist.twist.linear.y = this->curr_est(3);
  x_est.twist.twist.angular.z = this->get_current_omega();
  x_est.rw_vel = this->curr_est(4);

  /* Actually publish */
  this->state_pub->publish(x_est);
}


void EKF::read_matrix(std::string file, Eigen::Ref<Eigen::MatrixXd> mat)
{
  /* Init helper variables */
  std::ifstream f(file);
  std::vector<double> vec;
  double value = 0;

  /* Read all file entries to data */
  while(f >> value)
  {
    vec.push_back(value);
  }

  /* Close the file */
  f.close();

  /* Init the eigen matrix with it */
  for(int i = 0; i < mat.rows(); i++){
    for(int j = 0; j < mat.cols(); j++)
    {
      mat(i, j) = vec.at(i * mat.rows() + j);
    }
  }
}

void EKF::read_weights(std::string path)
{
  this->read_matrix(path + "q.ekf", this->Q_ekf);
  this->read_matrix(path + "r.ekf", this->R_ekf);
  this->read_matrix(path + "q.so2", this->Q_so2);
  this->read_matrix(path + "r.so2", this->R_so2);
}

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<EKF>());

  return 0;
}
