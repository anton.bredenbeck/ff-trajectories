#include "sigma_delta_modulator.hpp"

void SigmaDeltaModulator::give_input(double force, double t)
{
	/* Find the time difference used for integrating numerically */
	double dt = t_last == 0 ? 0 : t - t_last;
	/* Remember the time for the next call */
	this->t_last =  t;
	/* Integrate the current error weighted by the system gain k*/
	this->integrator_value += dt * this->K * (force - this->current_output);
}
double SigmaDeltaModulator::read_output()
{
	/* Update the current output */
	this->current_output = this->integrator_value > this->EPS ? 1 : 0;

	/* Return the output value */
	return this->current_output;
}

double SigmaDeltaModulator::modulate_continuous_force(double force, double t)
{
	this->give_input(force, t);
	return this->read_output();
}

void SigmaDeltaModulator::reset()
{
	this->current_output = 0.0;
	this->integrator_value = 0.0;
	this->t_last = 0.0;
}