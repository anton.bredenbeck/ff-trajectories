#include "controller.hpp"


void Controller::publish_force()
{
  this->curr_force_cmd.header.stamp = this->now();
  this->set_force_pub->publish(curr_force_cmd);
}

void Controller::publish_torque()
{ 
  this->curr_torque_cmd.header.stamp = this->now();
  this->set_torque_pub->publish(curr_torque_cmd);
}

double Controller::find_desired_heading(const RobotState curr_state, const RobotState curr_goal, double epsilon) const
{
  /* To avoid numeric problems we only find the heading if we're not to close 
   * to the point. If we are we simply return the current heading */
  if(eucl_dist(curr_state.x, curr_state.y, curr_goal.x, curr_goal.y) > epsilon)
  {
    double heading1 = std::atan((curr_goal.y-curr_state.y)/(curr_goal.x - curr_state.x));
    double heading2 = - std::atan((curr_goal.x-curr_state.x)/(curr_goal.y - curr_state.y));

    return std::abs(curr_state.theta - heading1) 
           <= std::abs(curr_state.theta - heading2) ?
          heading1 : heading2;
  }
  else return curr_state.theta;
}

RobotState Controller::get_most_recent_robot_state()
{
  RobotState state;
  state.x = this->robot_state.pose.pose.position.x;
  state.y = this->robot_state.pose.pose.position.y;
  state.theta = normalize_angle(2 * std::acos(this->robot_state.pose.pose.orientation.w));
  state.x_dot = this->robot_state.twist.twist.linear.x;
  state.y_dot = this->robot_state.twist.twist.linear.y;
  state.theta_dot = this->robot_state.twist.twist.angular.z;
  state.rw_velocity = this->robot_state.rw_vel - this->rw_offset;
  state.sec = this->robot_state.header.stamp.sec;
  state.nanosec = this->robot_state.header.stamp.nanosec;
  return state;
}

/*************** Private functions ********************/

void Controller::state_callback(const reacsa_interfaces::msg::State::SharedPtr state_msg)
{
  this->robot_state = reacsa_interfaces::msg::State(*state_msg);
}


