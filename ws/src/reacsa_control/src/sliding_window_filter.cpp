#include "sliding_window_filter.hpp"

void SlidingWindowFilter::cmpt_observed_state()
{
  /* We first find the current robot state in the 2d struct given the current message */
  this->curr_observed_state = this->compute_vel();
}

void SlidingWindowFilter::reset(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                                std::shared_ptr<std_srvs::srv::Empty::Response> response)
{
  RCLCPP_WARN(this->get_logger(), "SlidingWindowFilter::reset(...) not yet implemented");
  (void)request;
  (void)response;
}

reacsa_interfaces::msg::State SlidingWindowFilter::compute_vel()
{

  reacsa_interfaces::msg::State robot_state;
  /* Fill in the current pose */
  robot_state.header = this->curr_meas_state.header;
  robot_state.header.frame_id = "vicon";
  robot_state.rw_vel = this->curr_meas_state.rw_vel;
  robot_state.pose = this->curr_meas_state.pose;
 
  if(!this->last_states.vector.empty())
  {   

    double dt = static_cast<double>((rclcpp::Time(robot_state.header.stamp) 
                                   - rclcpp::Time(this->last_states.vector.back().header.stamp)).nanoseconds()) * 1e-9;

    /* If delta t is too close to zero we skip this value and only assume constant 
     * velocity from the previous measurement */
    if(dt < 0.00001) 
    {
        RCLCPP_WARN(this->get_logger(), "Received old pose message (%f s). Dropping.", dt);
        return this->last_states.vector.back();
    }

    const int size = static_cast<int>(this->last_states.vector.size());
  
    Eigen::MatrixXd quats =  Eigen::MatrixXd::Zero(4, size);
    quats(0, 0) = robot_state.pose.pose.orientation.x / ((double)size);
    quats(1, 0) = robot_state.pose.pose.orientation.y / ((double)size);
    quats(2, 0) = robot_state.pose.pose.orientation.z / ((double)size);
    quats(3, 0) = robot_state.pose.pose.orientation.w / ((double)size);

    for(int i = 1; i < size; i++)
    {
      auto st = this->last_states.vector.at(i);

      robot_state.pose.pose.position.x += st.pose.pose.position.x;
      robot_state.pose.pose.position.y += st.pose.pose.position.y;
      robot_state.pose.pose.position.z += st.pose.pose.position.z;

      /* Append quaternions to the matrix to form a 4xVectorSize matrix */
      quats(0, i) = this->last_states.vector.at(i).pose.pose.orientation.x / ((double)size);
      quats(1, i) = this->last_states.vector.at(i).pose.pose.orientation.y / ((double)size);
      quats(2, i) = this->last_states.vector.at(i).pose.pose.orientation.z / ((double)size);
      quats(3, i) = this->last_states.vector.at(i).pose.pose.orientation.w / ((double)size);
    }

    robot_state.pose.pose.position.x /= ((double)size);
    robot_state.pose.pose.position.y /= ((double)size);
    robot_state.pose.pose.position.z /= ((double)size);

    /* The average quaternion is the eigenvector of Q*Q^T that corresponds to tha largest 
     * largest Eigenvalue of said Matrix */
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>> s(quats * quats.transpose());
    /* SelfAdjointEigenSolver sorts the eigenvalues/eigenvectors from smallest to largest, so we simply
     * take the last eigenvector corresponding to the largest eigenvalue. Since the matrix is always 4x4 
     * we know the last value is at index 3. We also enforce positivity of w to avoid quaternion ambiguities. */
    robot_state.pose.pose.orientation.x = s.eigenvectors().col(3)(3) < 0 ? 
                  - s.eigenvectors().col(3)(0) : s.eigenvectors().col(3)(0);
    robot_state.pose.pose.orientation.y = s.eigenvectors().col(3)(3) < 0 ? 
                  - s.eigenvectors().col(3)(1) : s.eigenvectors().col(3)(1);
    robot_state.pose.pose.orientation.z = s.eigenvectors().col(3)(3) < 0 ?
                  - s.eigenvectors().col(3)(2) : s.eigenvectors().col(3)(2);
    robot_state.pose.pose.orientation.w = s.eigenvectors().col(3)(3) < 0 ? 
                  - s.eigenvectors().col(3)(3) : s.eigenvectors().col(3)(3);
    

    /* Now we compute the current rates of change by using a ramped 
     * filter of all recent poses. I.e. we compute the weighted average of 
     * the last poses. The weight is the inverse of the number of sampling 
     * periods lie between any given pose and the most recent measurement.
     * Only executed if the vector isn't empty. */
    robot_state.twist.twist.linear.x = (robot_state.pose.pose.position.x 
                                      - this->last_states.vector.back().pose.pose.position.x)
                                       / dt; 
    robot_state.twist.twist.linear.y = (robot_state.pose.pose.position.y
                                      - this->last_states.vector.back().pose.pose.position.y)
                                       / dt; 
    robot_state.twist.twist.angular.z = angle_diff(2 * std::acos(robot_state.pose.pose.orientation.w),
                                                   2 * std::acos(this->last_states.vector.back().pose.pose.orientation.w))
                                       / dt;    
  }

  /* We expect the orientation to only change about z,
   * hence small values for x and y are asserted */
  assert(robot_state.pose.pose.orientation.x < 0.1 && "Quaternion x should not be greater than 0.1");
  assert(robot_state.pose.pose.orientation.y < 0.1 && "Quaternion y should not be greater than 0.1");

  /* Put the state into our filter window for next time.*/
  this->last_states.push(robot_state);
  return robot_state;
}

void SlidingWindowFilter::publish_observed_state()
{	
	/* Publish the messgae */
	this->state_pub->publish(this->curr_observed_state);
}

int main(int argc, char const *argv[])
{

  /* Initialize ros, if it has not already been initialized. */
  if (!rclcpp::ok())
  {
    rclcpp::init(argc, argv);
  }

  rclcpp::spin(std::make_shared<SlidingWindowFilter>());

  return 0;
}
