#ifndef CONTROL_COMMON_H
#define CONTROL_COMMON_H

#include <vector>
#include <ignition/math.hh>

/* Seconds to Nanoseconds conversion factor */
const unsigned int S2NS = 1000000000;

/* Helper struct to encapsulate a instantaneous 
 * state of the robot. */
struct RobotState {
  double x,     y,     theta,
         x_dot, y_dot, theta_dot;
  double rw_velocity;
  unsigned int sec, nanosec;

  RobotState(): x(0), y(0), theta(0),
                x_dot(0), y_dot(0), theta_dot(0),
                rw_velocity(0),
                sec(0), nanosec(0){}
};

template <typename T>
class FixedSizeVector
{
    public:
        FixedSizeVector()
        {
            this->vector = std::vector<T>();
        }

        FixedSizeVector(unsigned int max_len): max_len(max_len)
        {
            this->vector = std::vector<T>();
        }

        void push(const T& value) {
            if (this->vector.size() == this->max_len) {
               this->vector.erase(this->vector.begin());
            }
            this->vector.push_back(value);
        }

        std::vector<T> vector;

    private:
        unsigned int max_len;
};

double norm(double x, double y);

double eucl_dist(double x1, double y1,
                 double x2, double y2);

double rad2deg(double rad);

double round5( double val );

double angle_diff(double ang1, double ang2);

double normalize_angle(double angle);

double to_seconds(unsigned int sec, unsigned int nanosec);

void to_sec_nsec(double t, unsigned int & sec, unsigned int & nanosec);
#endif //CONTROL_COMMON_H