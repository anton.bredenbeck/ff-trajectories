#ifndef MODULATOR_H
#define MODULATOR_H

/* Virtual class all implemented modulators inherent from to provide
 * a common interface */
class Modulator{
  public:
    virtual double modulate_continuous_force(double force, double t) = 0;  
    virtual ~Modulator() = default;
};

#endif //MODULATOR_H