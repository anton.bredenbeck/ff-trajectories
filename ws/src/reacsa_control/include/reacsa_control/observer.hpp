#ifndef OBSERVER_H
#define OBSERVER_H

#include <Eigen/Core>
#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>

#include "rclcpp/rclcpp.hpp"
#include "std_srvs/srv/empty.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "geometry_msgs/msg/twist_with_covariance.hpp"

#include "common.hpp"
#include "reacsa_interfaces/msg/force.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/msg/motor_state.hpp"
#include "reacsa_interfaces/msg/state.hpp"

/* Virtual class all implemented controllers inherent from to provide
 * a common interface */
class Observer : public rclcpp::Node {
  public:
    Observer(std::string name):
    	rclcpp::Node(name)
    {
      /* Init the subscribers, publisher and service client */
      this->pose_sub = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>(
        "/vicon/reacsa/reacsa", /* QoS */ rclcpp::SensorDataQoS(),
        std::bind(&Observer::pose_callback, this, std::placeholders::_1));
      this->motor_sub = this->create_subscription<reacsa_interfaces::msg::MotorState>(
        "/motor_state",  /* QoS */ rclcpp::SensorDataQoS(),
         std::bind(&Observer::motor_callback, this, std::placeholders::_1));
      this->force_sub = this->create_subscription<reacsa_interfaces::msg::Force>(
        "/set_force",  /* QoS */ rclcpp::SensorDataQoS(),
         std::bind(&Observer::force_callback, this, std::placeholders::_1));
      this->torque_sub = this->create_subscription<reacsa_interfaces::msg::Torque>(
        "/set_torque",  /* QoS */ rclcpp::SensorDataQoS(),
         std::bind(&Observer::torque_callback, this, std::placeholders::_1));
      
      /* Find the frequency parameter */
      this->declare_parameter("observer_interval");
      this->dt = this->get_parameter("observer_interval").as_double();
      this->declare_parameter("rw_limits");
      this->rw_limits = this->get_parameter("rw_limits").as_double_array();

      /* Init the publisher */
      this->state_pub = this->create_publisher<reacsa_interfaces::msg::State>("/observed_state",
                                                               /* QoS */ rclcpp::SensorDataQoS());

      /* Init the service server for resetting the oserver */
      server = this->create_service<std_srvs::srv::Empty>("reset_observer",
          std::bind(&Observer::reset, this, std::placeholders::_1, std::placeholders::_2));

      /* Init the timer that regularly calls the cmpt function */
      cmpt_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(this->dt * 1e3)),
            std::bind(&Observer::cmpt_observed_state, this));
      /* Init the timer that regularly calls the publish function */
      pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(this->dt * 1e3)),
            std::bind(&Observer::publish_observed_state, this));
    }

    double get_dt();
    void pause_timer();
    void restart_timer();

    /* Publisher all sub-classes can use to publish their estimated state */
    rclcpp::Publisher<reacsa_interfaces::msg::State>::SharedPtr state_pub;

    /* Most recent measurements and commands */
    reacsa_interfaces::msg::State curr_meas_state;
    reacsa_interfaces::msg::Force curr_force_cmd;
    reacsa_interfaces::msg::Torque curr_torque_cmd;

  private:
    void pose_callback(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg);
    void motor_callback(const reacsa_interfaces::msg::MotorState::SharedPtr msg);
    void force_callback(const reacsa_interfaces::msg::Force::SharedPtr msg);
    void torque_callback(const reacsa_interfaces::msg::Torque::SharedPtr msg);

    /* Function that packs the current estimate and publishes it. Needs to be implemented
     * by all subclasses of Observer */
    virtual void publish_observed_state() = 0;

    /* Function that computes the current observed state. Needs to be implemented
     * by all subclasses of Observer*/
    virtual void cmpt_observed_state() = 0;

    /* Function that resets the observer to the most recent measurement. Needs 
     * to be implemented by all subclasses of Observer */
    virtual void reset(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                       std::shared_ptr<std_srvs::srv::Empty::Response> response) = 0;

    /*****************************************************************************************
     ***************************** Class Variables *******************************************
     *****************************************************************************************/
    rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr pose_sub;
    rclcpp::Subscription<reacsa_interfaces::msg::MotorState>::SharedPtr motor_sub;
    rclcpp::Subscription<reacsa_interfaces::msg::Force>::SharedPtr force_sub;
    rclcpp::Subscription<reacsa_interfaces::msg::Torque>::SharedPtr torque_sub;
    rclcpp::Service<std_srvs::srv::Empty>::SharedPtr server;

    rclcpp::TimerBase::SharedPtr pub_timer, cmpt_timer;

    double dt; // Observer Interval
    std::vector<double> rw_limits; // Reaction Wheel velocity limits
    const double RPM_BUFFER = 0.523598775; // 5 RPM
    /******************************************************************************************/
};

#endif //OBSERVER_H