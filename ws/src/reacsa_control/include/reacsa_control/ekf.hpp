#ifndef EKF_H
#define EKF_H

#include "observer.hpp"

/* Define constant control dimensions */
const size_t STATE_DIM = 5;
const size_t SO2_DIM = 4;
const size_t CONTROL_DIM = 9;
const size_t OUTPUT_DIM = 5;
const size_t SO2_OUTPUT = 2;

class EKF: public Observer 
{
public:
	EKF():Observer("ekf")
	{
		/* Get System Parameters */
      	this->declare_parameter("m");
      	this->declare_parameter("r");
      	this->declare_parameter("Ib");
      	this->declare_parameter("Iw");
      	this->declare_parameter("path");

      	this->m = this->get_parameter("m").as_double();
      	this->r = this->get_parameter("r").as_double();
      	this->Ib = this->get_parameter("Ib").as_double();
      	this->Iw = this->get_parameter("Iw").as_double();
      	this->path = this->get_parameter("path").as_string();

		/* Define the discrete statespace matrix */
	    A_ekf << 1, 0, this->get_dt(), 0, 0,
	             0, 1, 0, this->get_dt(), 0,
	             0, 0, 1, 0, 0,
	             0, 0, 0, 1, 0,
	             0, 0, 0, 0, 1;

	    double dt = this->get_dt();
	    B_so2 << -dt*dt*0.5/Ib,  dt*dt*0.5*r/Ib, -dt*dt*0.5*r/Ib, dt*dt*0.5*r/Ib, -dt*dt*0.5*r/Ib, dt*dt*0.5*r/Ib, -dt*dt*0.5*r/Ib,  dt*dt*0.5*r/Ib, -dt*dt*0.5*r/Ib,
                 -dt/Ib,  dt*r/Ib, -dt*r/Ib, dt*r/Ib, -dt*r/Ib, dt*r/Ib, -dt*r/Ib, dt*r/Ib, -dt*r/Ib;
                

	    /* Define Observation Matrix, we pretend to observe
	     * all states, but in actuality do not observe the 
	     * velocities but compute those numerically from the 
	     * recent position estimates as "pseudomeasurements" 
	     * and give that sensor a high variance. */
	    dHdx = Eigen::Matrix<double, OUTPUT_DIM, STATE_DIM>::Identity();

        /* Assume constant noise derivative */
        dFdw = Eigen::Matrix<double, STATE_DIM, STATE_DIM>::Identity();
        dHdw = Eigen::Matrix<double, OUTPUT_DIM, OUTPUT_DIM>::Identity();

        /* SO2 transition matrix */
        F_so2 << 1, dt, 
                 0,  1;

        /* Read all the Weight matrices*/
        this->read_weights(this->path);

	    SO2_est << std::cos(0), -std::sin(0), 0, 0,
	               std::sin(0),  std::cos(0), 0, 0,
	                         0,            0, 1, 0,
	                         0,            0, 0, 1;
	    /* Init state estimate and covariance matrix */
	    curr_est = Eigen::Matrix<double, STATE_DIM, 1>::Zero();
	    covariance = 0.1 * Eigen::Matrix<double, STATE_DIM, STATE_DIM>::Identity();
	    SO2_covariance << 0.1, 0,
	                      0, 0.1;

	    /* Init last states and stamp */
	    this->last_states = FixedSizeVector<Eigen::Matrix<double, STATE_DIM, 1>>(2);
	    this->last_thetas = FixedSizeVector<double>(2);
	}

private:
	void publish_observed_state() override;
	void cmpt_observed_state() override;
	void reset(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                     std::shared_ptr<std_srvs::srv::Empty::Response> response) override;
	void read_weights(std::string path);
    void read_matrix(std::string file, Eigen::Ref<Eigen::MatrixXd>  mat);

	double get_current_theta(){return this->SO2_est.log()(1, 0);}
	double get_current_omega(){return this->SO2_est(2, 3);}
	void predict(Eigen::Matrix<double, CONTROL_DIM, 1> curr_ctrl);
	void update(Eigen::Matrix<double, OUTPUT_DIM, 1> curr_meas, 
                Eigen::Matrix<double, SO2_OUTPUT, 1> so2_meas);

	/* Control Matrices For EKF */
    Eigen::Matrix<double, STATE_DIM, STATE_DIM> A_ekf;
    Eigen::Matrix<double, STATE_DIM, STATE_DIM> dFdx; // Diff of dynamics f(x,u) with respect to x
    Eigen::Matrix<double,STATE_DIM, STATE_DIM> dFdw; // Diff of dynamics f(x,u) with respect to noise w
    Eigen::Matrix<double, OUTPUT_DIM, STATE_DIM> dHdx; // Diff of the output function h(x, w) with respect to x
    Eigen::Matrix<double, OUTPUT_DIM, OUTPUT_DIM> dHdw; // Diff of the output function h(x, w) with respect to w
    Eigen::Matrix<double, STATE_DIM, CONTROL_DIM> B_ekf;
    Eigen::Matrix<double, SO2_OUTPUT, CONTROL_DIM> B_so2;
    
    Eigen::Matrix<double, SO2_OUTPUT, SO2_OUTPUT> F_so2; // Transition Matrix
  		
    Eigen::Matrix<double, STATE_DIM, STATE_DIM> Q_ekf;
    Eigen::Matrix<double, SO2_OUTPUT, SO2_OUTPUT> Q_so2;
    Eigen::Matrix<double, OUTPUT_DIM, OUTPUT_DIM> R_ekf;
    Eigen::Matrix<double, SO2_OUTPUT, SO2_OUTPUT> R_so2;

    /* Current Kalman filter values required */
    Eigen::Matrix<double, STATE_DIM, 1> curr_est;
    Eigen::Matrix<double, SO2_DIM, SO2_DIM> SO2_est;
    Eigen::Matrix<double, STATE_DIM, STATE_DIM> covariance;
    Eigen::Matrix<double, SO2_OUTPUT, SO2_OUTPUT> SO2_covariance;
    
    /* System Parameters */
    double m, r, Ib, Iw;
    std::string path;

    /* Remember recent state estimates */
    FixedSizeVector<Eigen::Matrix<double, STATE_DIM, 1>> last_states;
    FixedSizeVector<double> last_thetas;
};

#endif //EKF_H