#ifndef TRAJECTORY_FOLLOWER_H
#define TRAJECTORY_FOLLOWER_H

#include <drake/common/copyable_unique_ptr.h>
#include <drake/common/trajectories/piecewise_polynomial.h>
#include <drake/systems/primitives/trajectory_affine_system.h>
#include <drake/systems/controllers/linear_quadratic_regulator.h>
#include <drake/systems/controllers/finite_horizon_linear_quadratic_regulator.h>

#include "controller.hpp"
#include "reacsa_interfaces/srv/prepare_trajectory.hpp"
#include "reacsa_actions/action/follow_trajectory.hpp"

#include "sigma_delta_modulator.hpp"

/* A node that implements a trajectory following controller.
 * The node subscribes to the pose topic 
 *  /vicon/pose
 * and has a publisher/client for the topic/services
 *  /set_torque
 *  /set_force 
 * Using this it offers an action that when started moves the robot 
 * along the specified trajectory which is loaded from a specified file.
 * 
 * The implementation is based on a model predictive control approach
 * that solves the LQR problem at each discrete control time and finds
 * the optimal state feedbback gain to control the pose towards the 
 * trajectory 
 */

/* Define constant control dimensions */
const size_t STATE_DIM = 7;
const size_t CONTROL_DIM = 9;

class TrajectoryFollower : public Controller
{
  public:

    using ControlAction = reacsa_actions::action::FollowTrajectory;
    using GoalHandle = rclcpp_action::ServerGoalHandle<ControlAction>;
 
    TrajectoryFollower():
    Controller("trajectory_follower")
    {
      /* Declare all parameters required */
      /* Control Parameters */
      this->declare_parameter("filter_size");
      this->declare_parameter("epsilon_position");
      this->declare_parameter("dot_epsilon_position");
      this->declare_parameter("epsilon_orientation");
      this->declare_parameter("dot_epsilon_orientation");
      this->declare_parameter("path");

      /* System Parameters */
      this->declare_parameter("f"); // Nominal Thruster Force
      this->declare_parameter("tau"); // Maximal torque 
      this->declare_parameter("k_sd"); // Sigma Delta Gain
      this->declare_parameter("eps"); // Sigma Delta Threshold
      this->declare_parameter("Iw"); // Reaction Wheel moment of inertia
      this->declare_parameter("m"); // Mass of the overall system
      this->declare_parameter("r"); // Radius of the overall system
      this->declare_parameter("Ib"); // Body moment of inertia

      /* Using the parameters to init the class variables */
      this->epsilon_position = this->get_parameter("epsilon_position").as_double();
      this->dot_epsilon_position = this->get_parameter("dot_epsilon_position").as_double();
      this->epsilon_orientation = this->get_parameter("epsilon_orientation").as_double();
      this->dot_epsilon_orientation = this->get_parameter("dot_epsilon_orientation").as_double();
      this->path = this->get_parameter("path").as_string();

      this->f = this->get_parameter("f").as_double();
      this->tau = this->get_parameter("tau").as_double();
      this->k_sd = this->get_parameter("k_sd").as_double();
      this->eps = this->get_parameter("eps").as_double();
      this->Iw = this->get_parameter("Iw").as_double();
      this->m = this->get_parameter("m").as_double();
      this->r = this->get_parameter("r").as_double();
      this->Ib = this->get_parameter("Ib").as_double();

      /* Init the service server that advertises the service of preparing the trajectory
       * i.e. precompute all the feedback gain Matrices K */
      prepare_trajectory_server = this->create_service<reacsa_interfaces::srv::PrepareTrajectory>("prepare_trajectory",
                std::bind(&TrajectoryFollower::prepare_trajectory_callback,
                          this, std::placeholders::_1, std::placeholders::_2));
      /* Init the action server that advertises the total control action */
      control_action_server = rclcpp_action::create_server<ControlAction>(
        this,
        "follow_trajectory",
        std::bind(&TrajectoryFollower::handle_goal, this, std::placeholders::_1, std::placeholders::_2),
        std::bind(&TrajectoryFollower::handle_cancel, this, std::placeholders::_1),
        std::bind(&TrajectoryFollower::handle_accepted, this, std::placeholders::_1));

      /* Compute the control matrices that remain constant */
      Eigen::Matrix<double,STATE_DIM, STATE_DIM>  a;
      a << 0, 0, 0, 1, 0, 0, 0,
           0, 0, 0, 0, 1, 0, 0,
           0, 0, 0, 0, 0, 1, 0,
           0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0,
           0, 0, 0, 0, 0, 0, 0;     
      this->A = drake::trajectories::PiecewisePolynomial<double>(a);

      this->C = drake::trajectories::PiecewisePolynomial<double>(
        Eigen::Matrix<double,STATE_DIM, STATE_DIM>::Identity());
      this->D = drake::trajectories::PiecewisePolynomial<double>(
        Eigen::Matrix<double, STATE_DIM, CONTROL_DIM>::Zero());
      this->f0 = drake::trajectories::PiecewisePolynomial<double>(
        Eigen::Matrix<double, STATE_DIM, 1>::Zero());
      this->y0 = drake::trajectories::PiecewisePolynomial<double>(
        Eigen::Matrix<double, STATE_DIM, 1>::Zero());

      /* Define the cost matrices.*/
      this->read_weights(this->path);

      /* Init modulators for all thrusters */
      for(unsigned int i = 0; i < CONTROL_DIM - 1; i++) this->modulators.push_back(SigmaDeltaModulator(this->k_sd, this->eps));
    }

  private:

    /* Service related binding function */
    void prepare_trajectory_callback(
      const std::shared_ptr<reacsa_interfaces::srv::PrepareTrajectory::Request> request,
            std::shared_ptr<reacsa_interfaces::srv::PrepareTrajectory::Response> response);

    void compute_control() override;
    void trigger_state() override;
    /* We override the publish function to separate the 
     * SigDel Input (in compute control) and its reading 
     * (in this function) */
    void publish_force() override;

    /* Function that sets all current control commands to zero */
    void turn_off_actuators();

    void precompute_ks();
    void read_weights(std::string path);
    void read_matrix(std::string file, Eigen::Ref<Eigen::MatrixXd>  mat);
    void load_trajectory(std::string state_filename,
                         std::string ctrl_filename,
                         std::string time_filename);
    Eigen::Matrix<double, STATE_DIM, 1> closest_pose(unsigned int sec, unsigned int nanosec);

    rclcpp_action::GoalResponse handle_goal(const rclcpp_action::GoalUUID & uuid,
                                            std::shared_ptr<const ControlAction::Goal> goal);
    rclcpp_action::CancelResponse handle_cancel(const std::shared_ptr<GoalHandle> goal_handle);
    void handle_accepted(const std::shared_ptr<GoalHandle> goal_handle);
    void execute(const std::shared_ptr<GoalHandle> goal_handle);

    /*****************************************************************************************
     ***************************** Class Variables *******************************************
     *****************************************************************************************/
    rclcpp_action::Server<ControlAction>::SharedPtr control_action_server;
    rclcpp::Service<reacsa_interfaces::srv::PrepareTrajectory>::SharedPtr prepare_trajectory_server;
    
    drake::trajectories::PiecewisePolynomial<double> state_trajectory;
    drake::trajectories::PiecewisePolynomial<double> ctrl_trajectory;
    unsigned int curr_idx; // current index along the trajectory
    unsigned int traj_start_sec, traj_start_nanosec;
    double epsilon_orientation, epsilon_position, dot_epsilon_orientation, dot_epsilon_position;

    double k_sd, eps, f, tau, Iw; // SigDel-Gain and threshold, Nominal Force, max torque, Motor/RW parameters
    double m, r, Ib; // Overall system mass, radius and inertia
    std::string path; // Path to weight matrices
    bool trajectory_prepared = false; // Bool that is true when the feedback matrices are computed

    /* Control Matrices For LQR */
    drake::trajectories::PiecewisePolynomial<double> A, B, C, D, f0, y0;

    Eigen::Matrix<double, STATE_DIM, STATE_DIM> Q, Q_final;
    Eigen::Matrix<double, CONTROL_DIM, CONTROL_DIM> R, R_final;

    drake::copyable_unique_ptr<drake::trajectories::Trajectory<double>> K;
    drake::copyable_unique_ptr<drake::trajectories::Trajectory<double>> k0;

    /* Modulators for all thrusters */
    std::vector<SigmaDeltaModulator> modulators;
    /******************************************************************************************/
}; 

#endif //TRAJECTORY_FOLLOWER_H