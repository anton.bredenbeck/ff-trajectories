#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <vector>
#include <algorithm>
#include <array>
#include <ignition/math.hh>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "message_filters/subscriber.h"
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "std_msgs/msg/float64.hpp"

#include "common.hpp"
#include "reacsa_interfaces/msg/force.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/msg/state.hpp"
/* Definition of state machine states */
enum CtrlState {IDLE, HEADING_CTRL, POSE_CTRL};

/* Virtual class all implemented controllers inherent from to provide
 * a common interface */
class Controller : public rclcpp::Node {
  public:
    Controller(std::string name):
    	rclcpp::Node(name), 
    	state(IDLE)
    {
      /* Find Inteval */
      this->declare_parameter("control_comp_interval");
      this->declare_parameter("f_pub_interval");
      this->declare_parameter("tau_pub_interval");
      this->declare_parameter("rw_offset");
      /* Interval at which the control is computed. Should be min(f_pub_interval, tau_pub_inteval) */
      this->control_comp_interval = this->get_parameter("control_comp_interval").as_double();
      /* Interval at which the respective control commands are published */
      this->f_pub_interval = this->get_parameter("f_pub_interval").as_double();
      this->tau_pub_interval = this->get_parameter("tau_pub_interval").as_double();
      /* Read the RPM value and transform it to rad/s */
      this->rw_offset = 2 * M_PI / 60.0 * this->get_parameter("rw_offset").as_double(); 
      /* Init the subscribers, publisher and service client */
      this->state_sub = this->create_subscription<reacsa_interfaces::msg::State>(
                  "/observed_state",
                  /* QoS */ rclcpp::SensorDataQoS(),
                  std::bind(&Controller::state_callback, this, std::placeholders::_1));

      /* Init the actuator instances */
      set_force_pub = this->create_publisher<reacsa_interfaces::msg::Force>("/set_force", 
                                                                            /* QoS */ rclcpp::SensorDataQoS());
      set_torque_pub = this->create_publisher<reacsa_interfaces::msg::Torque>("/set_torque", 
                                                                            /* QoS */ rclcpp::SensorDataQoS());
      
      /* Init the timer that regularly calls the compute control function */
      control_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(this->control_comp_interval * 1e3)),
            std::bind(&Controller::compute_control, this));

      /* Init the timer that regularly publishes the force messages */
      f_pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(this->f_pub_interval * 1e3)),
            std::bind(&Controller::publish_force, this));
      /* Init the timer that regularly publishes the torque messages */
      tau_pub_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(this->tau_pub_interval * 1e3)),
            std::bind(&Controller::publish_torque, this));

    }

    double find_desired_heading(const RobotState curr_state, const RobotState curr_goal, double epsilon = 0.1) const;

    RobotState get_most_recent_robot_state();

    /* Accessors and mutators for the private class variables */
    void set_state(CtrlState state) {this->state = state;}
    void set_current_torque_command(reacsa_interfaces::msg::Torque cmd){this->curr_torque_cmd = cmd;}
    void set_current_force_command(reacsa_interfaces::msg::Force cmd){this->curr_force_cmd = cmd;}

    CtrlState get_state() {return this->state;}

    /* Callbacks for the publishing timers */
    virtual void publish_force();
    virtual void publish_torque();
    
    rclcpp::Publisher<reacsa_interfaces::msg::Torque>::SharedPtr get_torque_pub(){return this->set_torque_pub;}
    rclcpp::Publisher<reacsa_interfaces::msg::Force>::SharedPtr get_force_pub(){return this->set_force_pub;}

  private:

    void state_callback(const reacsa_interfaces::msg::State::SharedPtr state_msg);

    /* Function that computes the current control action and 
     * stores it in the class variables. To be implemented by each 
     * controller that inherents from this base class */
    virtual void compute_control() = 0;

    /* Function that defines in which order and how the controller
     * transfers between states. It is called initially when the 
     * controller (which is initialized as IDLE) recieves an 
     * action request. To be implemented by each 
     * controller that inherents from this base class */
    virtual void trigger_state() = 0;

    /*****************************************************************************************
     ***************************** Class Variables *******************************************
     *****************************************************************************************/
    rclcpp::Subscription<reacsa_interfaces::msg::State>::SharedPtr state_sub;
    rclcpp::Publisher<reacsa_interfaces::msg::Torque>::SharedPtr set_torque_pub;
    rclcpp::Publisher<reacsa_interfaces::msg::Force>::SharedPtr set_force_pub;

    CtrlState state;
    reacsa_interfaces::msg::State robot_state;
    reacsa_interfaces::msg::Force curr_force_cmd;
    reacsa_interfaces::msg::Torque curr_torque_cmd;
    double control_comp_interval, f_pub_interval,
           tau_pub_interval, rw_offset;

    rclcpp::TimerBase::SharedPtr control_timer, f_pub_timer, tau_pub_timer;

    /******************************************************************************************/
};

#endif //CONTROLLER_H