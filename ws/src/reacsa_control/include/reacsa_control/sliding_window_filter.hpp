#ifndef SLIDING_WINDOW_FILTER_H
#define SLIDING_WINDOW_FILTER_H

#include "observer.hpp"

class SlidingWindowFilter: public Observer 
{
public:
	SlidingWindowFilter():Observer("sliding_window_filter")
	{
		this->declare_parameter("filter_size");
		this->init_last_states((unsigned int)(this->get_parameter("filter_size").as_int()));
	}
private:

    void init_last_states(unsigned int size) {this->last_states = FixedSizeVector<reacsa_interfaces::msg::State>(size);}

	reacsa_interfaces::msg::State compute_vel();

	void publish_observed_state() override;
	void cmpt_observed_state() override;
	void reset(const std::shared_ptr<std_srvs::srv::Empty::Request> request,
                     std::shared_ptr<std_srvs::srv::Empty::Response> response) override;

    FixedSizeVector<reacsa_interfaces::msg::State> last_states;
    reacsa_interfaces::msg::State curr_observed_state;
};

#endif //SLIDING_WINDOW_FILTER_H