#ifndef SIGMA_DELTA_MODULATOR_H
#define SIGMA_DELTA_MODULATOR_H


#include "modulator.hpp"

/* A sigma delta modulator that modulates the continuous force onto the 
 * binary, on / off actuator */
class SigmaDeltaModulator : public Modulator
{
  public:
    SigmaDeltaModulator(double k, double eps) : K(k), EPS(eps)  {
      this->t_last = 0;
      this->integrator_value = 0;
      this->current_output = 0;
    };

    void give_input(double force, double t);
    double read_output();
    double modulate_continuous_force(double force, double t);
    void reset();

  private:  

    const double K, EPS; // Gain in the feedforward path and threshold to trigger puls
    double t_last; // The timestamp from the last sampled value
    double integrator_value; // The integrator value the error is accumulated at
    double current_output; // Current output of the modulator
};

#endif //SIGMA_DELTA_MODULATOR_H