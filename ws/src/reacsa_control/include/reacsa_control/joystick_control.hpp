#ifndef JOYSTICK_CONTROL_H
#define JOYSTICK_CONTROL_H

#include <ignition/math.hh>
#include <chrono>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include "geometry_msgs/msg/pose_with_covariance_stamped.hpp"
#include "sensor_msgs/msg/joy.hpp"

#include "reacsa_actions/action/follow_trajectory.hpp"
#include "reacsa_actions/action/compute_trajectory.hpp"
#include "reacsa_interfaces/msg/force.hpp"
#include "reacsa_interfaces/msg/torque.hpp"
#include "reacsa_interfaces/srv/prepare_trajectory.hpp"

using namespace std::chrono_literals;

enum Direction {FRONT, BACK, RIGHT, LEFT, CW, CCW, OFF};

class JoystickControl : public rclcpp::Node{

public:

	using FollowTrajectory = reacsa_actions::action::FollowTrajectory;
  using GoalHandleFT = rclcpp_action::ClientGoalHandle<FollowTrajectory>;
  using ComputeTrajectory = reacsa_actions::action::ComputeTrajectory;
  using GoalHandleCT = rclcpp_action::ClientGoalHandle<ComputeTrajectory>;

	JoystickControl(): Node("joystick_control") 
	{
		/* Add subscriber for joy messages */
		this->joy_sub = this->create_subscription<sensor_msgs::msg::Joy>("/joy", 10,
                                      std::bind(&JoystickControl::joy_callback,
                                                this,
                                                std::placeholders::_1));
		/* Add subscriber for joy messages */
		this->pose_sub = this->create_subscription<geometry_msgs::msg::PoseWithCovarianceStamped>("/vicon/pose", 10,
                                      std::bind(&JoystickControl::pose_callback,
                                                this,
                                                std::placeholders::_1));
		/* Add Clients */
		this->cmp_traj = rclcpp_action::create_client<ComputeTrajectory>(this,"/compute_trajectory");
		this->prep_traj = this->create_client<reacsa_interfaces::srv::PrepareTrajectory>("/prepare_trajectory");
		this->follow_traj = rclcpp_action::create_client<FollowTrajectory>(this, "/trajectory_follower");

		while(!this->prep_traj->wait_for_service(1s))
		{
			RCLCPP_INFO(this->get_logger(), "/prepare_trajectory not available, waiting again... ");
		}

		/* Init the actuator instances */
    this->set_force_pub = this->create_publisher<reacsa_interfaces::msg::Force>("/set_force", 10);
    this->set_torque_pub = this->create_publisher<reacsa_interfaces::msg::Torque>("/set_torque", 10);
	  
    /* Get Parameters */
	  this->declare_parameter("f"); // Nominal Thruster Force
	  this->declare_parameter("tau"); // Maximal torque     

    this->f = this->get_parameter("f").as_double();
    this->tau = this->get_parameter("tau").as_double();
	}

private: 

	/* Subscriber callback function*/
	void joy_callback(const sensor_msgs::msg::Joy::SharedPtr msg);
	void pose_callback(const geometry_msgs::msg::PoseWithCovarianceStamped::SharedPtr msg);
	/* Action callback functions*/
	void ft_goal_response_callback(std::shared_future<GoalHandleFT::SharedPtr> future);
	void ft_feedback_callback(GoalHandleFT::SharedPtr, const std::shared_ptr<const FollowTrajectory::Feedback> feedback);
	void ft_result_callback(const GoalHandleFT::WrappedResult & result);
	void ct_goal_response_callback(std::shared_future<GoalHandleCT::SharedPtr> future);
	void ct_feedback_callback(GoalHandleCT::SharedPtr, const std::shared_ptr<const ComputeTrajectory::Feedback> feedback);
	void ct_result_callback(const GoalHandleCT::WrappedResult & result);

	/******************* Class Variables ******************************************/
  rclcpp_action::Client<FollowTrajectory>::SharedPtr follow_traj; 
  rclcpp_action::Client<ComputeTrajectory>::SharedPtr cmp_traj;
  rclcpp::Client<reacsa_interfaces::srv::PrepareTrajectory>::SharedPtr prep_traj;
  
  rclcpp::Subscription<sensor_msgs::msg::Joy>::SharedPtr joy_sub;
  rclcpp::Subscription<geometry_msgs::msg::PoseWithCovarianceStamped>::SharedPtr pose_sub;
  rclcpp::Publisher<reacsa_interfaces::msg::Torque>::SharedPtr set_torque_pub;
  rclcpp::Publisher<reacsa_interfaces::msg::Force>::SharedPtr set_force_pub;

  /* Current position and Nominal force and maximal torque */
  double curr_x, curr_y, curr_theta,
         f, tau; 
};

#endif //JOYSTICK_CONTROL_H