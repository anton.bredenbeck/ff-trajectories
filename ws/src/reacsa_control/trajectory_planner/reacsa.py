"""Drake model of the reacsa system."""
import numpy as np


class Reacsa(object):

    def __init__(
            self,
            mass,  # Mass of the overall system
            r,  # Radius of the system
            body_i,  # Inertia of the overall system
            rw_i,  # Inertia of the reaction wheel
            nom_f  # Nominal force produced by the thrusters
    ):
        self.mass = mass
        self.r = r
        self.body_i = body_i
        self.rw_i = rw_i
        self.nom_f = nom_f

    def __init__(self):
        self.mass = 221.670  # kg
        self.r = 0.35  # m
        self.body_i = 12.129  # kgm^2
        self.nom_f = 10.36  # N
        self.rw_i = 0.047  # kgm^2

    def continuous_dynamics(self, x, u) -> np.array:
        """Function that computes the state derivative of the system
           i.e. the dynamics of the form x_dot =  f(x,u). The state is
           x = [x y theta, x_dot, y_dot, theta_dot, rw_vel, current]"""

        # Extract controls as variables
        tau = u[0]
        u1a = self.nom_f * u[1]
        u1b = self.nom_f * u[2]
        u2a = self.nom_f * u[3]
        u2b = self.nom_f * u[4]
        u3a = self.nom_f * u[5]
        u3b = self.nom_f * u[6]
        u4a = self.nom_f * u[7]
        u4b = self.nom_f * u[8]

        # Consider current state
        theta = x[2]
        st = np.sin(theta)
        ct = np.cos(theta)

        # Compute derivates
        x_ddot = (1.0 / self.mass) * (- u1a * st + u1b * st
                                      - u2a * ct + u2b * ct
                                      + u3a * st - u3b * st
                                      + u4a * ct - u4b * ct)
        y_ddot = (1.0 / self.mass) * (u1a * ct - u1b * ct
                                      - u2a * st + u2b * st
                                      - u3a * ct + u3b * ct
                                      + u4a * st - u4b * st)
        theta_body_ddot = (1 / self.body_i) * (- tau
                                               + self.r * (u1a - u1b
                                                           + u2a - u2b
                                                           + u3a - u3b
                                                           + u4a - u4b))
        rw_dot = tau / self.rw_i

        # Stack the derivative vector
        dx = np.array([x[3], x[4], x[5], x_ddot, y_ddot,
                       theta_body_ddot, rw_dot])

        # and return
        return dx
