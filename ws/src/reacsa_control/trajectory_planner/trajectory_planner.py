#!/usr/bin/env python3
# Fix to import the files in the current folder
import sys
sys.path.append("./src/reacsa_control/trajectory_planner/")
from reacsa_actions.action import ComputeTrajectory
from rclpy.action import ActionServer
from rclpy.node import Node
import rclpy
import matplotlib.pyplot as plt
import numpy as np
from reacsa import Reacsa
from trajectory_optimizer import TrajectoryOptimizer
import time


class TrajectoryPlanner(Node):
    def __init__(self,
                 xmin=-np.array([100, 100, 100, 100, 100,
                                 100, 250 * 2 * np.pi / 60]),
                 xmax=np.array([100, 100, 100, 100, 100,
                                100, 250 * 2 * np.pi / 60]),
                 umin=- np.array([1.44, 0, 0, 0, 0, 0, 0, 0, 0]),
                 umax=np.array([1.44, 1, 1, 1, 1, 1, 1, 1, 1]),
                 alpha=12.0,
                 plotting=False):

        # Init ROS2 related things
        super().__init__('trajectory_planner')
        self.action_server =\
            ActionServer(self,
                         ComputeTrajectory, '/compute_trajectory',
                         self.compute_trajectory_callback)

        # Init Trajectory related quantities
        self._xmin = xmin
        self._xmax = xmax
        self._umin = umin
        self._umax = umax
        self._alpha = alpha

        # Whether or not the trajectory should be plotted
        self._plotting = plotting

    def normalize_angle(self, angle):
        return np.arctan2(np.sin(angle), np.cos(angle))

    def compute_trajectory_callback(self, goal_handle):
        # Logging

        x_init = goal_handle.request.init.pose.pose.position.x
        y_init = goal_handle.request.init.pose.pose.position.y
        theta_init = self.normalize_angle(
            2 * np.arccos(goal_handle.request.init
                          .pose.pose.orientation.w))
        x_dot_init = goal_handle.request.init.twist.twist.linear.x
        y_dot_init = goal_handle.request.init.twist.twist.linear.y
        theta_dot_init = goal_handle.request.init.twist.twist.angular.z
        rw_init = goal_handle.request.init.rw_vel

        x_final = goal_handle.request.final.pose.pose.position.x
        y_final = goal_handle.request.final.pose.pose.position.y
        theta_final = self.normalize_angle(
            2 * np.arccos(goal_handle.request.final
                          .pose.pose.orientation.w))
        x_dot_final = goal_handle.request.final.twist.twist.linear.x
        y_dot_final = goal_handle.request.final.twist.twist.linear.y
        theta_dot_final = goal_handle.request.final.twist.twist.angular.z
        rw_final = goal_handle.request.final.rw_vel

        self.get_logger().info("Received request! Computing Trajectory from "
                               + f"[{x_init:.4}, "
                               + f"{y_init:.4}, "
                               + f"{theta_init:.4}, "
                               + f"{x_dot_init:.4}, "
                               + f"{y_dot_init:.4}, "
                               + f"{theta_dot_init:.4}, "
                               + f"{rw_init:.4}]"
                               + " to "
                               + f"[{x_final:.4}, "
                               + f"{y_final:.4}, "
                               + f"{theta_final:.4}, "
                               + f"{x_dot_final:.4}, "
                               + f"{y_dot_final:.4}, "
                               + f"{theta_dot_final:.4}, "
                               + f"{rw_final:.4}]")
        start = time.time()

        # Extract the variables from the request
        x0 = np.array([x_init,
                       y_init,
                       theta_init,
                       x_dot_init,
                       y_dot_init,
                       theta_dot_init,
                       rw_init])
        xf = np.array([x_final,
                       y_final,
                       theta_final,
                       x_dot_final,
                       y_dot_final,
                       theta_dot_final,
                       rw_final])
        N = goal_handle.request.num_points

        # Initialize the optimization problem
        opti = TrajectoryOptimizer(Reacsa())

        # Find time optimal trajectort
        t_tstar, _, _ = opti.time_optimal_trajectory(x0, xf,
                                                     self._xmin, self._xmax,
                                                     self._umin, self._umax,
                                                     int(N / 10))

        # Init the response
        response = ComputeTrajectory.Result()
        # The solver didn't converge so we don't even have
        # to try the energy optimal trajectory.
        if len(t_tstar) == 1:
            t_star = np.zeros(1)
            u_star = np.zeros(np.shape(self._umax)),
            x_star = [x0]
            goal_handle.abort()

        else:
            # Find energy optimal trajectory
            t_star, u_star, x_star = opti.energy_optimal_trajectory(x0, xf,
                                                                    self._xmin,
                                                                    self._xmax,
                                                                    self._umin,
                                                                    self._umax,
                                                                    N,
                                                                    self._alpha
                                                                    * t_tstar[-1])
            response.duration = (time.time() - start)
            # Logging
            goal_handle.succeed()

        # Save the trajectories as txt files
        np.savetxt("src/reacsa_control/trajectories/x_star.txt", x_star)
        np.savetxt("src/reacsa_control/trajectories/u_star.txt", u_star)
        np.savetxt("src/reacsa_control/trajectories/t_star.txt", t_star)

        # Only plot if so specified
        if self._plotting:
            # Plot the trajectories
            fig1, axs1 = plt.subplots(2)
            axs1[0].plot(t_star, x_star[:, 0])
            axs1[0].plot(t_star, x_star[:, 1])
            axs1[0].grid()
            axs1[0].legend(['x', 'y'])
            axs1[1].plot(t_star, x_star[:, 2])
            axs1[1].legend([r'$\theta$'])
            axs1[1].grid()

            fig2, axs2 = plt.subplots(2)
            axs2[0].plot(t_star, u_star[:, 0])
            axs2[0].grid()
            axs2[0].legend(['Torque'])
            axs2[1].plot(t_star, u_star[:, 1:])
            axs2[1].grid()
            axs2[1].legend([f'force {i}' for i in range(8)])
            plt.show()

        # Logging
        self.get_logger().info("... done!")
        # Finally return the response
        return response


def main(args=None):
    if not rclpy.ok():
        rclpy.init(args=args)

    traj_plan = TrajectoryPlanner()
    rclpy.spin(traj_plan)


if __name__ == '__main__':
    main()
