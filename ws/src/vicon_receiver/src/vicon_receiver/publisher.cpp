#include "vicon_receiver/publisher.hpp"

Publisher::Publisher(std::string topic_name, rclcpp::Node* node)
{
    this->pose_publisher_ = node->create_publisher<geometry_msgs::msg::PoseWithCovarianceStamped>(topic_name,
                                                             /* QoS */ rclcpp::SensorDataQoS());
    this->is_ready = true;
}

void Publisher::publish(PoseStruct p)
{
    auto msg = std::make_shared<geometry_msgs::msg::PoseWithCovarianceStamped>();
    // Init header of msg
    msg->header.stamp = rclcpp::Clock().now();
    msg->header.frame_id = "vicon";

    // Init msg
    msg->pose.covariance = {0,0,0,0,0,0,
                            0,0,0,0,0,0,
                            0,0,0,0,0,0,
                            0,0,0,0,0,0,
                            0,0,0,0,0,0,
                            0,0,0,0,0,0};

    // Position is in mm we want it in meters hence divide by 1000
    double scale = 0.001;
    msg->pose.pose.position.x = p.translation[0] * scale;
    msg->pose.pose.position.y = p.translation[1] * scale;
    msg->pose.pose.position.z = p.translation[2] * scale;
    msg->pose.pose.orientation.x = p.rotation[0];
    msg->pose.pose.orientation.y = p.rotation[1];
    msg->pose.pose.orientation.z = p.rotation[2];
    msg->pose.pose.orientation.w = p.rotation[3];

    /* Enforce positivity of rotational axis. Enforces the sense of 
     * rotation to be consitent  */
    if(msg->pose.pose.orientation.z < 0)
    {
      msg->pose.pose.orientation.x *= -1;
      msg->pose.pose.orientation.y *= -1;
      msg->pose.pose.orientation.z *= -1;
      msg->pose.pose.orientation.w *= -1;
    }


    /* Publish Message */
    pose_publisher_->publish(*msg);
}
