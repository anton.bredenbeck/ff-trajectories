function K = non_linear_traj_tracker(dt,t_star,x_star,u_star, dyn)
%LQR_CONTROLLER returns the optimal state feedback matrix for a fixed final
%state, i.e. trajectory stabilization
%   Input
%       dt:= timestep size
%       t_star:= optimal trajectory time vector
%       x_star:= optimal trajectory to track
%       u_star:= optimal control values to track
%   Output
%       K:= optimal state feedback matrices with length numel(t_star)
n_states = length(x_star(:,1));
n_ctrl = length(u_star(:,1));

% Cost Function
R = diag(ones(1,n_ctrl));
Q = diag(ones(1,n_states));

% Init time Varying Discretized State Space Model
A = zeros(n_states, n_states, numel(t_star));
B = zeros(n_states, n_ctrl, numel(t_star));

for i=1:numel(t_star)
    x = sym('x', [n_states,1]);
    u = sym('u', [n_ctrl,1]);
    dxdt = dyn(x, u);
    % Continuous State Space Model
    Ak_c = double(subs(jacobian(dxdt, x), [x;u], [x_star(:,i); u_star(:,i)]));
    Bk_c = double(subs(jacobian(dxdt, u), [x;u], [x_star(:,i); u_star(:,i)]));
    
    %A(:,:,i) = eye(8) + dt*Ak_c;
    %B(:,:,i) = dt*Bk_c;
    A(:,:,i) = Ak_c;
    B(:,:,i) = Bk_c;
end

% Design a LQR stabilizing the trajectory
K = zeros(n_ctrl, n_states, numel(t_star));

% Iterate the Difference Riccati Equation to find S and K
for i = 1:(numel(t_star))   
    K(:,:,i) = lqr(A(:,:,i),B(:,:,i),Q,R);
end

end