function [u, t] = connect_states(x1,x2,t1,t2,n_ctrl,dyn,H,h)
%CONNECT_STATES Finds the binary and continuous control values needed to
%connect state x1 to x2 within dt time.
% For this the dynamics are linearized and the problem solved as a LP
% that minimizes binary actuation time and takes the actuator limits into
% consideration.
%   Input:
%          x1     := initial state
%          x2     := state to connect to
%          n_ctrl := num of control variables
%          dt     := time difference between the states
%          dyn    := dynamics of the system
%          H      := knot points (including start and not including end)
%          h      := sub-"knot"-points to discretize between the knot
%                    points
%   Output:
%          t_act := vector of actuation time of binary actuators
%          u     := control value of continues actuators
% Function returning the optimal control value based on the 
% currently active controller, i.e. either trajectory tracker
% or stabilizing LQR
assert(length(x1) == length(x2));
n_states = length(x1);
% Find delta t between the two states
dt_long = t2-t1;
% Setup System Matrix Solver
x = sym('x', [n_states,1]);
u = sym('u', [n_ctrl,1]);
dxdt = dyn(x, u);

% Continuous State Space Model
A_c = double(subs(jacobian(dxdt, x), [x;u], [x1; zeros(n_ctrl,1)]));
B_c = double(subs(jacobian(dxdt, u), [x;u], [x1; zeros(n_ctrl,1)]));
dt = dt_long/H;
sys_d = c2d(ss(A_c,B_c,eye(n_states),0), dt);
A = sys_d.A;
B = sys_d.B;

% Linear cost function as J = f^T * x_opt
f = repmat([1;100*ones(n_ctrl - 1,1)],[H,1]);
% Never fire counteracting thrusters together TODO
Aineq = []; bineq = []; 
% Equality Constraint: Enforce state in the end is equal to x2
Aeq = zeros(n_states,H*n_ctrl);
for i=1:H    
    Aeq(:,(i-1)*n_ctrl + 1 : i*n_ctrl) = A^(H-i)*B;
end
beq = x2 - A^H*x1;
% Lower and upper boundaries for the optimization variables
lb = repmat([-14; zeros(n_ctrl-1,1)], [H,1]);
ub = repmat([ 14; ones(n_ctrl-1,1)], [H,1]);
% Solve the linear program
x = linprog(f,Aineq,bineq,Aeq,beq,lb,ub);
% Extract solution
u_t = reshape(x, [n_ctrl,H])';
t = linspace(0,dt_long,h*H);
u = zeros(length(t),n_ctrl);
for i = 1:H   
 indexing = (t >= (i-1)*dt & t < i*dt)' & ... % Only the correct timestamps
     (repmat((1:h)',[H,1]) .* ones(h*H,n_ctrl)) <= floor(u_t(i,:) * h); % Only at the active sub-timestamps
 u(indexing) = 1; 
 u(t >= (i-1)*dt & t < i*dt,1) = u_t(i,1);
end

% Move the time vector to the current time
% Before it was [0,...,dt_long] now it is [t1,...,t2] 
t = t + t1;
end

