function y = thruster_output_model(X)
%THRUSTER_OUTPUT_MODEL Dynamic model of the thrusters
% State vector    x = [P_s, Source Tank Pressure
%                        P_r, Regulated Pressure
%                        A_r, Regulated Valve opening
%                        A_v  Thruster Valve opening]
% Output y = F = g(X)

%% Define Parmeters
ve = 20; % Exhaust velocity
T = 293; % Room Temp in Kelvins
M = 28.9647e-3; % Molar Mass Breathing Air
R = 8.31446261815324/M; % Specific Gas Constant
Cd = 1; % Coefficient of discharge
gamma = 1.40; % Ratio of Specific Heat

P_a = 1.01325e5; % Ambient pressure at sea level 1.01 Bar = 1.01e5 Pa

%% Read out state variables
P_s = X(:,1);
P_r = X(:,2);
A_r = X(:,3);
A_v = X(:,4:end);

%% Compute Force

% Mass flow from the regulated area out of the thruster
mass_flow = Cd.*A_v.*P_r.*...
    sqrt(2.*gamma/(R.*T.*(gamma-1)).*((P_a./P_r).^(2./gamma) - (P_a./P_r).^((gamma+1)./gamma)));

F = mass_flow.*ve  + (P_r - P_a) .* A_v;

%% Store output
y = F;
end

