recompute_opti_traj = true;
recompute_traj_foll = true;

alpha = 15;
x0 = [0,0,0,0,0,0,0]';
xf = [1,2,pi,0,0,0,0]';
Tf = 100; % s
f_ctrl = 10; % Hz

c = load_constants();
dyn = @(x,u) reacsa(x,u,c);

disp("Compute optimal trajectory.... ")
if recompute_opti_traj
    [u_star,t_star,x_star,dt] = non_linear_traj_finder(x0, xf, alpha, dyn);
    save('u_star.mat', 'u_star');
    save('t_star.mat', 't_star');
    save('x_star.mat', 'x_star');
    save('dt.mat', 'dt');
else
    load('u_star.mat');
    load('t_star.mat');
    load('x_star.mat');
    load('dt.mat');
end
disp("...done")

disp("Compute optimal feedback for trajectory tracking...")
if recompute_traj_foll
    optimal_K = non_linear_traj_tracker(dt,t_star,x_star,u_star, dyn);
    save('optimal_K.mat','optimal_K');
else
    load('optimal_K.mat');
end
disp("... done")

% Compute the feedback matrix to 
% stabilize about the final state
K_final = lqr_ffs(xf,dyn);

% Init the SigDelta modulate and control handle
modu = SigmaDeltaModulator(8);
bin_control = @(t,x) modu.ubin(t,f_ctrl,...
    feedback_control(t,x,u_star,t_star,x_star,optimal_K,K_final,xf));

% Simulate the dynamics of the model
disp("Simulating...")
[t, y] = ode45(@(t, x) dyn(x, bin_control(t, x)),...
               0:dt_ctrl/100:Tf, ...
               x0);
disp("...done")
modu.reset(8);
modu.integrator_value;

% Finding the applied control 
u = zeros(9,numel(t));
for i=1:numel(t)
    u(:,i) = bin_control(t(i),y(i,:)');
end

% Plotting
plot_opti_vs_true(u_star, t_star,x_star, u,t',y')