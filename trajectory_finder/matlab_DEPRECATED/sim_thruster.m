% Control Policy Function
uf = @(t,x) (t > 1) .* [1;0;0;0;0;0;0;0];
           
% 300 Bar = 300e5 Pa; 10 Bar = 2e5 Pa
x0 = [300e5, 8e5, 0.01,zeros(1,8)];
Tf = 4;

% Simulate the dynamics of the model
disp("Simulating...")
[t, x] = ode45(@(t, x) ...
    thruster_state_model(x, uf(t, x)),...
    [0 Tf], x0);
disp("...done")

fig1 = figure;
subplot(2,1,1)
plot(t,x(:, 1)/1e5)
legend("P_s")
xlabel("Time [s]")
ylabel("Pressure [Bar]")
grid on;
subplot(2,1,2)
plot(t,x(:, 2)/1e5)
legend("P_r")
xlabel("Time [s]")
ylabel("Pressure [Bar]")
grid on;

fig2 = figure;
subplot(2,1,1)
plot(t,x(:, 3))
legend("A_r")
xlabel("Time [s]")
ylabel("Opening Area [m^2]")
grid on;
subplot(2,1,2)
hold all;
plot(t,x(:, 4),'--')
plot(t,x(:, 5),'--')
plot(t,x(:, 6),'--')
plot(t,x(:, 7),'--')
plot(t,x(:, 8),'--')
plot(t,x(:, 9),'--')
plot(t,x(:, 10),'--')
plot(t,x(:, 11),'--')
legend("A_0","A_1","A_2","A_3","A_4","A_5","A_6","A_7")
xlabel("Time [s]")
ylabel("Opening Area [m^2]")
grid on;

fig3 = figure;
y = thruster_output_model(x);
hold all;
plot(t, y(:,1), '--');
plot(t, y(:,2), '--');
plot(t, y(:,3), '--');
plot(t, y(:,4), '--');
plot(t, y(:,5), '--');
plot(t, y(:,6), '--');
plot(t, y(:,7), '--');
plot(t, y(:,8), '--');
grid on;
legend("F_0", "F_1","F_2","F_3","F_4","F_5","F_6","F_7");
xlabel("Time [s]")
ylabel("Force [N]")