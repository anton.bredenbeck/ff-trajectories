function control = feedback_control(t,x,... % State and Time
                                    u_star,t_star,x_star,optimal_K,... % Trajectory Tracker Params
                                    K_final, xf) % Final stabilizer Params
%FEEDBACK_CONTROL Function returning the optimal control value based on the 
% currently active controller, i.e. either trajectory tracker or
% stabilizing LQR. In any case the control is mapped to the discrete
% thruster space by solving the LP described in the function "connect_states"
    indeces = find(t_star <= t);
    index = indeces(end);
    if numel(indeces) < numel(t_star)
        control = u_star(:,index) - optimal_K(:,:,index)*(x-x_star(:,index));
        %control(abs(u_star(:,index)) < 0.001) = 0; 
    else
        control = - K_final*(x-xf);
    end                    
    
    % Saturate torque control if it exceeds capabilities
    control(1) = (abs(control(1)) > 4.8) * sign(control(1))*4.8 ...
                + (abs(control(1)) < 4.8) * control(1);
    control(2:end) = (control(2:end) > 1) .* 1 ...
        + (control(2:end) < 1 & control(2:end) > 0) .* control(2:end);     
end