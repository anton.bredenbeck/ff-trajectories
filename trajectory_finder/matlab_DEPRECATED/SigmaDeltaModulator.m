classdef SigmaDeltaModulator < handle
   properties
      k % Error gain
      t_last % The last stored time value
      integrator_value % Current value of the integrator
      curr_output % Current output value
   end
   methods
      function obj = SigmaDeltaModulator(N)
        obj.k = 100*ones(N,1);
        obj.t_last = zeros(N,1);
        obj.integrator_value = zeros(N,1);
        obj.curr_output = zeros(N,1);
      end
      
      function control = ubin(obj,t,f_ctrl,uf)
         delta_t = t - obj.t_last;
         if delta_t >= 1/f_ctrl
             obj.t_last = t;
             obj.integrator_value = obj.integrator_value + ...
                 delta_t.*obj.k.*(uf(2:end)-obj.curr_output);
             obj.curr_output = (obj.integrator_value > 0.5).*1;
             obj.integrator_value(obj.curr_output == 1) = 0;
         end

         control = [uf(1); obj.curr_output];
      end
      function reset(obj,N)
         obj.k = 10*ones(N,1);
         obj.t_last = zeros(N,1);
         obj.integrator_value = zeros(N,1);
         obj.curr_output = zeros(N,1);
      end
   end
end