function plotting_wrapper(u_star,t_star,x_star)
%PLOTTING_WRAPPER Summary of this function goes here
%   Detailed explanation goes here

%% Plotting

% Ground Track
fig1 = figure;
min_val = min([x_star(1,:),min(x_star(2,:))]);
max_val = max([x_star(1,:),max(x_star(2,:))]);
title("Ground Track")
plot(x_star(1,:),x_star(2,:));
xlim([min_val - 0.05*max_val, max_val + 0.05*max_val]);
ylim([min_val - 0.05*max_val, max_val + 0.05*max_val]);
xlabel('x[m]');
ylabel('y[m]');
grid on;

% Pose
fig2 = figure;
title("Pose over time")
subplot(2,1,1)
plot(t_star, x_star(1,:), t_star,x_star(2,:));
legend('x', 'y');
xlabel('time [s]');
ylabel('coordinate [m]');
grid on;

subplot(2,1,2)
plot(t_star, x_star(3,:));
xlabel('time [s]');
ylabel('$\theta$ [rad]', 'Interpreter','latex');
grid on;

% Velocities
fig3 = figure;
title("Velocities over time")
subplot(2,1,1)
plot(t_star, x_star(4,:), t_star,x_star(5,:));
legend('$\dot{x}$', '$\dot{y}$', 'Interpreter', 'latex');
xlabel('time [s]');
ylabel('velocity [m/s]');
grid on;

subplot(2,1,2)
plot(t_star, x_star(6,:));
xlabel('time[s]');
ylabel('$\dot{\theta}$ [rad/s]', 'Interpreter','latex');
grid on;

%Actuator - Stuff
fig4 = figure;
title("Actuators")
subplot(3,1,1)
plot(t_star, u_star(1,:))
xlabel('time [s]');
ylabel('torque [Nm]');
grid on;

subplot(3,1,2)
plot(t_star, 60/(2*pi)*x_star(7,:))
xlabel('time [s]');
ylabel('RW-angular-velocity [RPM]');
grid on;

subplot(3,1,3)
plot(t_star, u_star(2:9,:));
legend('$f1a$','$f1b$','$f2a$','$f2b$','$f3a$','$f3b$','$f4a$','$f4b$',...
       'Interpreter','latex');
xlabel('time [s]');
ylabel('thrust [N]');
grid on;

end

