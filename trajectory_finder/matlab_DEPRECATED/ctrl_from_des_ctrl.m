function u = ctrl_from_des_ctrl(u_des, x, H,dt)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
r = 0.3; %m
Ib = 11.506; % kgm^2
Kt = 0.12; %Nm/A Torque Constant
f = 14; % N

% Control Bounds
umin = [0,0,0,0,0,0,0,0,-14]';
umax = [1,1,1,1,1,1,1,1,14]';
% This will contain the indeces of binary decision variables
intcon = zeros(1,8*H);
% This will contain the mapping from control values 
% to fx, fy and tau (forces and torques)
M_list = cell(H);
for i = 1:H
    % Extract the current state
    theta = x(i,3);
    sT = sin(theta);
    cT = cos(theta);
    % Find the mapping of the forces at the given state
    M = [-sT*f sT*f -cT*f cT*f sT*f -sT*f cT*f -cT*f 0;
         cT*f -cT*f -sT*f sT*f -cT*f cT*f sT*f -sT*f 0;
         f*r/Ib -f*r/Ib f*r/Ib -f*r/Ib f*r/Ib -f*r/Ib f*r/Ib -f*r/Ib Kt/Ib];
    % Concatenate it together
    M_list{i} = M;
    % Fancy math to find the right indeces
    intcon(i:i+7) = 3*H + (i-1)*9 + (1:8);
end

% Stack the large vectors
dt_tilde = dt*ones(1,H);
M_tilde = blkdiag(M_list{:});

% Cost function, we're simply minimizing
% the sum of auxillery variables that are 
% subjected to constraints such that the 
% absolute difference is minimized
f = [ones(3*H,1); zeros(9*H,1)];

% Introduces a constraint that counterfacing 
% thrusters should never be on at the same time
sigma = [1 1 0 0 0 0 0 0 0;
         0 0 1 1 0 0 0 0 0;
         0 0 0 0 1 1 0 0 0;
         0 0 0 0 0 0 1 1 0];

% Matrix and vector describing the liner 
% constraint
A  = [-eye(3*H), -dt_tilde*M_tilde;
      -eye(3*H),  dt_tilde*M_tilde;
      zeros(4,3*H), repmat(sigma, [1,H])];
b = [-dt_tilde * M_tilde * u_des;
      dt_tilde * M_tilde * u_des;
      ones(4*H,1)];
 
% Lower and upper bounds
lb = [zeros(3*H,1); repmat(umin, [H,1])];
up = [inf(3*H,1); repmat(umax,[H,1])];

% Call the solver
x = intlinprog(f,intcon, A, b, [],[], lb, up);

% Extract the solution
u = x(3*H+1:end);

end

