function [dX] = reacsa(X,u,c)
%REACSA Non-linear but idealized model of the reacsa system on flat ground
%   State vector    x = [x,y,theta, x_dot, y_dot, theta_dot, RW_vel]
%   Control inputs  u = [tau, F_1a, F_1b,
%                             F_2a, F_2b,
%                             F_3a, F_3b,
%                             F_4a, F_4b]
%                   c = Parameters that describe the system
%   The non-linear model is then described by
%     x_dot = f(x,u)


% Extract necessary states and control actions
tau = u(1);
u1a = c.f*u(2);
u1b = c.f*u(3);
u2a = c.f*u(4);
u2b = c.f*u(5);
u3a = c.f*u(6);
u3b = c.f*u(7);
u4a = c.f*u(8);
u4b = c.f*u(9);
theta = X(3);

st = sin(theta);
ct = cos(theta);
% Compute the second derivatives
x_ddot = (1/c.m)*(-u1a*st + u1b*st - u2a*ct + u2b*ct ...
              +u3a*st - u3b*st + u4a*ct - u4b*ct);
y_ddot = (1/c.m)*( u1a*ct - u1b*ct - u2a*st + u2b*st ...
              -u3a*ct + u3b*ct + u4a*st - u4b*st);
theta_body_ddot = (1/c.Ib)*(-tau + c.r*(u1a - u1b + ...
                                        u2a - u2b + ...
                                        u3a - u3b + ...
                                        u4a - u4b));

% Compute the reaction wheel derivatives
d_rw = 1/c.Iw * tau;

% Finally we stack the state derivative vector
dX = [X(4);X(5);X(6);x_ddot; y_ddot; theta_body_ddot; d_rw];
end

