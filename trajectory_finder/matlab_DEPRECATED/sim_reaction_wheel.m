x = sym('x', [2,1]);
u = sym('u', 1);
dxdt = reaction_wheel_state_model(x, u);

% Continuous State Space Model
A = double(jacobian(dxdt, x));
B = double(jacobian(dxdt, u));

% Cost Function
R = 0.01;
Q = diag([1e10,1e-10]);

% Compute Feedback Matrix
K = dlqr(A,B,Q,R)

% Compute prefilter to guarantee reference following
V = 1/dcgain(-feedback(K*ss(A,B,eye(2),0),-1));

% Control Policy Function
xref = [1;0]; 
uf = @(t,x) -K*(V*xref - x);

           
x0 = [5, 0];
Tf = 20;

% Simulate the dynamics of the model
disp("Simulating...")
[t, y] = ode45(@(t, x) ...
    reaction_wheel_state_model(x, uf(t, x)),...
    [0 Tf], x0);
disp("...done")

u = zeros(1,numel(t));
for i=1:numel(t)
    u(i) = uf(t(i),y(i,:)');
end


fig1 = figure;
subplot(3,1,1)
plot(t,y(:, 1))
xlabel("Time [s]")
ylabel("Angular Velocity [rad/s]")
grid on;
subplot(3,1,2)
plot(t,y(:,2))
xlabel("Time [s]")
ylabel("Current [A]")
grid on;
subplot(3,1,3)
plot(t,u)
xlabel("Time [s]")
ylabel("Input Voltage [V]")
grid on;
