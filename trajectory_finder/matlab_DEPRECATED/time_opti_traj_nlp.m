function [u_star,t_star,x_star,dt] = time_opti_traj_nlp(x0,xf,umin,umax,...
                                                        xmin, xmax,N,dyn)
%TIME_OPTI_TRAJ returns the time optimal control procedure for the 
% REACSA
%   Returns an array containing the optimal control sequence, the
%   corresponding time vector, state trajectory and the timestep size.
%   Given the state and ctrl limits for the specified dynamics. 
%
%   Input:
%          x0   := initial state
%          xf   := final state
%          umin := lower bound of controls
%          umax := upper bound of controls
%          xmin := lower bound of states
%          xmax := upper bound of states
%          N    := number of samples
%          dyn  := function handle for the dynamics
%   Output:
%          u_star:= optimal control policy
%          t_star:= time vector for optimal trajectory
%          x_star:= optimal trajectory
%          dt := time step between each point in trajectory/control policy
import casadi.*

% Make sure initial and final state refer to 
% the same state space and control limits also
% refer to the same limits
assert(length(x0) == length(xf))
assert(length(xmax) == length(xmin));
assert(length(x0) == length(xmin));
assert(length(umin) == length(umax));
n_states = length(x0);
n_ctrl = length(umin);

% Initialize the optimization problem
opti = casadi.Opti();

% define decision variables
X = opti.variable(n_states,N+1); % state trajectory
U = opti.variable(n_ctrl,N+1);
Tf = opti.variable();

% Objective function
% minimize time
opti.minimize(Tf);

% Dynamic constraints
f = @(x,u) dyn(x, u); % dx/dt = f(x,u)
dt = Tf/N; % length of a control interval

% Implement the dynamic constraints by collocation method
% State and derivative estimation at collocation poinc at  t_c = (t_k+t_(k+1))/2
xc = @(x,u,k) 1/2.*(x(:,k)+x(:,k+1)) + dt/8.*(f(x(:,k),u(:,k))-f(x(:,k+1),u(:,k+1)));
uc = @(u,k) (u(:,k)+u(:,k+1))/2;

% Collocation Constraint
for k=1:N %loop over control intervals
    opti.subject_to(...
        (X(:,k)-X(:,k+1)) + ...
        dt/6*(  f(X(:,k),U(:,k)) +...
                4*f(xc(X,U,k),uc(U,k)) +...
                f(X(:,k+1),U(:,k+1))) ...
                == 0);
end

% Final time needs to be non negative
opti.subject_to(0 <= Tf);

% Control Constraints
for i = 1:n_ctrl
    opti.subject_to(umin(i) <= U(i,:));
    opti.subject_to(umax(i) >= U(i,:));
end

% State Constraints
for i = 1:n_states
    opti.subject_to(xmin(i) <= X(i,:));
    opti.subject_to(xmax(i) >= X(i,:));
end

% Implement the initial and final-time constraints
opti.subject_to(X(:,1) == x0);
opti.subject_to(X(:,end) == xf);

% Initialize decision variables
opti.set_initial(U, zeros(n_ctrl,N+1));
% Linear interpolation between start and end state as 
% inital values for the optimization
opti.set_initial(X, x0 + linspace(0,1,N+1).*(xf-x0));
opti.set_initial(Tf,3);

% Solve NLP
p_opts = struct();
s_opts = struct('max_iter',6e3);
opti.solver('ipopt',p_opts,s_opts); % set numerical backend
sol = opti.solve();   % actual solve

%Extract the states and control from the decision variables
Tf_star = sol.value(Tf);
dt = Tf_star/N;
x_star = sol.value(X);
u_star = sol.value(U);
t_star = (0:N)*dt;

end

