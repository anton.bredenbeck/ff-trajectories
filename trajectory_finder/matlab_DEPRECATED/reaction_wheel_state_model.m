function dX = reaction_wheel_state_model(X,u,c)
%REACTION_WHEEL_STATE_MODEL dynamic model of the reaction wheel
%   State vector    x = [theta_dot, Angular Velocity of RW
%                        i, Motor Current
%                       ]
%   Control inputs  u = [v, voltage set]
%                   c = Params that describe the system
%
%   The inear model is then described by
%     x_dot = f(x,u)

%% Define state matrices
rw = X(1);
I = X(2);

A = [-c.b/c.Iw, c.Kt/c.Iw;
     -c.Ke/c.L, -c.R/c.L];
B = [0;1/c.L];

%% Compute State Derivative
dX = A*[rw;I] + B*u;
end

