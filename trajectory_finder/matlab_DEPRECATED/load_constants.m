function c = load_constants()% Define System constants
    c = struct;
    c.m = 221.670; %kg
    c.r = 0.3; %m
    c.Ib = 11.506; % kgm^2
    c.Kt = 0.12; %Nm/A Torque Constant
    c.f = 14; % Nominal force of the thrusters 
    c.Iw = 0.047; %kgm^2 Reaction Wheel Moment of Inertia
    c.b = 0.01; %kgm / s rad Damping coefficient  TBS
    c.L = 0.6e-3 ;%H Inductivity of the motor
    c.R = 0.1; %Ohm Overall Resistance
    c.Ke = c.Kt; %0.001; %Vs/rad Constant that relates angular velocity to back electromotive force TBS
end