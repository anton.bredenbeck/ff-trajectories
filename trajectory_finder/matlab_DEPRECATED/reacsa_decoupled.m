function dX = reacsa_decoupled(X, u, c)
%REACSA_DECOUPLED non-linear but decoupled model of the reacsa system on flat 
%                 ground. In this model the thrusters and the reaction
%                 wheel are decoupled, i.e. thrusters only produce force
%                 (we always fire a pair of thrusters) and the RW is the
%                 sole provider of torque.        
%   State vector    x = [x,y,theta, x_dot, y_dot, theta_dot, RW_vel]
%   Control inputs  u = [Fx, Fy, Tau] aka Force in x & y (local robot 
%                                         coordinate system) and torque
%                   c = Params that describe the system
%   The linear model is then described by
%     x_dot = f(x,u)


A = [zeros(3), eye(3), zeros(3,1);
     zeros(4,6), [0;0; -b/Iw;0]];

theta = x(3);
sT = sin(theta);
cT = cos(theta);
 
B = [zeros(3); 
     c.f/c.m*cT, c.f/c.m*sT, 0;
    -c.f/c.m*sT, c.f/c.m*cT,0;
    0,0,-1/c.Ib;
    0,0,1/c.Iw];

dX = A*X + B*u;
end

