function [u_star,t_star,x_star,dt] = non_linear_traj_finder(x0,xf,alpha,dyn)
%NON_LINEAR_TRAJ_FINDER returns the energy optimal control procedure for the time that is 
%                       alpha*t_star between two states for REACSA
%   Returns an array containing the optimal control sequence, the
%   corresponding time vector, state trajectory and the timestep size.
%
%   State Vector    x = [x,y,theta_body,x_dot,y_dot,theta_body_dot, theta_wheel_dot]
%   Control inputs  u = [tau ,F_1a,F_1b,F_2a,F_2b,F_3a,F_3b,F_4a, F_4b]
%
%   Input:
%          x0    := initial state
%          xf    := final state
%          alpha := multiplication factor with optimal time
%          dyn   := dynamics of the system
%   Output:
%          u_star:= optimal control policy
%          t_star:= time vector for optimal trajectory
%          x_star:= optimal trajectory
%          dt := time step between each point in trajectory/control policy

% Define the number of knot points
N = 100;

% Define the control limits using a limit
% Double Check the limits, in particular the
% thruster limits are guesstimated
u_tolerance = 0;
umin = - [4.8;zeros(8,1)] + u_tolerance;
umax =   [4.8; ones(8,1)*7.5] - u_tolerance;

% Define the saturation limits 
xmin = - [inf(6,1); 500*2*pi/60];
xmax =   [inf(6,1); 500*2*pi/60];

% Finding the time optimal trajectory
[~,t_tstar,~,~] = time_opti_traj_nlp(x0,xf, umin, umax, ...
                                     xmin, xmax, N/10, dyn);
% Finding the desired final time for the energy optimal trajectory
tf = alpha*t_tstar(end);

% Use the time optimial solution as initial guess for the energy 
% optimal solution. 
R = [0,1,1,1,1,1,1,1,1];
[u_star,t_star,x_star,dt] = energy_opti_traj(x0,xf,tf,umin,umax,...
                                             xmin, xmax, N, dyn, ...
                                             R);

writematrix(t_star, 't_star.txt','Delimiter','space');
writematrix(u_star', 'u_star.txt','Delimiter','space');
writematrix(x_star', 'x_star.txt','Delimiter','space');
end
