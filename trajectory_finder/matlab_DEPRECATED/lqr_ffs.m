function K = lqr_ffs(xf, dyn)
%LQR_FFS returns lqr feedback gains for a free final state on a
%        finite control horizon
%   Input
%      xf:= final state
%   Output
%      K:= optimal state feedback matrix

% Setup System Matrix Solver
x = sym('x', [7,1]);
u = sym('u', [9,1]);
dxdt = dyn(x, u);

% Continuous State Space Model
A = double(subs(jacobian(dxdt, x), [x;u], [xf; zeros(9,1)]));
B = double(subs(jacobian(dxdt, u), [x;u], [xf; zeros(9,1)]));

% Cost Function
R = diag([1,1,1,1,1,1,1,1,1]);
Q = diag([1,1,1,1,1,1,1]);

% Compute Feedback Matrix
K = lqr(A,B,Q,R, []);
end