function dX = thruster_state_model(X,u)
%THRUSTER_MODEL Dynamic model of the thrusters
%   State vector    x = [P_s, Source Tank Pressure
%                        P_r, Regulated Pressure
%                        A_r, Regulated Valve opening
%                        A_v1,  Thruster i Valve opening
%                        A_v2,
%                        A_v3,
%                        A_v4,
%                        A_v5,
%                        A_v6,
%                        A_v7,
%                        A_v8]
%   Control inputs  u = [dot_A_v1, trigger to open or close the thruster valve i
%                        dot_A_v2,
%                        dot_A_v3,
%                        dot_A_v4,
%                        dot_A_v5,
%                        dot_A_v6,
%                        dot_A_v7,
%                        dot_A_v8]
%
%   The non-linear model is then described by
%     x_dot = f(x,u)

%% Define Parmeters
k = 0.5; % Spring Constant
b = 0.005; % Friction Coefficient
Ad = 0.1; % Diaphragm Area
cr = 0.1; % Linear relationship between regulatory valve area and plug position
cv = 0.1; % Output valve opening and closing rate
vs = 13.6; % Source tank volume
vr = 0.1; % Regulated tank volume
T = 293; % Room Temp in Kelvin
M = 28.9647e-3; % Molar Mass Breathing Air
R = 8.31446261815324/M; % Specific Gas Constant
Cd = 1; % Coefficient of discharge
gamma = 1.40; % Ratio of Specific Heat

P_ref = 10e5; % Reference Pressure 10 Bar = 10e5 Pa
P_a = 1.01325e5; % Ambient pressure at sea level 1.01 Bar = 1.01e5 Pa
A_r_max = 0.1; % Maximal opening regulator
A_v_max = 0.001; % Maximal opening output valve

% Derived auxillery Constants
tau_r = b/k;
k_r = Ad*cr/k;

%% Read out state variables
P_s = X(1);
P_r = X(2);
A_r = X(3);
A_v = X(4:end);

%% Assertions,
%  Input is only tertiary
%  either the trigger to open the valve or the trigger to close the valve
%  or to leave it in the current state.
assert(all(u == 1 | u == -1 | u == 0));
% Source pressure must be higher than operating pressure
assert(P_s > P_r, "P_s = %f !> P_r = %f", P_s, P_r)
% Operating Pressure must be higher than Ambient Pressure
assert(P_r > P_a, "P_r = %f !> P_a", P_r)

%% Compute State Derivative

% Mass flow from the source tank to the regulated area
mass_flow1 = Cd*A_r*P_s*...
    sqrt(2*gamma/(R*T*(gamma-1))*((P_r/P_s)^(2/gamma) - (P_r/P_s)^((gamma+1)/gamma)));
% Mass flow from the regulated area out of the thruster
mass_flow2 = Cd*sum(A_v)*P_r*...
    sqrt(2*gamma/(R*T*(gamma-1))*((P_a/P_r)^(2/gamma) - (P_a/P_r)^((gamma+1)/gamma)));

P_s_dot = -mass_flow1 * R*T/(vs*M);
P_r_dot = (mass_flow1 - mass_flow2) * R * T/(vr * M);
% TODO saturate the valve openings
A_r_dot = (-A_r - k_r*(P_r - P_ref))/tau_r;
A_v_dot = u.*cv;

% Stack the output vector
dX = [P_s_dot; P_r_dot; A_r_dot; A_v_dot];
end

