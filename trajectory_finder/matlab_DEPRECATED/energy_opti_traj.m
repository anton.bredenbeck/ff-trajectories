function [u_star,t_star,x_star,dt] = energy_opti_traj(x0,xf,tf,umin,umax,...
                                                      xmin, xmax,N,dyn,...
                                                      R)
%ENERGY_OPTI_TRAJ returns the energy optimal control procedure between
%                 two states given some duration tf for REACSA
%   Returns an array containing the optimal control sequence, the
%   corresponding time vector, state trajectory and the timestep size.
%
%   Input:
%          x0   := initial state
%          xf   := final state
%          tf   := final time
%          umin := lower bound of controls
%          xmax := upper bound of controls
%          xmin := lower bound of states
%          xmax := upper bound of states
%          N    := number of samples
%          dyn  := function handle for the dynamics
%          R    := weighting matrix for optimization
%   Output:
%          u_star:= optimal control policy
%          t_star:= time vector for optimal trajectory
%          x_star:= optimal trajectory
%          dt := time step between each point in trajectory/control policy

import casadi.*

% Make sure initial and final state refer to 
% the same state space and control limits also
% refer to the same limits
assert(length(x0) == length(xf))
assert(length(xmax) == length(xmin));
assert(length(x0) == length(xmin));
assert(length(umin) == length(umax));
n_states = length(x0);
n_ctrl = length(umin);

% Initialize the optimization problem
opti = casadi.Opti();

% define decision variables
X = opti.variable(n_states,N+1); % state trajectory
U = opti.variable(n_ctrl,N+1); % control trajectory
Tf = tf;

% Objective function
% minimize control effort according to linear quadradic cost function
% we penalize the thruster actuation in particular because that 
% depletes the tank, the bottelneck for testing 
sum = 0;
for i = 1:n_ctrl
    sum = sum + U(i,:) * R(i) * U(i,:)';
end
opti.minimize(sum);

% Dynamic constraints
f = @(x,u) dyn(x, u); % dx/dt = f(x,u)
dt = Tf/N; % length of a control interval

% Implement the dynamic constraints by collocation method
% State and derivative estimation at collocation poinc at  t_c = (t_k+t_(k+1))/2
xc = @(x,u,k) 1/2.*(x(:,k)+x(:,k+1)) + dt/8.*(f(x(:,k),u(:,k))-f(x(:,k+1),u(:,k+1)));
uc = @(u,k) (u(:,k)+u(:,k+1))/2;

% Collocation Constraint
for k=1:N %loop over control intervals
    opti.subject_to(...
        (X(:,k)-X(:,k+1)) + ...
        dt/6*(  f(X(:,k),U(:,k)) +...
                4*f(xc(X,U,k),uc(U,k)) +...
                f(X(:,k+1),U(:,k+1))) ...
                == 0);
end

% Control Constraints
for i = 1:n_ctrl
    opti.subject_to(umin(i) <= U(i,:));
    opti.subject_to(umax(i) >= U(i,:));
end

% State Constraints
for i = 1:n_states
    opti.subject_to(xmin(i) <= X(i,:));
    opti.subject_to(xmax(i) >= X(i,:));
end

% Implement the initial and final-time constraints
opti.subject_to(X(:,1) == x0);
opti.subject_to(X(:,end) == xf);

% Initialize decision variables
opti.set_initial(U, zeros(n_ctrl,N+1));
opti.set_initial(X, x0 + linspace(0,1,N+1).*(xf-x0));

% Solve NLP
opti.solver('ipopt'); % set numerical backend
sol = opti.solve();   % actual solve

%Extract the states and control from the decision variables
dt = Tf/N;
x_star = sol.value(X);
u_star = sol.value(U);
t_star = (0:N)*dt;
end

