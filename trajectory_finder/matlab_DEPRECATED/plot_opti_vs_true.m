function plot_opti_vs_true(u_star,t_star,x_star,...
                           u,t,x)
%PLOT_OPTI_VS_TRUE plot optimal trajectory vs true trajectory

% Ground Track
fig1 = figure;
hold all;
min_val = min([x_star(1,:),x_star(2,:),x(1,:),x(2,:)]);
max_val = max([x_star(1,:),x_star(2,:),x(1,:),x(2,:)]);
title("Ground Track")
plot(x_star(1,:),x_star(2,:),'--');
plot(x(1,:),x(2,:));
xlim([min_val - 0.05*max_val, max_val + 0.05*max_val]);
ylim([min_val - 0.05*max_val, max_val + 0.05*max_val]);
legend('Optimal Trajectory','Followed Trajectory')
xlabel('x[m]');
ylabel('y[m]');
axis equal
grid on;

% Pose
fig2 = figure;
title("Pose over time")
subplot(2,1,1)
hold all;
plot(t, x(1,:), t,x(2,:));
plot(t_star, x_star(1,:),'--', t_star,x_star(2,:),'--');
legend('$x_{followed}$','$y_{followed}$','$x_{opti}$', '$y_{opti}$','Interpreter','latex');
xlabel('time [s]');
ylabel('coordinate [m]')
grid on;

subplot(2,1,2)
hold all;
plot(t, x(3,:));
plot(t_star, x_star(3,:),'--');
xlabel('time [s]');
ylabel('$\theta$ [rad]', 'Interpreter','latex');
legend('$\theta_{followed}$','$\theta_{opti}$','Interpreter','latex');
grid on;

% Velocities
fig3 = figure;
title("Velocities over time")
subplot(2,1,1)
hold all;
plot(t, x(4,:), t,x(5,:));
plot(t_star, x_star(4,:),'--', t_star,x_star(5,:),'--');
legend('$\dot{x}_{followed}$', '$\dot{y}_{followed}$',...
       '$\dot{x}_{opti}$', '$\dot{y}_{opti}$',...
       'Interpreter','latex');
xlabel('time [s]');
ylabel('velocity [m/s]');
grid on;

subplot(2,1,2)
hold all;
plot(t, x(6,:));
plot(t_star, x_star(6,:),'--');
legend('$\dot{\theta}_{followed}$',...
       '$\dot{\theta}_{opti}$',...
...done
       'Interpreter','latex')
xlabel('time[s]');
ylabel('$\dot{\theta}$ [rad/s]', 'Interpreter','latex');
grid on;

%Actuator - Stuff
fig4 = figure;
title("Actuators")
subplot(2,1,1)
hold all;
plot(t, u(1,:))
plot(t_star, u_star(1,:),'--')
legend('$\tau_{followed}$', ... 
       '$\tau_{opti}$',...
       'Interpreter','latex')
xlabel('time [s]');
ylabel('Torque [Nm]');
grid on;

subplot(2,1,2)
hold all;
plot(t, 60/(2*pi)*x(7,:))
plot(t_star, 60/(2*pi)*x_star(7,:),'--')
legend('$\omega_{RW,followed}$', ...
       '$\omega_{RW, opti}$', ...
       'Interpreter','latex')
xlabel('time [s]');
ylabel('RW-angular-velocity [RPM]');
grid on;

fig5 = figure;
title("Thrusters");
hold all;
area(t, u(2,:),'FaceColor','b');
area(t, u(3,:),'FaceColor','g');
area(t, u(4,:),'FaceColor','r');
area(t, u(5,:),'FaceColor','c');
area(t, u(6,:),'FaceColor','m');
area(t, u(7,:),'FaceColor','y');
area(t, u(8,:),'FaceColor','k');
area(t, u(9,:),'FaceColor',[0.33, 0.33, 1]);
plot(t_star, u_star(2,:),'color','b')
plot(t_star, u_star(3,:),'color','g')
plot(t_star, u_star(4,:),'color','r')
plot(t_star, u_star(5,:),'color','c')
plot(t_star, u_star(6,:),'color','m')
plot(t_star, u_star(7,:),'color','y')
plot(t_star, u_star(8,:),'color','k')
plot(t_star, u_star(9,:),'color','#5454ff');
legend('$f_{1a,followed}$', '$f_{1b,followed}$',...
       '$f_{2a,followed}$', '$f_{2b,followed}$',...
       '$f_{3a,followed}$', '$f_{3b,followed}$',...
       '$f_{4a,followed}$', '$f_{4b,followed}$',...
       '$f_{1a,opti}$', '$f_{1b,opti}$',...
       '$f_{2a,opti}$', '$f_{2b,opti}$',...
       '$f_{3a,opti}$', '$f_{3b,opti}$',...
       '$f_{4a,opti}$', '$f_{4b,opti}$',...
       'Interpreter','latex');
xlabel('time [s]');
ylabel('Quotient of Nominal Force');
grid on;

end

