"""Python script that finds the optimal activation trajectory."""
import numpy as np
import time
import matplotlib.pyplot as plt
from trajectory_optimizer import TrajectoryOptimizer
from reacsa import Reacsa

start = time.time()

res = 100  # dpi
size = 16, 12

model = Reacsa()
opti = TrajectoryOptimizer(model)

# Parameters for the optimization
#  Initial and Final state
x0 = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
xf = np.array([1.0, 2.0, np.pi, 0.0, 0.0, 0.0, 0.0])

#  State and Control Limitations
xmin = - np.array([100, 100, 100, 100, 100, 100, 250 * 2 * np.pi / 60])
xmax = np.array([100, 100, 100, 100, 100, 100, 250 * 2 * np.pi / 60])
umin = - np.array([1.44, 0, 0, 0, 0, 0, 0, 0, 0])
umax = np.array([1.44, 1, 1, 1, 1, 1, 1, 1, 1])

#  Number of Knot Points
N = 100

# Find time optimal trajectort
t_tstar, _, _ = opti.time_optimal_trajectory(x0, xf,
                                             xmin, xmax,
                                             umin, umax,
                                             10)


alpha = 12

# Find energy optimal trajectory
t_star, u_star, x_star = opti.energy_optimal_trajectory(x0, xf,
                                                        xmin, xmax,
                                                        umin, umax,
                                                        N, alpha * t_tstar[-1])

print(f"Found trajectory with {N} knot points"
      + f" in {time.time() - start:.3f} seconds.")

# Compute optimal thruster on time
on_times = np.trapz(u_star[:, 1:], x=t_star, axis=0)
print(f"On Times: {on_times}")
print(f"Sum: {np.sum(on_times)}")

# Save the trajectories as txt files
np.savetxt("../x_star.txt", x_star)
np.savetxt("../u_star.txt", u_star)
np.savetxt("../t_star.txt", t_star)


# Increase font size
plt.rcParams.update({'font.size': 30})

# Plot the trajectories
fig, ax = plt.subplots()
ax.grid()
ax.set_xlabel("x [m]")
ax.set_ylabel("y [m]")
fig.set_size_inches(size)
plt.gca().set_aspect('equal', adjustable='box')
plt.tight_layout()
fig.set_size_inches(size)
ax.plot(x_star[:, 0], x_star[:, 1])
plt.savefig("groundtrack.eps", dpi=res)


fig1, axs1 = plt.subplots(2, sharex=True)
axs1[0].grid()
axs1[0].set_ylabel("Coordinate [m]")
axs1[0].set_xlim(0, t_star[-1])
axs1[0].plot(t_star, x_star[:, 0])
axs1[0].plot(t_star, x_star[:, 1])
axs1[0].legend(['x', 'y'])
axs1[1].set_xlabel("Time [s]")
axs1[1].set_xlim(0, t_star[-1])
axs1[1].set_ylabel(r"Orientation [$\degree$]")
axs1[1].grid()
axs1[1].plot(t_star, 180.0 / np.pi * x_star[:, 2])
axs1[1].legend([r'$\theta$'])
fig1.set_size_inches(size)
plt.tight_layout()
plt.savefig("coordinates.eps", dpi=res)

fig2, axs2 = plt.subplots(3, sharex=True)
axs2[0].grid()
axs2[0].set_ylabel(r"$m/s$")
axs2[0].set_xlim(0, t_star[-1])
axs2[0].plot(t_star, x_star[:, 3])
axs2[0].plot(t_star, x_star[:, 4])
axs2[0].legend([r'$\dot{x}$', r'$\dot{y}$'])
axs2[1].set_xlim(0, t_star[-1])
axs2[1].set_ylabel(r"$\degree/s$")
axs2[1].grid()
axs2[1].plot(t_star, 180.0 / np.pi * x_star[:, 5])
axs2[1].legend([r'$\dot{\theta}$'])
axs2[2].set_xlabel("Time [s]")
axs2[2].set_xlim(0, t_star[-1])
axs2[2].set_ylabel("RPM")
axs2[2].grid()
axs2[2].plot(t_star, x_star[:, 6] * 60 / (2 * np.pi))
axs2[2].legend([r'$\omega_{RW}$'])
fig2.set_size_inches(size)
plt.tight_layout()
plt.savefig("velocities.eps", dpi=res)


fig3, axs3 = plt.subplots(2, sharex=True)
axs3[0].grid()
axs3[0].set_ylabel("Torque [Nm]")
axs3[0].set_xlim(0, t_star[-1])
axs3[0].plot(t_star, u_star[:, 0])
axs3[1].set_xlabel("Time [s]")
axs3[1].set_ylabel("Force [N]")
axs3[1].set_xlim(0, t_star[-1])
axs3[1].grid()
axs3[1].plot(t_star, model.nom_f * u_star[:, 1:])
axs3[1].legend([fr'$f_{i}$' for i in range(8)], ncol=4)
fig3.set_size_inches(size)
plt.tight_layout()
plt.savefig("actuation.eps", dpi=res)
