import sys
import numpy as np
import matplotlib.pyplot as plt

from reacsa import Reacsa
from trajectory_optimizer import TrajectoryOptimizer


def main(fraction, radius):
    res = 100  # dpi
    size = 16, 12

    opti = TrajectoryOptimizer(Reacsa())

    N = 40
    r = radius  # m radius
    v = 0.01  # m/s specified linear velocity
    w = v / r  # rad / s resulting angular velocity
    # resulting duration of one segment
    tf = fraction * 2 * np.pi / (N * v) * r

    #  State and Control Limitations
    xmin = - np.array([100, 100, 100, 100, 100, 100, 500 * 2 * np.pi / 60])
    xmax = np.array([100, 100, 100, 100, 100, 100, 500 * 2 * np.pi / 60])
    umin = -  np.array([1.44, 0, 0, 0, 0, 0, 0, 0, 0])
    umax = np.array([1.44, 1, 1, 1, 1, 1, 1, 1, 1])

    x = np.zeros([N + 1, 7])
    for i in range(N + 1):
        angle = i * fraction * 2 * np.pi / N - np.pi * 0.5
        x_pos, y_pos = r * np.cos(angle), r * (np.sin(angle) + 1)
        x_vel, y_vel = - w * r * np.sin(angle), w * r * np.cos(angle)
        x[i, :] = [x_pos, y_pos, angle + np.pi * 0.5,
                   x_vel, y_vel,
                   2 * np.pi / (N * tf),
                   0]  # xmax[-1] / 2]

    # Initial and final state have zero velocities and rw speed
    x[0, 3:-1] = 0
    x[-1, 3:-1] = 0
    # Samples for interpolation between states
    n_int = 10

    x_star = np.zeros([N * (n_int) + 1, 7])
    u_star = np.zeros([N * (n_int) + 1, 9])
    t_star = np.zeros([N * (n_int) + 1, 1])

    for i in range(N):
        t_star_sec, u_star_sec, x_star_sec\
            = opti.energy_optimal_trajectory(x[i, :], x[(i + 1), :],
                                             xmin, xmax,
                                             umin, umax,
                                             n_int, tf)
        t_star[i * n_int: (i + 1) * n_int + 1, 0] = t_star_sec + \
            t_star[i * n_int]
        u_star[i * n_int: (i + 1) * n_int + 1, :] = u_star_sec
        x_star[i * n_int: (i + 1) * n_int + 1, :] = x_star_sec

    # Compute optimal thruster on time
    on_times = np.trapz(u_star[:, 1:], x=t_star, axis=0)
    print(f"On Times: {on_times}")
    print(f"Sum: {np.sum(on_times)}")

    # Save the trajectories as txt files
    np.savetxt("../x_star.txt", x_star)
    np.savetxt("../u_star.txt", u_star)
    np.savetxt("../t_star.txt", t_star)

    # Increase font size
    plt.rcParams.update({'font.size': 30})

    # Plot the trajectories
    fig0, ax = plt.subplots()
    ax.scatter(x[:, 0], x[:, 1])
    ax.plot(x_star[:, 0], x_star[:, 1])
    plt.gca().set_aspect('equal', adjustable='box')
    ax.grid()
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    fig0.set_size_inches(size)
    plt.savefig("groundtrack.eps", bbox_inches="tight", dpi=res)

    fig1, axs1 = plt.subplots(2, sharex=True)
    axs1[0].plot(t_star, x_star[:, 0])
    axs1[0].plot(t_star, x_star[:, 1])
    axs1[0].set_ylabel("Coordinate [m]")
    axs1[0].grid()
    axs1[0].legend(['x', 'y'])
    axs1[1].plot(t_star, 180.0 / np.pi * x_star[:, 2])
    axs1[1].legend([r'$\theta$'])
    axs1[1].set_xlabel("Time [s]")
    axs1[1].set_ylabel(r"Orientation [$\degree$]")
    axs1[1].grid()
    fig1.set_size_inches(size)
    plt.savefig("coordinates.eps", bbox_inches="tight", dpi=res)

    fig2, axs2 = plt.subplots(2, sharex=True)
    axs2[0].plot(t_star, u_star[:, 0])
    axs2[0].grid()
    axs2[0].set_ylabel("Torque [Nm]")
    axs2[1].plot(t_star, u_star[:, 1:])
    axs2[1].grid()
    axs2[1].legend([rf'$f_{i}$' for i in range(8)], ncol=4)
    axs2[1].set_ylabel("Force [N]")
    axs2[1].set_xlabel("Time [s]")
    fig2.set_size_inches(size)
    plt.savefig("actuation.eps", bbox_inches="tight", dpi=res)

    fig3, axs3 = plt.subplots(3, sharex=True)
    axs3[0].grid()
    axs3[0].set_ylabel(r"$m/s$")
    axs3[0].set_xlim(0, t_star[-1])
    axs3[0].plot(t_star, x_star[:, 3])
    axs3[0].plot(t_star, x_star[:, 4])
    axs3[0].legend([r'$\dot{x}$', r'$\dot{y}$'])
    axs3[1].set_xlim(0, t_star[-1])
    axs3[1].set_ylabel(r"$\degree/s$")
    axs3[1].grid()
    axs3[1].plot(t_star, 180.0 / np.pi * x_star[:, 5])
    axs3[1].legend([r'$\dot{\theta}$'])
    axs3[2].set_xlabel("Time [s]")
    axs3[2].set_xlim(0, t_star[-1])
    axs3[2].set_ylabel(r"RPM")
    axs3[2].grid()
    axs3[2].plot(t_star, x_star[:, 6] * 60 / (2 * np.pi))
    axs3[2].legend([r'$\omega_{RW}$'])
    fig3.set_size_inches(size)
    plt.tight_layout()
    plt.savefig("velocities.eps", bbox_inches="tight", dpi=res)


if __name__ == '__main__':
    if len(sys.argv) == 3:
        main(float(sys.argv[1]), float(sys.argv[2]))
    else:
        print("Wrong number of arguments. Usage:\n"
              + "python circular_traj.py <fraction of circle>"
              + " <radius of circle>")
