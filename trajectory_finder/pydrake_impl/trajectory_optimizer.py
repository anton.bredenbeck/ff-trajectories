import numpy as np
from typing import Tuple
from pydrake.all import (MathematicalProgram, eq)
from pydrake.solvers.ipopt import IpoptSolver
from pydrake.solvers.snopt import SnoptSolver


class TrajectoryOptimizer(object):

    def __init__(self, model):
        self.model = model

    def normalize_angle(self, angle):
        return np.arctan2(np.sin(angle), np.cos(angle))

    def energy_optimal_trajectory(self, x0, xf,
                                  xmin, xmax, umin,
                                  umax, N, Tf) -> Tuple[np.array,
                                                        np.array,
                                                        np.array]:
        """Function that finds the energy optimal trajectory."""
        # State and control space
        n_states = len(x0)
        n_ctrl = len(umin)
        # Weighting factors for the control effort penalty
        R = [1e-5, 10, 10, 10, 10, 10, 10, 10, 10]
        # Init the NLP handler
        prog = MathematicalProgram()

        # Init decision variables
        X = prog.NewContinuousVariables(N + 1, n_states, "X")
        U = prog.NewContinuousVariables(N + 1, n_ctrl, "U")

        # The cost we want to minimize: Weighted actuation
        c = prog.AddQuadraticCost(
            sum([np.matmul(R[i] * U[:, i], U[:, i].T) for i in range(n_ctrl)]))
        c.evaluator().set_description("Control Costs")

        # Setting up the collocation constraint
        self._add_collocation_constraint_euler(prog, X, U, N, Tf)

        # Add Kinematic constraints
        self._add_kinematic_constraints(prog, X, U,
                                        xmin, xmax,
                                        umin, umax)

        # Boundary Conditions
        self._add_boundary_conditions(prog, X, x0, xf)

        # Set initial guess of decision variables
        #  Straight line in statespace
        x_init_guess = np.array([x0] * (N + 1)) + \
            np.array([np.linspace(0, 1, N + 1)] * n_states).T * \
            np.array([(xf - x0)] * (N + 1))
        #  No control input
        u_init_guess = np.zeros([N + 1, n_ctrl])
        prog.SetInitialGuess(X, x_init_guess)
        prog.SetInitialGuess(U, u_init_guess)

        # Call the solver
        solver = SnoptSolver()
        # Set print level to 5.
        # prog.SetSolverOption(solver.solver_id(), "print file", 1)
        # prog.SetSolverOption(solver.solver_id(), "major print level", 1)
        result = solver.Solve(prog)

        # Check that we could find a solution
        assert(result.is_success()), "Solver didn't converge"

        # Extract the solution
        x_prime = result.GetSolution(X)
        # Normalize angle output
        x_prime[:, 2] = self.normalize_angle(x_prime[:, 2])

        u_prime = result.GetSolution(U)
        t_prime = np.linspace(0, Tf, N + 1)

        # And return it
        return t_prime, u_prime, x_prime

    def time_optimal_trajectory(self, x0, xf,
                                xmin, xmax, umin,
                                umax, N) -> Tuple[np.array,
                                                  np.array,
                                                  np.array]:
        """Function that finds the time optimal trajectory."""
        # State and control space
        n_states = len(x0)
        n_ctrl = len(umin)
        # Init the NLP handler
        prog = MathematicalProgram()

        # Init decision variables
        X = prog.NewContinuousVariables(N + 1, n_states, "X")
        U = prog.NewContinuousVariables(N + 1, n_ctrl, "U")
        Tf = prog.NewContinuousVariables(1, "Tf")

        # Minimize Quadratic Time
        prog.AddQuadraticCost(Tf[0] ** 2)
        prog.AddBoundingBoxConstraint(0, 1e10, Tf[0])
        # Setting up the collocation constraint
        self._add_collocation_constraint_hermite_simpson(prog, X, U, N, Tf)

        # Add Kinematic constraints
        self._add_kinematic_constraints(prog, X, U,
                                        xmin, xmax,
                                        umin, umax)

        # Boundary Conditions
        self._add_boundary_conditions(prog, X, x0, xf)

        # Set initial guess of decision variables
        #  Straight line in statespace
        x_init_guess = np.array([x0] * (N + 1)) + \
            np.array([np.linspace(0, 1, N + 1)] * n_states).T * \
            np.array([(xf - x0)] * (N + 1))
        #  No control input
        u_init_guess = np.zeros([N + 1, n_ctrl])
        prog.SetInitialGuess(X, x_init_guess)
        prog.SetInitialGuess(U, u_init_guess)

        # Call the solver
        solver = IpoptSolver()
        # Set print level to 0.
        prog.SetSolverOption(solver.solver_id(), "print_level", 0)
        result = solver.Solve(prog)

        # Check that we could find a solution
        assert(result.is_success()), "Solver didn't converge"

        # Extract the solution
        x_prime = result.GetSolution(X)
        # Normalize angle output
        x_prime[:, 2] = self.normalize_angle(x_prime[:, 2])

        u_prime = result.GetSolution(U)
        Tf_prime = result.GetSolution(Tf)
        t_prime = np.linspace(0, Tf_prime, N + 1)

        # And return it
        return t_prime, u_prime, x_prime

    def _add_boundary_conditions(self, prog, X, x0, xf):
        # Initial and Final State Constraint
        c1 = prog.AddConstraint(eq(X[0, :], x0))
        c1.evaluator().set_description("InitialStateConstraint")
        c2 = prog.AddConstraint(eq(X[-1, :-1], xf[:-1]))
        c2.evaluator().set_description("FinalStateConstraint")

    def _add_kinematic_constraints(self, prog, X, U, xmin, xmax, umin, umax):
        # Control Constraints
        for i in range(np.size(U, 1)):
            c = prog.AddBoundingBoxConstraint(umin[i], umax[i], U[:, i])
            c.evaluator().set_description(f"U_limits_{i}")
        # State Constraints
        for i in range(np.size(X, 1)):
            c = prog.AddBoundingBoxConstraint(xmin[i], xmax[i], X[:, i])
            c.evaluator().set_description(f"X_limits_{i}")

    def _add_collocation_constraint_hermite_simpson(self, prog, X, U, N, Tf):
        #  Time duration of one interval
        dt = Tf / N
        # Loop over all knot point and enforce the collocation costraint
        for k in range(N):
            c = prog.AddConstraint(
                eq((X[k, :] - X[k + 1, :])
                   + dt / 6 * (self._f(X[k, :], U[k, :])
                               + 4 * self._f(self._xc(X, U, k, dt),
                                             self._uc(U, k))
                               + self._f(X[k + 1, :], U[k + 1, :])),
                   [0] * np.size(X, 1)))
            c.evaluator().set_description(f"Collocation_Constraint_{k}")

    def _add_collocation_constraint_euler(self, prog, X, U, N, Tf):
        #  Time duration of one interval
        dt = Tf / N
        # Loop over all knot point and enforce the collocation costraint
        for k in range(N):
            for j in range(np.size(X, 1)):
                c = prog.AddConstraint(
                    eq((X[k, j] - X[k + 1, j]) + dt
                        * self._f(X[k, :], U[k, :])[j], [0]))
                c.evaluator().set_description(
                    f"Collocation_Constraint_{k}_{j}")

    def _f(self, x, u):
        """Helper function to express the system dynamics more concise."""
        return self.model.continuous_dynamics(x, u)

    def _xc(self, x, u, k, dt):
        """Helper function to compute the collocation point."""
        return 1 / 2 * (x[k, :] + x[k + 1, :])\
            + dt / 8 * (self._f(x[k, :], u[k, :])
                        - self._f(x[k + 1, :], u[k + 1, :]))

    def _uc(self, u, k):
        """Helper function to compute the control value
           at the collocation point."""
        return (u[k, :] + u[k + 1, :]) / 2
