"""Python script that finds the optimal activation trajectory."""
import numpy as np
import time
from trajectory_optimizer import TrajectoryOptimizer
from reacsa import Reacsa

model = Reacsa()
opti = TrajectoryOptimizer(model)

# Parameters for the optimization
#  Initial and Final state
x0 = np.array([[0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0],
               [1, 1, 0, 0, 0, 0, 0]])
xf = np.array([[1, 2, np.pi, 0, 0, 0, 0],
               [0.2, 3, 0, 0, 0, 0, 250 * 2 * np.pi / 60],
               [0, 1, -1, 0, 0, 0, 0]])

#  State and Control Limitations
xmin = - np.array([100, 100, 100, 100, 100, 100, 250 * 2 * np.pi / 60])
xmax = np.array([100, 100, 100, 100, 100, 100, 250 * 2 * np.pi / 60])
umin = - np.array([1.44, 0, 0, 0, 0, 0, 0, 0, 0])
umax = np.array([1.44, 1, 1, 1, 1, 1, 1, 1, 1])
data = np.array([[10.0, 20.0, 30.0, 50.0, 100.0, 200.0, 300.0],
                 [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                 [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
                 ])

for traj in range(3):
    for i in range(len(data[0, :])):
        N = data[0, i]
        times = np.zeros(100)
        for j in range(100):
            start = time.time()
            print(f"{N} knot points...")
            # Find time optimal trajectort
            t_tstar, _, _ = opti.time_optimal_trajectory(x0[traj, :],
                                                         xf[traj, :],
                                                         xmin, xmax,
                                                         umin, umax,
                                                         10)
            alpha = 12

            # Find energy optimal trajectory
            t_star, u_star, x_star = \
                opti.energy_optimal_trajectory(x0[traj, :], xf[traj, :],
                                               xmin, xmax,
                                               umin, umax,
                                               int(N),
                                               alpha * t_tstar[-1])
            times[j] = time.time() - start
            print("... done.")
        data[1, i] = np.mean(times)
        data[2, i] = np.std(times)

    np.savetxt(f"t{traj + 1}times.txt", data.transpose())
