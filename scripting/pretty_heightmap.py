import sys
from matplotlib.pylab import cm
import numpy as np
import cv2
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

res = 100  # dpi
size = 16, 12

n_pix = 513
hm_res = 10.0 / n_pix  # m/pixel
max_h = 0.001004545239150106 * 1000  # mm


def main(im_path):
    im = cv2.imread(im_path, cv2.COLORMAP_JET)

    x, y = np.meshgrid(hm_res * np.arange(im.shape[0]),
                       hm_res * np.arange(im.shape[1]))

    # show hight map in 3d
    fig0 = plt.figure()
    ax = fig0.add_subplot(111, projection='3d')
    ax.set_xlabel("x [m]")
    ax.set_ylabel("y [m]")
    ax.set_zlabel("Height [mm]")
    ax.plot_surface(x, y, max_h / 255 * im[:, :])
    plt.show()

    # show hight map in 2d
    im = cv2.imread(im_path, cv2.COLORMAP_JET)
    fig1, ax1 = plt.subplots(figsize=[5, 4])
    ax1.set_xlabel("x [m]")
    ax1.set_ylabel("y [m]")
    p = ax1.imshow(max_h / 255 * im[:, :],
                   extent=[-hm_res * n_pix / 2 + 2, hm_res * n_pix / 2 + 2,
                           -hm_res * n_pix / 2, hm_res * n_pix / 2],
                   cmap=cm.jet)
    bar = plt.colorbar(p)
    bar.set_label("Height [mm]")
    p.set_clim(0, max_h)

    ax1.set_aspect('equal', adjustable='box')
    fig1.tight_layout()
    fig1.savefig("heightmap.png", bbox_inches="tight", dpi=res)



if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print("Wrong number of arguments. Usage:\n"
              + "python plot.py <path to heightmap.png>")
