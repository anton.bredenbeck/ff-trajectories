""" This script reads an excel sheet of a measured surface and
    turns it into a greyscale png file which is readable for
    gazebo in order to create an height map. Since the measured
    floor isn't square, but the gazebo requires square png images
    the height map is padded by zero height pixels. """
import pandas as pd
import numpy as np
import cv2
from scipy import interpolate

# Floor dimensions
floorWidth = 4750 # mm
floorHeight = 8780 # mm

# Define image size in pixels and mm (for gazebo sdf models it needs to be 2^n+1, else 2^n is good)
imageSize = 129
imageSizeMm = 10000

# Define pixel equivalent in mm
pxLength = imageSizeMm/imageSize

# Read the excel as database
x = pd.read_excel(
    'Orbital Robotics Lab Floor Measurements 121115.xlsx', 'XYZ 121115')
x = x[1:]
x = x.set_index('ID', drop=True)

# Extract all x-y coordinate pairs
xy = np.array(x[['Y','X']]) # switched order to be in image coordinate system: origin top left, x right, y down
min_x = min(x['Y'])  # switched order
min_y = min(x['X'])  # switched order
max_x = max(x['Y'])  # switched order
max_y = max(x['X'])  # switched order

# Find the maximal measured extend
min_val = min([min_x, min_y])
max_val = max([max_x, max_y])

# Extract z data
z = np.array(x['Z'])
min_z = min(z)
max_z = max(z)

# Wall corner coordinates
corner1 = np.array([-(floorWidth-(max_x-min_x))/2, -(floorHeight-(max_y-min_y))/2])
corner2 = np.array([floorWidth-(floorWidth-(max_x-min_x))/2, floorHeight-(floorHeight-(max_y-min_y))/2])

# Buffer (in mm) chosen such that the resulting image is square
buffer = ((imageSize-1)*pxLength-(max_val-min_val))/2

# Create a square grid that covers the maximal extend and
# has a buffer around it
grid_x = np.linspace(min_val - buffer, max_val + buffer, imageSize)
grid_y = np.linspace(min_val - buffer, max_val + buffer, imageSize)

# Combine it into a mesh grid and interpolate the height values
# The interpolation slightly alters the min and max values
# rest of the floor (buffer area) is levelled at 0
xi, yi = np.meshgrid(grid_x, grid_y)
zi = interpolate.griddata(xy, z, (xi, yi), method='cubic', fill_value=0)

# Normalize zi so that values are between 0 and 1
min_zi = np.amin(zi)
# zi[zi == 10] = min_z # make everything else minimum
max_zi = np.amax(zi)

zi = (zi - min_zi) / (max_zi - min_zi)

print("### Excel data ###")
print(f"Max x = {max_x/1000} m, min x = {min_x/1000} m, difference = {(max_x-min_x)/1000} m")
print(f"Max y = {max_y/1000} m, min y = {min_y/1000} m, difference = {(max_y-min_y)/1000} m")
print(f"Max z = {max_z/1000} m, min z = {min_z/1000} m, difference = {(max_z-min_z)/1000} m")
print("### Interpolated data ###")
print(f"Max z = {max_zi/1000:.6f} m, min z = {min_zi/1000:.6f} m, difference = {(max_zi-min_zi)/1000:.6f} m")
print("### Image data (image coordinate system) ###")
print(f"Image size = {zi.shape} px, ({pxLength*imageSize/1000},{pxLength*imageSize/1000}) m")
print(f"Resolution: {pxLength:.6f} mm/px")
print(f"Middle image coordinate: ({pxLength*imageSize/1000/2},{pxLength*imageSize/1000/2}) m")
print(f"Floor corner coordinates: ({(corner1[0]-(min_val - buffer-pxLength/2))/1000:.6f},{(corner1[0]-(min_val - buffer-pxLength/2))/1000:.6f}) m, ({(corner2[0]-(min_val - buffer-pxLength/2))/1000:.6f},{(corner2[0]-(min_val - buffer-pxLength/2))/1000:.6f}) m")
print(f"Floor coordinate system: ({-(min_val - buffer-pxLength/2)/1000:.6f},{-(min_val - buffer-pxLength/2)/1000:.6f})")
print(f"Middle floor coordinate: ({(corner1[0]-(min_val - buffer-pxLength/2)+floorWidth/2)/1000:.6f},{(corner1[0]-(min_val - buffer-pxLength/2)+floorHeight/2)/1000:.6f})")
print("### Floor data (floor coordinate system) ###")
print(f"Image corner coodinates: ({(min_val - buffer-pxLength/2)/1000:.6f},{(min_val - buffer-pxLength/2)/1000:.6f}) m, ({(max_val + buffer+pxLength/2)/1000:.6f},{(max_val + buffer+pxLength/2)/1000:.6f}) m")
print(f"Middle image coordinate: [{grid_x[int((imageSize-1)/2)]/1000:.6f},{grid_y[int((imageSize-1)/2)]/1000:.6f}] m")
print(f"Floor corner coodinates: ({corner1[0]/1000:.6f},{corner1[0]/1000:.6f}) m, {corner2[0]/1000:.6f},{corner2[0]/1000:.6f} m, x length = {floorWidth/1000} m, y length = {floorHeight/1000} m")
print(f"Middle floor coordinate: ({(corner1[0]+floorWidth/2)/1000:.6f},{(corner1[0]+floorHeight/2)/1000:.6f})")

grayImage = np.array(zi*255, dtype = np.uint8)
colorImage = cv2.applyColorMap(grayImage, cv2.COLORMAP_JET)
cv2.imwrite("heightmap_"+str(imageSize)+".png", grayImage)
cv2.imwrite("heightmap_"+str(imageSize)+"_color.png", colorImage)