"""
A python script to compute the average error
between two trajectories in terms of 
    eucledian error (sqrt((x-x*)**2+(y-y*)**2))
    angular error abs(theta - theta*)
Over a trajectory
"""
import sys
import numpy as np
import matplotlib.pyplot as plt
from pydrake.all import PiecewisePolynomial


def wrap_to_pi(ang):
    return (ang + np.pi) % (2 * np.pi) - np.pi


def main(data_path, ground_truth_t_path, ground_truth_x_path=None):
    # Load text files
    data = np.loadtxt(data_path)
    start_time = data[0, 0]
    data[:, 0] = (data[:, 0] - start_time) * 1e-9

    if ground_truth_x_path is not None:
        ground_truth_t = np.loadtxt(ground_truth_t_path)
        ground_truth_x = np.loadtxt(ground_truth_x_path)

        traj = PiecewisePolynomial.CubicShapePreserving(
            ground_truth_t, ground_truth_x[:, :3].T)
    else:
        ground_truth = np.loadtxt(ground_truth_t_path)
        ground_truth[:, 0] = (ground_truth[:, 0] - start_time) * 1e-9
        traj = PiecewisePolynomial.CubicShapePreserving(
            ground_truth[:, 0], ground_truth[:, 1:4].T)

    opti = np.array([traj.value(t) for t in data[:, 0]])
    x_err, y_err, eucl_error, theta_error = 0, 0, 0, 0

    for i in range(len(data)):
        x_err = x_err + 1 / len(data[:, 0]) * (opti[i, 0] - data[i, 1])**2
        y_err = y_err + 1 / len(data[:, 0]) * (opti[i, 1] - data[i, 2])**2
        eucl_error = eucl_error\
            + 1 / len(data[:, 0]) * ((opti[i, 0] - data[i, 1])**2
                                     + (opti[i, 1] - data[i, 2])**2)
        theta_error = theta_error\
            + 1 / len(data[:, 0]) * (wrap_to_pi(opti[i, 2] - data[i, 3]))**2

    x_err = np.sqrt(x_err)
    y_err = np.sqrt(y_err)
    eucl_error = np.sqrt(eucl_error)
    theta_error = np.sqrt(theta_error)
    plt.plot(data[:, 1], data[:, 2], ":")
    plt.plot(opti[:, 0], opti[:, 1])
    plt.grid()
    fig, axs = plt.subplots(2)
    axs[0].plot(data[:, 0], data[:, 1], ":")
    axs[0].plot(data[:, 0], data[:, 2], ":")
    axs[0].plot(data[:, 0], opti[:, 0])
    axs[0].plot(data[:, 0], opti[:, 1])
    axs[0].grid()
    axs[1].plot(data[:, 0], 180.0 / np.pi * wrap_to_pi(data[:, 3]), ":")
    axs[1].plot(data[:, 0], 180.0 / np.pi * wrap_to_pi(opti[:, 2]))
    axs[1].grid()
    plt.show()
    print(f"Average X Error = {x_err}")
    print(f"Average Y Error = {y_err}")
    print(f"Average Euclidean Error = {eucl_error}")
    print(f"Average Angular Error = {theta_error}")


if __name__ == '__main__':
    if len(sys.argv) == 3:
        main(sys.argv[1], sys.argv[2])
    elif len(sys.argv) == 4:
        main(sys.argv[1], sys.argv[2], sys.argv[3])
    else:
        print("Wrong number of arguments. Usage:")
        print("python avg_error.py <path to data file>"
              + " <path to ground truth folder>"
              + " [or time and state file seperately]")
