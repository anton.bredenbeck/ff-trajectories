import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate

f_smpl = 10
K = 1
eps = 1 / f_smpl

t = np.linspace(0, 4 * np.pi, 10000)
f = 1 / 2 * (np.sin(t - np.pi / 2) + 1)
f_sigdel = 0 * t

integrator = 0
integrator_arr = 0 * t
output = 0
t_last = 0
for i in range(1, len(t)):
    if t[i] - t_last > 1 / f_smpl:
        integrator += (t[i] - t_last) * K * (f[i] - output)
        integrator_arr[i] = integrator
        t_last = t[i]

        if integrator > eps:
            output = 1
        else:
            output = 0

    integrator_arr[i] = integrator
    f_sigdel[i] = output


print(integrate.simps(f, t))
print(integrate.simps(f_sigdel, t))

fig, axs = plt.subplots(2, sharex=True)
axs[0].plot(t, f)
axs[0].fill_between(t, f_sigdel, color="orange")
axs[0].grid()
axs[0].legend(["Continuous Signal", "Modulated Signal"])
axs[0].set_xlim([0, 4 * np.pi])
axs[0].set_ylim([0, 1.1])
axs[0].set_ylabel("Output")
axs[1].plot(t, integrator_arr)
axs[1].grid()
axs[1].set_xlabel("Time [s]")
axs[1].set_ylabel("Integrator Value")

plt.tight_layout()
plt.savefig("example_modulation.eps")
